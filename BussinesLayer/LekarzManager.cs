﻿using DataLayer;
using DataLayer.Connection;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerLekarzy
    {
        public ManagerLekarzy(Connector connector)
        {
            this.connector = connector;
        }

        public class PoszukiwaczLekarzy : IPoszukiwacz<Lekarz>
        {
            private Connector connector;
            public int? idLekarza = null;

            public PoszukiwaczLekarzy(ManagerLekarzy m)
            {
                connector = m.connector;
            }

            public PoszukiwaczLekarzy IdLekarza(int id)
            {
                this.idLekarza = id;
                return this;
            }

            public void Wykonaj(Action<Lekarz> action)
            {
                connector.ExecuteTransaction(session =>
                {
                    var query = session.Query<Lekarz>();
                    if (idLekarza != null)
                        query = query.Where(x => x.Id == idLekarza.Value);

                    var lekarze = query.ToList();
                    lekarze.Sort((x, y) =>
                    {
                        int result = x.Nazwisko.CompareTo(y.Nazwisko);
                        if (result == 0)
                            result = x.Imie.CompareTo(y.Imie);
                        return result;
                    });
                    foreach (var l in lekarze)
                        action(l);
                });
            }
        }

        private Connector connector;
    }
}
