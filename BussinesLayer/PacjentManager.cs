﻿using DataLayer;
using DataLayer.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace BusinessLayer
{
    public class ManagerPacjentow
    {
        public ManagerPacjentow(Connector connector)
        {
            this.connector = connector;
        }

        public class PoszukiwaczPacjentow : IPoszukiwacz<Pacjent>
        {
            private Connector connector;
            private string imiePacjenta = null;
            private string nazwiskoPacjenta = null;
            private string pesel = null;
            private string adres = null;
            private int? idPacjenta = null;

            public PoszukiwaczPacjentow(ManagerPacjentow m)
            {
                this.connector = m.connector;
            }

            public PoszukiwaczPacjentow IdPacjenta(int id)
            {
                this.idPacjenta = id;
                return this;
            }

            public PoszukiwaczPacjentow Imie(string imie)
            {
                imiePacjenta = imie;
                return this;
            }

            public PoszukiwaczPacjentow Nazwisko(string nazwisko)
            {
                nazwiskoPacjenta = nazwisko;
                return this;
            }

            public PoszukiwaczPacjentow Pesel(string pesel)
            {
                this.pesel = pesel;
                return this;
            }

            public PoszukiwaczPacjentow Adres(string adres)
            {
                this.adres = adres;
                return this;
            }

            public void Wykonaj(Action<Pacjent> action)
            {
                connector.ExecuteTransaction(session =>
                {

                    var query = session.Query<Pacjent>();
                    if (idPacjenta != null)
                        query = query.Where(x => x.Id == idPacjenta.Value);
                    if (!string.IsNullOrEmpty(imiePacjenta))
                        query = query.Where(x => x.Imie.ToLower().Contains(imiePacjenta.ToLower()));
                    if (!string.IsNullOrEmpty(nazwiskoPacjenta))
                        query = query.Where(x => x.Nazwisko.ToLower().Contains(nazwiskoPacjenta.ToLower()));
                    if (!string.IsNullOrEmpty(adres))
                        query = query.Where(x => x.Adres.ToLower().Contains(adres.ToLower()));
                    if (!string.IsNullOrEmpty(pesel))
                        query = query.Where(x => x.Pesel.ToLower().Contains(pesel.ToLower()));

                    var result = query.ToList();
                    foreach (var p in result)
                        action(p);
                });
            }
        }

        public int NowyPacjent(string imie, string nazwisko, string pesel, string adres)
        {
            int result = 0;
            connector.ExecuteTransaction(session =>
            {
                var pac = new Pacjent { Imie = imie, Nazwisko = nazwisko, Pesel = pesel, Adres = adres };
                session.Save(pac);
                result = pac.Id;
            });
            return result;
        }
        public void EdytujPacjenta(int id, string imie, string nazwisko, string pesel, string adres)
        {
            connector.ExecuteTransaction(session =>
            {
                var pac = session.Get<Pacjent>(id);
                pac.Imie = imie;
                pac.Nazwisko = nazwisko;
                pac.Pesel = pesel;
                pac.Adres = adres;
                session.Save(pac);
            });
        }
        private Connector connector;
    }
}
