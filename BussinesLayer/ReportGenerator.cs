﻿using DataLayer.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using DataLayer;
using System.Data;
using System.Reflection;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace BusinessLayer
{
    public class ReportGenerator
    {
        public ReportGenerator(Connector c)
        {
            connector = c;
            font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, BaseFont.CP1250, 8);
            font5b = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, BaseFont.CP1250, 8);
            font5b.SetStyle("bold");
            frameColor = new BaseColor(0.5f, 0.5f, 0.5f);
        }
        iTextSharp.text.Font font5;
        iTextSharp.text.Font font5b ;
        BaseColor frameColor;
        public DataTable Pacjenci()
        {
            DataTable result = null;
            connector.ExecuteTransaction(session =>
            {
                var  list = session.Query<Pacjent>()
                .Select(x => new  { AdresZamieszkania = x.Adres, Imie = x.Imie, Nazwisko = x.Nazwisko, Pesel = x.Pesel })
                .ToList();
                result = CreateDataTable(list);
            });
            return result;
        }
        public DataTable TypyBadania()
        {
            DataTable result = null;
            connector.ExecuteTransaction(session =>
            {
                var list = session.Query<TypBadania>()
                    .ToList()
                    .Select(x => new  { Kod = x.Kod, Nazwa = x.Nazwa, NazwaTypu = Enum.GetName(typeof(BadanieTyp), x.Typ), Opis = x.Opis })
                    .ToList();
                result = CreateDataTable(list);
            });
            return result;
        }

        public DataTable Wizyty(string imie, string nazwisko, string pesel, DateTime? dataOd, DateTime? dataDo, int? stanId, int? lekarzId, int? rejestratorId)
        {
            DataTable result = null;
            connector.ExecuteTransaction(session =>
            {
                var query = session.Query<Wizyta>();
                if (imie != null && imie.Length > 0)
                {
                    imie = imie.ToLower();
                    query = query.Where(x => x.Pacjent.Imie.ToLower().Contains(imie));
                }
                if (nazwisko != null && nazwisko.Length > 0)
                {
                    nazwisko = nazwisko.ToLower();
                    query = query.Where(x => x.Pacjent.Nazwisko.ToLower().Contains(nazwisko));
                }
                if (pesel != null && pesel.Length > 0)
                {
                    pesel = pesel.ToLower();
                    query = query.Where(x => x.Pacjent.Pesel.ToLower().Contains(pesel));
                }
                if (dataOd != null || dataDo != null)
                {
                    query = query.Where(x => (x.DataRejestracji >= dataOd && x.DataRejestracji <= dataDo) || (x.DataZakonczenia >= dataOd && x.DataZakonczenia <= dataDo));
                }
                if (stanId != null)
                {
                    StanWizyty stan = (StanWizyty)stanId.Value;
                    query = query.Where(x => x.Stan == stan);
                }
                if (lekarzId != null)
                {
                    query = query.Where(x => x.Lekarz.Id == lekarzId.Value);
                }
                if (rejestratorId != null)
                {
                    query = query.Where(x => x.Rejestrator.Id == rejestratorId.Value);
                }
                var list = query.Select(x => new {Id = x.IdWizyty, Imię = x.Pacjent.Imie, Nazwisko = x.Pacjent.Nazwisko, DataRejestracji = x.DataRejestracji, DataZakończenia = x.DataZakonczenia, lekarz = x.Lekarz.Imie + " " + x.Lekarz.Nazwisko, rejestrator = x.Rejestrator.Imie + " " + x.Rejestrator.Nazwisko, Stan = x.Stan}).ToList();
                result = CreateDataTable(list);
            });
            return result;
        }
        /// <summary>
        /// Generuje DataTAble z listy
        /// nazwy kolumn sązczytywane z typu T listy za pomocą refleksji, wyrazy oddzielane ( DataUrodzenia będzie widoczne jako data urodzenia)
        /// aby zmienić jakie kolumny / jak się mają nazywać wystarczy zrobić selecta na podawanej liście: 
        /// list1 = list.Select(x => new {ImiePacjenta = x.Imie, Nazwisko = x.Nazwisko).ToList();
        /// CrateDataTable(list1);
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable CreateDataTable<T>(IList<T> data)
        {
            Type t = typeof(T);
            List<PropertyInfo> prop = t.GetProperties().ToList();
            DataTable dt = new DataTable();
            foreach (var p in prop)
            {
                dt.Columns.Add(new DataColumn(ToSpaceCase(p.Name)));
            }
            foreach (T d in data)
            {
                string[] values = new string[prop.Count];

                for (int i = 0; i < prop.Count; i++)
                {
                    var value = prop[i].GetValue(d);
                    if(value == null){
                        values[i] = "";
                    }else{
                        if (prop[i].PropertyType.IsEnum)
                        {
                            values[i] = ToSpaceCase(Enum.GetName(prop[i].PropertyType, value));
                        }
                        else
                        {
                            values[i] = value.ToString();
                        }
                    }
                }
                dt.Rows.Add(values);
            }
            return dt;
        }

        public void WizytyReportPdf(List<int> ids, string fileName, DateTime dataOd, DateTime dataDo)
        {
            using (var file = new FileStream(fileName, FileMode.Create))
            {
                connector.ExecuteTransaction(session =>
                {
                    Document document = new Document();
                    PdfWriter writer = PdfWriter.GetInstance(document, file);
                    document.Open();
                    var wizyty = session.Query<Wizyta>()
                        .Where(x => ids.Contains(x.IdWizyty));
                    var lAnulowanych = wizyty.Where(x => x.Stan == StanWizyty.Anulowana).Count();
                    var lBadanf = wizyty.SelectMany(x => x.BadaniaFizyczne).Count();
                    var lBadanl = wizyty.SelectMany(x => x.BadaniaLaboratoryjne).Count();


                    var basicData = new { ZakresDat = dataOd.ToString() + " - " + dataDo.ToString(), DataGeneracji = DateTime.Now, IlośćWizyt = ids.Count, IlośćAnulowanychWizyt = lAnulowanych, ŁącznaIlośćBadańLaboratorych = lBadanf + lBadanl};
                    AddTitle(document, "Raport Wizyt", 20);
                    AddRecord(document, basicData);
                    AddTitle(document, "Raport szczegółowy", 20);

                    foreach (var w in wizyty)
                    {
                        WizytaPdf(document, w);
                    }

                    document.Close();
                });
            }
        }
        private void WizytaPdf(Document document, Wizyta w)
        {
            AddTitle(document, "Wizyta #" + w.IdWizyty, 15);
            var daneZwykle = new { DataRejestracji =  w.DataRejestracji, DataZakończenia = w.DataZakonczenia, Stan = w.Stan, pacjent = w.Pacjent.Imie + " " + w.Pacjent.Nazwisko, lekarz = w.Lekarz.Imie + " " + w.Lekarz.Nazwisko, Rejestrator = w.Rejestrator.Imie + " " + w.Rejestrator.Nazwisko, Opis = w.Opis, Diagnoza = w.Diagnoza};
            AddRecord(document,daneZwykle);
            if (w.BadaniaLaboratoryjne.Count > 0)
            {
                AddTitle(document, "Badania Laboratoryjne");
                int cnt = 0;
                foreach (var b in w.BadaniaLaboratoryjne)
                {
                    cnt++;
                    AddTitle(document, "#" + cnt.ToString(), 8);
                    var dane = new { Nazwa = b.TypBadania.Nazwa, Kod = b.TypBadania.Kod, Wynik = b.Wynik, DataZlecenia = b.DataZlecenia, DataWykonania = b.DataWykonania, DataZatwierdzenia = b.DataZatwierdzenia, DataAnulowania = b.DataAnulowania, UwagiLekarza = b.UwagiLekarza, UwagiKierownika = b.UwagiKierownika, Stan = b.Stan, Laborant = b.Laborant == null ? "" : b.Laborant.Imie + " " + b.Laborant.Nazwisko, KierownikLaboratorium = b.KierownikLaboratorium == null ? "" : b.KierownikLaboratorium.Imie + " " + b.KierownikLaboratorium.Nazwisko };
                    AddRecord(document, dane);
                }
            }
            if (w.BadaniaFizyczne.Count > 0)
            {
                AddTitle(document, "Badania Fizyczne");
                int cnt = 0;
                foreach (var b in w.BadaniaFizyczne)
                {
                    cnt++;
                    AddTitle(document, "#" + cnt.ToString(), 8);
                    var dane = new { Nazwa = b.TypBadania.Nazwa, Kod = b.TypBadania.Kod, Wynik = b.Wynik };
                    AddRecord(document, dane);
                }
            }
        }
        private  void AddRecord<T>(Document document, T obj)
        {
            var l = new List<T>();
            l.Add(obj);
            DataTable data = CreateDataTable(l);
            PdfPTable table = new PdfPTable(2);
            float[] widths = new float[] { 2, 4 };
            table.SetWidths(widths);
            table.WidthPercentage = 100;
            for (int i = 0; i < data.Columns.Count; i++ )
            {
                var phrase1 = new Phrase(data.Columns[i].ColumnName, font5b);
                var cell1 = new PdfPCell(phrase1);
                cell1.BorderColor = frameColor;
                table.AddCell(cell1);
                var phrase2 = new Phrase(data.Rows[0][i].ToString(), font5);
                var cell2 = new PdfPCell(phrase2);
                cell2.BorderColor = frameColor;
                table.AddCell(cell2);
            }
            document.Add(table);
        }

        private  void AddTitle(Document document, string title, int size = 10) {
            var p = new Paragraph(title, iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, BaseFont.CP1250, size));
           p.SpacingAfter = size;
            document.Add(p);
        }

        public static void DataTableToPdf(DataTable dt, string title, string filename, HashSet<int> selectedRows = null)
        {
            using (var file = new FileStream(filename, FileMode.Create))
            {
                Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, file);
                document.Open();
                document.AddTitle(title);
                document.Add(new Paragraph(title));
                iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.COURIER, 5);


                PdfPTable table = new PdfPTable(dt.Columns.Count);

                float[] widths = new float[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    widths[i] = 4f;
                }

                table.SetWidths(widths);
                table.WidthPercentage = 100;
                PdfPCell cell = new PdfPCell(new Phrase("Products"));

                cell.Colspan = dt.Columns.Count;
                foreach (DataColumn c in dt.Columns)
                {
                    var hcell = new Phrase(c.ColumnName, font5);
                    foreach (var chunk in hcell.Chunks)
                    {
                        chunk.SetBackground(new BaseColor(0.5f, 0.5f, 0.5f, 0.5f));
                    }
                    table.AddCell(hcell);
                }

                for (int r = 0; r < dt.Rows.Count; r++ )
                {
                    if (selectedRows != null)
                    {
                        if (!selectedRows.Contains(r)) continue;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            table.AddCell(new Phrase(dt.Rows[r][i].ToString(), font5));
                        }

                    }
                }
                document.Add(table);
                document.Close();
            }
        }

        public static string ToSpaceCase(string n)
        {

            string result = "";
            foreach (char x in n)
            {

                if (Char.IsUpper(x) && result.Count() > 0)
                {
                    result += " ";
                }
                char toAdd;
                if (result.Length == 0)
                {
                    toAdd = Char.ToUpper(x);
                }
                else
                {
                    toAdd = Char.ToLower(x);
                }

                result += toAdd;
            }
            if (result.Length > 0)
            {
            }
            return result;
        }
        private Connector connector;
    }
}
