﻿using DataLayer;
using DataLayer.Connection;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerBadanFizycznych
    {
        public class PoszukiwaczBadanFizycznych : IPoszukiwacz<BadanieFizyczne>
        {
            private Connector connector;
            private int? idBadania = null;

            public PoszukiwaczBadanFizycznych(ManagerBadanFizycznych m)
            {
                this.connector = m.connector;
            }

            public PoszukiwaczBadanFizycznych IdBadania(int id)
            {
                idBadania = id;
                return this;
            }

            public void Wykonaj(Action<BadanieFizyczne> action)
            {
                connector.ExecuteTransaction(session => {
                    var query = session.Query<BadanieFizyczne>();
                    if (idBadania != null)
                        query = query.Where(x => x.Id == idBadania.Value);

                    var result = query.ToList();
                    foreach (var b in result)
                        action(b);
                });
            }
        }

        public ManagerBadanFizycznych(Connector connector)
        {
            this.connector = connector;
        }

        private Connector connector;
    }
}
