﻿using DataLayer.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using DataLayer;
using System.Security.Cryptography;
namespace BusinessLayer
{
    public class ManagerKont
    {
        public class PoszukiwaczKont : IPoszukiwacz<Konto>
        {
            private Connector connector;
            private string imie = null;
            private string nazwisko = null;
            private string login = null;
            private KontoRola? rola = null;
            private int? idKonta = null;

            public PoszukiwaczKont(ManagerKont m)
            {
                this.connector = m.connector;
            }

            public PoszukiwaczKont Imie(string imie)
            {
                this.imie = imie;
                return this;
            }

            public PoszukiwaczKont Nazwisko(string nazwisko)
            {
                this.nazwisko = nazwisko;
                return this;
            }

            public PoszukiwaczKont Login(string login)
            {
                this.login = login;
                return this;
            }

            public PoszukiwaczKont Rola(KontoRola rola)
            {
                this.rola = rola;
                return this;
            }

            public PoszukiwaczKont IdKonta(int id)
            {
                this.idKonta = id;
                return this;
            }

            public void Wykonaj(Action<Konto> action)
            {
                connector.ExecuteTransaction(session => {
                    List<Konto> users = null;
                    if (rola != null)
                    {
                        switch (rola.Value)
                        {
                            case KontoRola.Administrator:
                                users = Query<Administrator>(session);
                                break;
                            case KontoRola.Lekarz:
                                users = Query<Lekarz>(session);
                                break;
                            case KontoRola.Laborant:
                                users = Query<Laborant>(session);
                                break;
                            case KontoRola.KierownikLaboratorium:
                                users = Query<KierownikLaboratorium>(session);
                                break;
                            case KontoRola.Rejestrator:
                                users = Query<Rejestrator>(session);
                                break;
                        }
                    }
                    else
                        users = Query<Konto>(session);

                    users.Sort((x, y) => {
                        if (x.Login != y.Login)
                            return x.Login.CompareTo(y.Login);
                        else if (x.Imie != y.Imie)
                            return x.Imie.CompareTo(y.Imie);
                        else if (x.Nazwisko != y.Nazwisko)
                            return x.Nazwisko.CompareTo(y.Nazwisko);
                        else if (x.GetRole() != y.GetRole())
                            return x.GetRole().CompareTo(y.GetRole());
                        else
                            return x.DataKonca.Value.CompareTo(y.DataKonca.Value);
                    });
                    foreach (var u in users)
                        action(u);
                });
            }

            private List<Konto> Query<T>(NHibernate.ISession session) where T : Konto
            {
                var query = session.Query<T>();
                if (idKonta != null)
                    query = query.Where(x => x.Id == idKonta.Value);
                if (!string.IsNullOrEmpty(imie))
                    query = query.Where(x => x.Imie.ToLower().Contains(imie.ToLower()));
                if (!string.IsNullOrEmpty(nazwisko))
                    query = query.Where(x => x.Nazwisko.ToLower().Contains(nazwisko.ToLower()));
                if (!string.IsNullOrEmpty(login))
                    query = query.Where(x => x.Login.ToLower().Contains(login.ToLower()));
                return query.ToList<Konto>();
            }
        }

        public void HasloHash(Konto konto)
        {

        }

        public int? ZalogowaneKontoId { get; private set; }
        public bool Zaloguj(string login, string haslo)
        {
            connector.ExecuteTransaction(session =>
            {
                var konto = session.Query<Konto>()
                    .Where(x => x.Login == login)
                    .SingleOrDefault();
                if (konto != null && konto.DataKonca != null && konto.DataKonca >  DateTime.Now && konto.SprawdzHaslo(haslo))
                {
                    ZalogowaneKontoId = konto.Id;
                }
                else
                {
                    ZalogowaneKontoId = null;
                }
                });
            return ZalogowaneKontoId != null;
        }

        public ManagerKont(Connector connector)
        {
            this.connector = connector;
        }
        public void Wyloguj()
        {
            ZalogowaneKontoId = null;
            foreach (var a in onLogout)
            {
                a();
            }
        }

        public void AddLogoutListener(Action a)
        {
            onLogout.Add(a);
        }
        public KontoRola? RolaZalogowanegoKonta()
        {
            if (ZalogowaneKontoId == null)
            {
                return null;
            }
            KontoRola? result = null;
            connector.ExecuteTransaction(session =>
            {
                result = session.Get<Konto>(ZalogowaneKontoId).GetRole();
            });
            return result;
        }

        public bool CzyLoginWolny(string login)
        {
            bool result = false;
            connector.ExecuteTransaction(session =>
            {
                var konto = session.Query<Konto>()
                    .Where(x => x.Login == login)
                    .SingleOrDefault();
                result = (konto == null) ? true : false;
            });
            return result;
        }

        public int DodajKonto(string imie, string nazwisko, string login, string haslo, KontoRola rola, DateTime dataWaznosci)
        {
            int result = 0;
            connector.ExecuteTransaction(session =>
            {
                Konto konto = null;
                switch (rola)
                {
                    case KontoRola.Administrator:
                        konto = new Administrator { Imie = imie, Nazwisko = nazwisko, Login = login, DataKonca = dataWaznosci };
                        break;
                    case KontoRola.KierownikLaboratorium:
                        konto = new KierownikLaboratorium { Imie = imie, Nazwisko = nazwisko, Login = login, DataKonca = dataWaznosci };
                        break;
                    case KontoRola.Laborant:
                        konto = new Laborant { Imie = imie, Nazwisko = nazwisko, Login = login, DataKonca = dataWaznosci };
                        break;
                    case KontoRola.Lekarz:
                        konto = new Lekarz { Imie = imie, Nazwisko = nazwisko, Login = login, DataKonca = dataWaznosci };
                        break;
                    case KontoRola.Rejestrator:
                        konto = new Rejestrator { Imie = imie, Nazwisko = nazwisko, Login = login, DataKonca = dataWaznosci };
                        break;
                }
                konto.Haslo = haslo;
                session.Save(konto);
                result = konto.Id;
            });
            return result;
        }

        public void EdytujKonto(int id, string imie, string nazwisko, string login, DateTime dataWaznosci, string haslo = null)
        {
            connector.ExecuteTransaction(session =>
            {
                var konto = session.Get<Konto>(id);
                if (konto != null)
                {
                    konto.Imie = imie;
                    konto.Nazwisko = nazwisko;
                    konto.Login = login;
                    if (haslo != null)
                    {
                        konto.Haslo = haslo;
                    }
                    konto.DataKonca = dataWaznosci;
                    session.Update(konto);
                }
            });
        }


        private Connector connector;
        private List<Action> onLogout = new List<Action>();
    }
}
