﻿using DataLayer;
using DataLayer.Connection;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ApplicationEngine
    {
        public ManagerKont KontoManager { get; private set; }
        public ManagerPacjentow PacjentManager { get; private set; }
        public ManagerTypowBadan SlownikBadanManager { get; private set; }
        public ManagerBadanLaboratoryjnych BadanieLaboratoryjneManager { get; private set; }
        public ManagerLekarzy LekarzManager { get; private set; }
        public ManagerLaborantow LaborantManager { get; private set; }
        public ManagerWizyt WizytaManager { get; private set; }
        public ManagerBadanFizycznych BadanieFizyczneManager { get; private set; }
        public ReportGenerator ReportGenerator { get; private set; }

        private Connector connector;
        public ApplicationEngine(bool newDb, Action<Exception> onException)
        {
            var connector = new Connector(newDb);
            connector.OnException = onException;
            connector.DumbQuery();
            KontoManager = new ManagerKont(connector);
            PacjentManager = new ManagerPacjentow(connector);
            SlownikBadanManager = new ManagerTypowBadan(connector);
            BadanieLaboratoryjneManager = new ManagerBadanLaboratoryjnych(connector);
            LekarzManager = new ManagerLekarzy(connector);
            LaborantManager = new ManagerLaborantow(connector);
            WizytaManager = new ManagerWizyt(connector);
            BadanieFizyczneManager = new ManagerBadanFizycznych(connector);
            ReportGenerator = new ReportGenerator(connector);
        }
    }
}
