﻿using DataLayer;
using DataLayer.Connection;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerLaborantow
    {

        public class PoszukiwaczLaborantow : IPoszukiwacz<Laborant>
        {
            private Connector connector;
            public int? idLaboranta = null;

            public PoszukiwaczLaborantow(ManagerLaborantow m)
            {
                connector = m.connector;
            }

            public PoszukiwaczLaborantow IdLaboranta(int id)
            {
                this.idLaboranta = id;
                return this;
            }

            public void Wykonaj(Action<Laborant> action)
            {
                connector.ExecuteTransaction(session =>
                {
                    var query = session.Query<Laborant>();
                    if (idLaboranta != null)
                        query = query.Where(x => x.Id == idLaboranta.Value);

                    var laboranci = query.ToList();
                    laboranci.Sort((x, y) =>
                    {
                        int result = x.Nazwisko.CompareTo(y.Nazwisko);
                        if (result == 0)
                            result = x.Imie.CompareTo(y.Imie);
                        return result;
                    });
                    foreach (var l in laboranci)
                        action(l);
                });
            }
        }


        public ManagerLaborantow(Connector connector)
        {
            this.connector = connector;
        }


        private Connector connector;

    }


}
