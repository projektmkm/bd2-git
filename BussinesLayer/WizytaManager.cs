﻿using DataLayer;
using DataLayer.Connection;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerWizyt
    {
        public ManagerWizyt(Connector connector)
        {
            this.connector = connector;
        }

        public class PoszukiwaczWizyt : IPoszukiwacz<Wizyta>
        {
            private Connector connector;
            private int? idWizyty = null;
            private int? idLekarza = null;
            private int? idPacjenta = null;
            private bool? posiadaBadaniaLaboratoryjne = null;
            private bool? posiadaBadaniaFizyczne = null;
            private string imiePacjenta = null;
            private string nazwiskoPacjenta = null;
            private string diagnoza = null;
            private DateTime? najwczesniejszaDataRejestracji = null;
            private DateTime? najpozniejszaDataRejestracji = null;
            private DateTime? najwczesniejszaDataZakonczenia = null;
            private DateTime? najpozniejszaDataZakonczenia = null;
            private StanWizyty? stanWizyty = null;

            public PoszukiwaczWizyt(ManagerWizyt managerWizyt)
            {
                this.connector = managerWizyt.connector;
            }

            public PoszukiwaczWizyt IdWizyty(int id)
            {
                idWizyty = id;
                return this;
            }

            public PoszukiwaczWizyt IdLekarza(int id)
            {
                idLekarza = id;
                return this;
            }

            public PoszukiwaczWizyt IdPacjenta(int id)
            {
                idPacjenta = id;
                return this;
            }

            public PoszukiwaczWizyt PosiadaBadaniaLaboratoryjne(bool b)
            {
                posiadaBadaniaLaboratoryjne = b;
                return this;
            }

            public PoszukiwaczWizyt PosiadaBadaniaFizyczne(bool b)
            {
                posiadaBadaniaFizyczne = b;
                return this;
            }

            public PoszukiwaczWizyt ImiePacjenta(string imie)
            {
                imiePacjenta = imie;
                return this;
            }

            public PoszukiwaczWizyt NazwiskoPacjenta(string nazwisko)
            {
                nazwiskoPacjenta = nazwisko;
                return this;
            }

            public PoszukiwaczWizyt NajwczesniejszaDataRejestracji(DateTime data)
            {
                najwczesniejszaDataRejestracji = data;
                return this;
            }

            public PoszukiwaczWizyt NajpozniejszaDataRejestracji(DateTime data)
            {
                najpozniejszaDataRejestracji = data;
                return this;
            }

            public PoszukiwaczWizyt NajwczesniejszaDataZakonczenia(DateTime data)
            {
                najwczesniejszaDataZakonczenia = data;
                return this;
            }

            public PoszukiwaczWizyt NajpozniejszaDataZakonczenia(DateTime data)
            {
                najpozniejszaDataZakonczenia = data;
                return this;
            }

            public PoszukiwaczWizyt Stan(StanWizyty stan)
            {
                this.stanWizyty = stan;
                return this;
            }

            public PoszukiwaczWizyt Diagnoza(string diagnoza)
            {
                this.diagnoza = diagnoza;
                return this;
            }

            public void Wykonaj(Action<Wizyta> action)
            {
                connector.ExecuteTransaction(session =>
                {
                    var query = session.Query<Wizyta>();
                    if (idWizyty != null)
                        query = query.Where(x => x.IdWizyty == idWizyty.Value);
                    if(idLekarza != null)
                        query = query.Where(x => x.Lekarz.Id == idLekarza.Value);
                    if (idPacjenta != null)
                        query = query.Where(x => x.Pacjent.Id == idPacjenta.Value);
                    if(posiadaBadaniaLaboratoryjne != null)
                    {
                        if(posiadaBadaniaLaboratoryjne == true)
                            query = query.Where(x => x.BadaniaLaboratoryjne.Count > 0);
                        else
                            query = query.Where(x => x.BadaniaLaboratoryjne.Count == 0);
                    }
                    if (posiadaBadaniaFizyczne != null)
                    {
                        if (posiadaBadaniaFizyczne == true)
                            query = query.Where(x => x.BadaniaFizyczne.Count > 0);
                        else
                            query = query.Where(x => x.BadaniaFizyczne.Count == 0);
                    }
                    if (!string.IsNullOrEmpty(imiePacjenta))
                        query = query.Where(x => x.Pacjent.Imie.ToLower().Contains(imiePacjenta.ToLower()));
                    if (!string.IsNullOrEmpty(nazwiskoPacjenta))
                        query = query.Where(x => x.Pacjent.Nazwisko.ToLower().Contains(nazwiskoPacjenta.ToLower()));
                    if (!string.IsNullOrEmpty(diagnoza))
                        query = query.Where(x => x.Diagnoza.ToLower().Contains(diagnoza.ToLower()));
                    if (najwczesniejszaDataRejestracji != null)
                        query = query.Where(x => x.DataRejestracji.Date >= najwczesniejszaDataRejestracji.Value.Date);
                    if (najpozniejszaDataRejestracji != null)
                        query = query.Where(x => x.DataRejestracji.Date <= najpozniejszaDataRejestracji.Value.Date);
                    if (najwczesniejszaDataZakonczenia != null)
                        query = query.Where(x => (x.DataZakonczenia != null) && (x.DataZakonczenia.Value.Date >= najwczesniejszaDataZakonczenia.Value.Date));
                    if (najpozniejszaDataZakonczenia != null)
                        query = query.Where(x => (x.DataZakonczenia != null) && (x.DataZakonczenia.Value.Date <= najpozniejszaDataZakonczenia.Value.Date));
                    if (stanWizyty != null)
                        query = query.Where(x => stanWizyty.Value == x.Stan);

                    var result = query.ToList();
                    foreach (var wizyta in result)
                        action(wizyta);
                });
            }
        }

        public void AnulujWizyte(int idWizyty, string opis = null)
        {
            connector.ExecuteTransaction(session => {
                var wizyta = session.Get<Wizyta>(idWizyty);
                if (wizyta != null)
                {
                    wizyta.Stan = StanWizyty.Anulowana;
                    if(opis != null)
                        wizyta.Opis = opis;
                    wizyta.Diagnoza = string.Empty;
                    wizyta.DataZakonczenia = DateTime.Now;
                    session.Update(wizyta);
                }
            });
        }

        public void RozpocznijWizyte(int idWizyty)
        {
            connector.ExecuteTransaction(session => {
                var wizyta = session.Get<Wizyta>(idWizyty);
                if (wizyta != null)
                {
                    if (wizyta.Stan == StanWizyty.Zarejestrowana)
                    {
                        wizyta.Stan = StanWizyty.WToku;
                        session.Update(wizyta);
                    }
                }
            });
        }

        public void ZakonczWizyte(int idWizyty, string opis, string diagnoza)
        {
            connector.ExecuteTransaction(session => {
                var wizyta = session.Get<Wizyta>(idWizyty);
                if (wizyta != null)
                {
                    wizyta.Stan = StanWizyty.Wykonana;
                    wizyta.Diagnoza = diagnoza;
                    wizyta.Opis = opis;
                    wizyta.DataZakonczenia = DateTime.Now;
                    session.Update(wizyta);
                }
            });
        }

        public void DodajBadanieFizykalne(int idWizyty, int idTypuBadania, string wynik)
        {
            connector.ExecuteTransaction(session =>
            {
                var wizyta = session.Get<Wizyta>(idWizyty);
                var typBadania = session.Get<TypBadania>(idTypuBadania);
                if((wizyta != null)&&(typBadania != null))
                {
                    var badanie = new BadanieFizyczne();
                    badanie.Wizyta = wizyta;
                    badanie.Wynik = wynik;
                    badanie.TypBadania = typBadania;
                    badanie.DataWykonania = DateTime.Now;
                    session.Save(badanie);
                    wizyta.BadaniaFizyczne.Add(badanie);
                    session.Update(wizyta);
                }
            });
        }

        public void DodajBadanieLaboratoryjne(int idWizyty, int idTypuBadania, string uwagiLekarza)
        {
            connector.ExecuteTransaction(session =>
            {
                var wizyta = session.Get<Wizyta>(idWizyty);
                var typBadania = session.Get<TypBadania>(idTypuBadania);
                if((wizyta != null) &&(typBadania != null))
                {
                    var badanie = new BadanieLaboratoryjne();
                    badanie.Wizyta = wizyta;
                    badanie.TypBadania = typBadania;
                    badanie.DataZlecenia = DateTime.Now;
                    badanie.Stan = StanBadania.Zlecone;
                    badanie.UwagiLekarza = uwagiLekarza;
                    session.Save(badanie);
                    wizyta.BadaniaLaboratoryjne.Add(badanie);
                    session.Update(wizyta);
                }
            });
        }

        public void EdytujWizyte(int idWizyty, int idLekarza, DateTime dataWizyty)
        {
            connector.ExecuteTransaction(session => {
                var wizyta = session.Get<Wizyta>(idWizyty);
                if(wizyta != null)
                {
                    wizyta.DataRejestracji = dataWizyty;
                    if(wizyta.Lekarz.Id != idLekarza)
                    {
                        var poprzedniLekarz = session.Get<Lekarz>(wizyta.Lekarz.Id);
                        var nowyLekarz = session.Get<Lekarz>(idLekarza);
                        poprzedniLekarz.Wizyty.Remove(wizyta);
                        nowyLekarz.Wizyty.Add(wizyta);
                        wizyta.Lekarz = nowyLekarz;
                        session.Update(nowyLekarz);
                        session.Update(poprzedniLekarz);
                    }
                    session.Update(wizyta);
                }
            });
        }

        public void DodajWizyte(int idRejestratora, int idLekarza, int idPacjenta, DateTime dataWizyty)
        {
            connector.ExecuteTransaction(session => {
                var pacjent = session.Get<Pacjent>(idPacjenta);
                var lekarz = session.Get<Lekarz>(idLekarza);
                var rejestrator = session.Get<Rejestrator>(idRejestratora);
                if ((pacjent != null) && (lekarz != null) && (rejestrator != null))
                {
                    var wizyta = new Wizyta
                    {
                        Lekarz = lekarz,
                        Pacjent = pacjent,
                        Rejestrator = rejestrator,
                        DataRejestracji = dataWizyty,
                        Stan = StanWizyty.Zarejestrowana
                    };
                    lekarz.Wizyty.Add(wizyta);
                    pacjent.Wizyty.Add(wizyta);
                    rejestrator.Wizyty.Add(wizyta);
                    session.Save(wizyta);
                    session.Update(pacjent);
                    session.Update(lekarz);
                    session.Update(rejestrator);
                }
            });
        }

        private Connector connector;
    }
}
