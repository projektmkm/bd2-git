﻿using BusinessLayer;
using DataLayer;
using DataLayer.Connection;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerTypowBadan
    {
        public ManagerTypowBadan(Connector connector)
        {
            this.connector = connector;
        }

        public class PoszukiwaczTypowBadan : IPoszukiwacz<TypBadania>
        {
            private Connector connector;
            private BadanieTyp? typBadania = null;
            private int? idBadania = null;

            public PoszukiwaczTypowBadan(ManagerTypowBadan m)
            {
                this.connector = m.connector;
            }

            public PoszukiwaczTypowBadan TypBadania(BadanieTyp t)
            {
                typBadania = t;
                return this;
            }

            public PoszukiwaczTypowBadan IdBadania(int id)
            {
                idBadania = id;
                return this;
            }

            public void Wykonaj(Action<TypBadania> action)
            {
                connector.ExecuteTransaction(session =>
                {
                    var query = session.Query<TypBadania>();
                    if (typBadania != null)
                        query = query.Where(x => typBadania.Value == x.Typ);
                    if (idBadania != null)
                        query = query.Where(x => x.Id == idBadania.Value);

                    var result = query.ToList();
                    foreach (var b in result)
                        action(b);
                });
            }
        }

        public bool MoznaUsunac(int id)
        {
            bool result = false;
            connector.ExecuteTransaction(session =>
            {
                var badFiz = session.Query<BadanieFizyczne>()
                .Where(x => x.TypBadania.Id == id).FirstOrDefault();
                if(badFiz != null)
                {
                    result = false;
                    return;
                }
                var badLab = session.Query<BadanieLaboratoryjne>()
               .Where(x => x.TypBadania.Id == id).FirstOrDefault();
                if (badLab != null)
                {
                    result = false;
                    return;
                }
                result = true;
            });
            return result;
        }

        public void UsunTypBadania(int id)
        {
            connector.ExecuteTransaction(session => {
                var b = session.Get<TypBadania>(id);
                if(b != null)
                    session.Delete(b);
            });
        }

        public int DodajTypBadania(string kod, string nazwa, string opis, BadanieTyp typ)
        {
            int result = 0;
            connector.ExecuteTransaction(session => {
                TypBadania badanie = new TypBadania { Kod = kod, Nazwa = nazwa, Opis = opis, Typ = typ };
                session.Save(badanie);
                result = badanie.Id;
            });
            return result;
        }

        public void EdytujTypBadania(int id, string kod, string nazwa, string opis, BadanieTyp typ)
        {
            connector.ExecuteTransaction(session => {
                TypBadania badanie = session.Get<TypBadania>(id);
                if (badanie != null)
                {
                    badanie.Kod = kod;
                    badanie.Nazwa = nazwa;
                    badanie.Opis = opis;
                    session.Update(badanie);
                }
            });
        }

        public bool KodWolny(string kod)
        {
            bool result = false;
            connector.ExecuteTransaction(session =>
            {
                var badanie = session.Query<TypBadania>()
                    .Where(x => x.Kod == kod)
                    .SingleOrDefault();
                result = (badanie == null) ? true : false;
            });
            return result;
        }

        private Connector connector;
    }
}
