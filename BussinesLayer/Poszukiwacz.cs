﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IPoszukiwacz<T>
    {
        void Wykonaj(Action<T> action);
    }
}
