﻿using DataLayer;
using DataLayer.Connection;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ManagerBadanLaboratoryjnych
    {

        public class PoszukiwaczBadanLaboratoryjnych : IPoszukiwacz<BadanieLaboratoryjne>
        {
            private Connector connector;
            private int? idBadania = null;
            private int? idLekarza = null;
            private int? idLaboranta = null;
            private int? idKierownika = null;
            private StanBadania? stan = null;
            private string imiePacjenta = null;
            private string nazwiskoPacjenta = null;
            private DateTime? najwczesniejszaDataZlecenia = null;
            private DateTime? najpozniejszaDataZlecenia = null;
            private DateTime? najwczesniejszaDataWykonania = null;
            private DateTime? najpozniejszaDataWykonania = null;
            private DateTime? najwczesniejszaDataZatwierdzenia = null;
            private DateTime? najpozniejszaDataZatwierdzenia = null;
            private DateTime? najwczesniejszaDataAnulowania = null;
            private DateTime? najpozniejszaDataAnulowania = null;

            public PoszukiwaczBadanLaboratoryjnych(ManagerBadanLaboratoryjnych m)
            {
                this.connector = m.connector;
            }

            public PoszukiwaczBadanLaboratoryjnych IdBadania(int id)
            {
                idBadania = id;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych IdLekarza(int id)
            {
                idLekarza = id;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych IdLaboranta(int id)
            {
                idLaboranta = id;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych IdKierownika(int id)
            {
                idKierownika = id;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych Stan(StanBadania stan)
            {
                this.stan = stan;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych ImiePacjenta(string imie)
            {
                imiePacjenta = imie;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NazwiskoPacjenta(string nazwisko)
            {
                nazwiskoPacjenta = nazwisko;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajwczesniejszaDataZlecenia(DateTime data)
            {
                najwczesniejszaDataZlecenia = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajpozniejszaDataZlecenia(DateTime data)
            {
                najpozniejszaDataZlecenia = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajwczesniejszaDataWykonania(DateTime data)
            {
                najwczesniejszaDataWykonania = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajpozniejszaDataWykonania(DateTime data)
            {
                najpozniejszaDataWykonania = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajwczesniejszaDataZatwierdzenia(DateTime data)
            {
                najwczesniejszaDataZatwierdzenia = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajpozniejszaDataZatwierdzenia(DateTime data)
            {
                najpozniejszaDataZatwierdzenia = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajwczesniejszaDataAnulowania(DateTime data)
            {
                najwczesniejszaDataAnulowania = data;
                return this;
            }

            public PoszukiwaczBadanLaboratoryjnych NajpozniejszaDataAnulowania(DateTime data)
            {
                najpozniejszaDataAnulowania = data;
                return this;
            }

            public void Wykonaj(Action<BadanieLaboratoryjne> action)
            {
                connector.ExecuteTransaction(session => {
                    var query = session.Query<BadanieLaboratoryjne>();
                    if (idBadania != null)
                        query = query.Where(x => x.Id == idBadania.Value);
                    if (idLekarza != null)
                        query = query.Where(x => x.Wizyta.Lekarz.Id == idLekarza.Value);
                    if (idLaboranta != null)
                        query = query.Where(x => (x.Laborant != null)&&(x.Laborant.Id == idLaboranta.Value));
                    if (idKierownika != null)
                        query = query.Where(x => (x.KierownikLaboratorium != null) && (x.KierownikLaboratorium.Id == idKierownika.Value));
                    if (stan != null)
                        query = query.Where(x => stan.Value == x.Stan);
                    if (!string.IsNullOrEmpty(imiePacjenta))
                        query = query.Where(x => x.Wizyta.Pacjent.Imie.ToLower().Contains(imiePacjenta.ToLower()));
                    if (!string.IsNullOrEmpty(nazwiskoPacjenta))
                        query = query.Where(x => x.Wizyta.Pacjent.Nazwisko.ToLower().Contains(nazwiskoPacjenta.ToLower()));
                    if (najwczesniejszaDataZlecenia != null)
                        query = query.Where(x => x.DataZlecenia.Date >= najwczesniejszaDataZlecenia.Value.Date);
                    if (najpozniejszaDataZlecenia != null)
                        query = query.Where(x => x.DataZlecenia.Date <= najpozniejszaDataZlecenia.Value.Date);
                    if(najwczesniejszaDataWykonania != null)
                        query = query.Where(x => (x.DataWykonania != null) && (x.DataWykonania.Value.Date >= najwczesniejszaDataWykonania.Value.Date));
                    if (najpozniejszaDataWykonania != null)
                        query = query.Where(x => (x.DataWykonania != null) && (x.DataWykonania.Value.Date <= najpozniejszaDataWykonania.Value.Date));
                    if (najwczesniejszaDataZatwierdzenia != null)
                        query = query.Where(x => (x.DataZatwierdzenia != null) && (x.DataZatwierdzenia.Value.Date >= najwczesniejszaDataZatwierdzenia.Value.Date));
                    if (najpozniejszaDataZatwierdzenia != null)
                        query = query.Where(x => (x.DataZatwierdzenia != null) && (x.DataZatwierdzenia.Value.Date <= najpozniejszaDataZatwierdzenia.Value.Date));
                    if (najwczesniejszaDataAnulowania != null)
                        query = query.Where(x => (x.DataAnulowania != null) && (x.DataAnulowania.Value.Date >= najwczesniejszaDataAnulowania.Value.Date));
                    if (najpozniejszaDataAnulowania != null)
                        query = query.Where(x => (x.DataAnulowania != null) && (x.DataAnulowania.Value.Date <= najpozniejszaDataAnulowania.Value.Date));

                    var listaBadan = query.ToList();
                    foreach (var b in listaBadan)
                        action(b);
                });
            }
        }


        public ManagerBadanLaboratoryjnych(Connector connector)
        {
            this.connector = connector;
        }

        public void ZatwierdzBadanie(int idBadania, int idKierownika, string uwagiKierownika)
        {
            connector.ExecuteTransaction(session => {
                var badanie = session.Get<BadanieLaboratoryjne>(idBadania);
                var kierownik = session.Get<KierownikLaboratorium>(idKierownika);
                if ((badanie != null) && (kierownik != null))
                {
                    badanie.UwagiKierownika = uwagiKierownika;
                    badanie.Stan = StanBadania.Zatwierdzone;
                    badanie.DataZatwierdzenia = DateTime.Now;
                    badanie.KierownikLaboratorium = kierownik;
                    kierownik.Badania.Add(badanie);
                    session.Update(kierownik);
                    session.Update(badanie);
                }
            });
        }

        public void RozpocznijBadanie(int idBadania, int idLaboranta)
        {
            connector.ExecuteTransaction(session => {
                var badanie = session.Get<BadanieLaboratoryjne>(idBadania);
                var laborant = session.Get<Laborant>(idLaboranta);
                if ((badanie != null) && (laborant != null))
                {
                    if (badanie.Stan == StanBadania.Zlecone)
                    {
                        badanie.Stan = StanBadania.Wykonywane;
                        badanie.Laborant = laborant;
                        laborant.Badania.Add(badanie);
                        session.Update(laborant);
                        session.Update(badanie);
                    }
                }
            });
        }

        public void ZakonczBadanie(int idBadania, string wynik)
        {
            connector.ExecuteTransaction(session => {
                var badanie = session.Get<BadanieLaboratoryjne>(idBadania);
                if (badanie != null)
                {
                    badanie.Wynik = wynik;
                    badanie.Stan = StanBadania.Wykonane;
                    badanie.DataWykonania = DateTime.Now;
                    session.Update(badanie);
                }
            });
        }

        /// <summary>
        /// Anulowanie badania - kierownik.
        /// </summary>
        /// <param name="idBadania"></param>
        /// <param name="idKierownika"></param>
        /// <param name="uwagiKierownika"></param>
        public void AnulujBadanieKierownik(int idBadania, int idKierownika, string uwagiKierownika)
        {
            connector.ExecuteTransaction(session => {
                var badanie = session.Get<BadanieLaboratoryjne>(idBadania);
                var kierownik = session.Get<KierownikLaboratorium>(idKierownika);
                if ((badanie != null) && (kierownik != null))
                {
                    badanie.UwagiKierownika = uwagiKierownika;
                    badanie.Stan = StanBadania.AnulowaneKierownik;
                    badanie.DataAnulowania = DateTime.Now;
                    badanie.KierownikLaboratorium = kierownik;
                    kierownik.Badania.Add(badanie);
                    session.Update(kierownik);
                    session.Update(badanie);
                }
            });
        }

        /// <summary>
        /// Anulowanie badania - laborant.
        /// </summary>
        /// <param name="idBadania"></param>
        /// <param name="idLaboranta"></param>
        public void AnulujBadanieLaborant(int idBadania, int idLaboranta, string przyczynaAnulowania)
        {
            connector.ExecuteTransaction(session => {
                var badanie = session.Get<BadanieLaboratoryjne>(idBadania);
                var Laborant = session.Get<Laborant>(idLaboranta);
                if ((badanie != null) && (Laborant != null))
                {
                    badanie.Wynik = przyczynaAnulowania;
                    badanie.Stan = StanBadania.AnulowaneLaborant;
                    badanie.DataAnulowania = DateTime.Now;
                    badanie.Laborant = Laborant;
                    Laborant.Badania.Add(badanie);
                    session.Update(Laborant);
                    session.Update(badanie);
                }
            });
        }

        private Connector connector;
    }
}
