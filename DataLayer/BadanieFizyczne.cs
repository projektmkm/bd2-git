﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class BadanieFizyczne
    {
        public virtual int Id { get; set; }
        public virtual DateTime DataWykonania { get; set; }
        public virtual string Wynik {get; set; }
        public virtual Wizyta Wizyta { get; set; }
        public virtual TypBadania TypBadania { get; set; }
    }
    public class BadanieFizyczneMap : ClassMap<BadanieFizyczne>
    {
        public BadanieFizyczneMap()
        {
            Id(x => x.Id);
            Map(x => x.DataWykonania)
                .Not.Nullable();
            Map(x => x.Wynik)
                .CustomType("StringClob")
                .Not.Nullable();
            References(x => x.Wizyta)
                .Not.Nullable();
            References(x => x.TypBadania)
                .Not.Nullable();
        }
    }
}
