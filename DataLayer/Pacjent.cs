﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Pacjent
    {
        public virtual int Id { get; set; }
        public virtual string Imie { get; set; }
        public virtual string Nazwisko {get;set;}
        public virtual string Pesel { get; set; }
        public virtual string Adres { get; set; }
        public virtual IList<Wizyta> Wizyty { get; set; }
        public Pacjent()
        {
            Wizyty = new List<Wizyta>();
        }
    }
    public class PacjentMap : ClassMap<Pacjent>
    {
        public PacjentMap()
        {
            Id(x => x.Id);
            Map(x => x.Imie)
                .Not.Nullable();
            Map(x => x.Nazwisko)
                .Not.Nullable();
            Map(x => x.Pesel);
            Map(x => x.Adres)
                .CustomType("StringClob");
            HasMany(x => x.Wizyty);
        }
    }
}
