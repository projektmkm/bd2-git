﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public abstract class Konto
    {
        public virtual int Id { get; set; }
        public virtual string Login { get; set; }
        public virtual string HasloHash { get; set; }
        public virtual string Salt { get; set; }
        public virtual DateTime? DataKonca { get; set; }
        public virtual string Imie { get; set; }
        public virtual string Nazwisko { get; set; }
        public abstract KontoRola GetRole();
        public virtual string Haslo { set { NoweHaslo(value); } }
        public Konto()
        {
        }
        public virtual bool SprawdzHaslo(string haslo)
        {
            byte[] passHash = GenerujHash(haslo, System.Convert.FromBase64String(Salt));
            return Enumerable.SequenceEqual<byte>(System.Convert.FromBase64String(HasloHash), passHash);
        }
        protected virtual void NoweHaslo(string haslo)
        {
            if (haslo == null || haslo.Length == 0)
            {
                throw new ArgumentException("Puste haslo");
            }
            byte[] salt = new byte[10];
            lock (rnd)
            {
                rnd.NextBytes(salt);
            }
            Salt = System.Convert.ToBase64String(salt);
            HasloHash = System.Convert.ToBase64String(GenerujHash(haslo, salt));
        }
        private static byte[] GenerujHash(string haslo, byte[] sol)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] haslob = System.Text.Encoding.UTF8.GetBytes(haslo);
            byte[] hasloISol = new byte[haslob.Length + sol.Length];
            Array.Copy(haslob, 0, hasloISol, 0, haslob.Length);
            Array.Copy(sol, 0, hasloISol, haslob.Length, sol.Length);
            return algorithm.ComputeHash(hasloISol);
        }
        private static Random rnd = new Random();
    }

    public class KontoMap : ClassMap<Konto>
    {
        public KontoMap()
        {
            Id(x => x.Id);
            Map(x => x.Login)
                .Not.Nullable();
            Map(x => x.HasloHash)
                .Not.Nullable();
            Map(x => x.Salt)
                .Not.Nullable();
            Map(x => x.DataKonca);
            Map(x => x.Imie)
                .Not.Nullable();
            Map(x => x.Nazwisko)
                .Not.Nullable();
        }
    }

    public enum KontoRola
    {
        Administrator,
        KierownikLaboratorium,
        Laborant,
        Lekarz,
        Rejestrator
    }
}
