﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Rejestrator : Konto
    {
        public virtual IList<Wizyta> Wizyty { get; set; }
        public override KontoRola GetRole()
        {
            return KontoRola.Rejestrator;
        }
        public Rejestrator()
        {
            Wizyty = new List<Wizyta>();
        }
    }
    public class RejestratorMap : SubclassMap<Rejestrator>
    {
        public RejestratorMap()
        {
            HasMany(x => x.Wizyty);
        }
    }
}
