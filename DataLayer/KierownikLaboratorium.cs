﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class KierownikLaboratorium : Konto
    {
        public virtual IList<BadanieLaboratoryjne> Badania { get; set; }
        public override KontoRola GetRole()
        {
            return KontoRola.KierownikLaboratorium;
        }
        public KierownikLaboratorium()
        {
            Badania = new List<BadanieLaboratoryjne>();
        }
    }
    public class KierownikLabMap : SubclassMap<KierownikLaboratorium>
    {
        public KierownikLabMap()
        {
            HasMany(x => x.Badania);
        }
    }
}
