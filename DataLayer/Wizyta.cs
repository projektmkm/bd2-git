﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public enum StanWizyty
    {
        Zarejestrowana,
        Wykonana,
        Anulowana,
        WToku
    }
    public class Wizyta
    {
        public virtual int IdWizyty { get; set; }
        public virtual string Opis { get; set; }
        public virtual string Diagnoza { get; set; }
        public virtual StanWizyty Stan { get; set; }
        public virtual DateTime DataRejestracji { get; set; }
        public virtual DateTime? DataZakonczenia { get; set; }
        public virtual Rejestrator Rejestrator { get; set; }
        public virtual Lekarz Lekarz { get; set; }
        public virtual Pacjent Pacjent { get; set; }
        public virtual IList<BadanieFizyczne> BadaniaFizyczne { get; set; }
        public virtual IList<BadanieLaboratoryjne> BadaniaLaboratoryjne { get; set; }
        public Wizyta()
        {
            BadaniaLaboratoryjne = new List<BadanieLaboratoryjne>();
            BadaniaFizyczne = new List<BadanieFizyczne>();
        }
    }

    public class WizytaMap : ClassMap<Wizyta>
    {
        public WizytaMap()
        {
            Id(x => x.IdWizyty);
            Map(x => x.Opis)
                .CustomType("StringClob");
            Map(x => x.Diagnoza)
                .CustomType("StringClob");
            Map(x => x.Stan);
            Map(x => x.DataRejestracji)
                .Not.Nullable();
            Map(x => x.DataZakonczenia);
            References(x => x.Rejestrator)
                .Not.Nullable();
            References(x => x.Lekarz)
                .Not.Nullable();
            References(x => x.Pacjent)
                .Not.Nullable();
            HasMany(x => x.BadaniaFizyczne);
            HasMany(x => x.BadaniaLaboratoryjne);

        }
    }
}
