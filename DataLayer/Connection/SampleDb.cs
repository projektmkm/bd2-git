﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Connection
{
    public class SampleDb
    {
        public SampleDb(ISession session)
        {
            this.session = session;
        }
        public void CreateAdmin(string user = "admin", string password = "admin")
        {
            session.Save(new Administrator { Imie = "Jan", Nazwisko = "Kowalski", Haslo = password, Login = user, DataKonca = new DateTime(9000,12,31,23,59,59) });
        }
        public void CreateSampleDb()
        {
            CreateAdmin();
            var lekarz1 = new Lekarz { Imie = "Adebe", Nazwisko = "Makulebe", Haslo = "lekarz", Login = "lekarz", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };
            var lekarz2 = new Lekarz { Imie = "Andrzej", Nazwisko = "Śmietana", Haslo = "lekarz2", Login = "lekarz2", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };
            var rejestrator = new Rejestrator { Imie = "Adam", Nazwisko = "Miauczyński", Login = "rejestrator", Haslo = "rejestrator", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };
            var kierownik = new KierownikLaboratorium { Imie = "Paweł", Nazwisko = "Boligłowa", Login = "kierownik", Haslo = "kierownik", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };
            var laborant1 = new Laborant { Imie = "Juliusz", Nazwisko = "Mickiewicz", Haslo = "laborant", Login = "laborant", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };
            var laborant2 = new Laborant { Imie = "Izabela", Nazwisko = "Łęcka", Haslo = "laborant2", Login = "laborant2", DataKonca = new DateTime(2019, 12, 31, 23, 59, 59) };

            session.Save(lekarz1);
            session.Save(lekarz2);
            session.Save(rejestrator);
            session.Save(kierownik);
            session.Save(laborant1);
            session.Save(laborant2);

            var pacjent1 = new Pacjent { Imie = "Andrzej", Nazwisko = "Nowak", Adres = "Warszwa Wiejska 123", Pesel = "12345678900" };
            var pacjent2 = new Pacjent { Imie = "Grzegorz", Nazwisko = "Brzęczyszczykiewicz", Adres = "Chrząszczyżewoszyce Polna 1 ", Pesel = "23456789100" };
            var pacjent3 = new Pacjent { Imie = "Bartłomiej", Nazwisko = "Nowakowski", Adres = "Gdańsk Sopocka 3", Pesel = "91234567800" };
            var pacjent4 = new Pacjent { Imie = "Tomasz", Nazwisko = "Kwasigroch", Adres = "Warszwa Krakowska 48/50 123", Pesel = "12345678910" };

            session.Save(pacjent1);
            session.Save(pacjent2);
            session.Save(pacjent3);
            session.Save(pacjent4);

            var TypBadaniaL1 = new TypBadania { Nazwa = "Badanie krwi", Kod = "KR", Opis = "kompleksowe badanie krwi", Typ = BadanieTyp.Laboratoryjne };
            var TypBadaniaL2 = new TypBadania { Nazwa = "Badanie moczu", Kod = "MO", Opis = "kompleksowe badanie moczu", Typ = BadanieTyp.Laboratoryjne };
            var TypBadaniaF1 = new TypBadania { Nazwa = "Badanie czucia w kończynach", Kod = "CO", Opis = "sprawdzenie czy implulsy nerwowe z kończyn pacjenta docierają do mózgu", Typ = BadanieTyp.Fizyczne };
            var TypBadaniaF2 = new TypBadania { Nazwa = "Badanie refleksu", Kod = "RE", Opis = "badanie refleksu pacjenta poprzez walenie młotkiem w kolano", Typ = BadanieTyp.Fizyczne };

            session.Save(TypBadaniaF1);
            session.Save(TypBadaniaF2);
            session.Save(TypBadaniaL1);
            session.Save(TypBadaniaL2);

            var wizyta11 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-100), DataZakonczenia = DateTime.Now.AddDays(-50), Lekarz = lekarz1, Opis = "Leczenie choroby przewlekłej", Pacjent = pacjent1, Rejestrator = rejestrator, Stan = StanWizyty.Wykonana, Diagnoza = "skierowanie na dalsze badania" };
            var wizyta12 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-50), DataZakonczenia = DateTime.Now.AddDays(-50), Lekarz = lekarz2, Opis = "Sprawdzenie  stanu pacjenta", Pacjent = pacjent1, Rejestrator = rejestrator, Stan = StanWizyty.Anulowana, Diagnoza = "" };
            var wizyta13 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-10), Lekarz = lekarz1, Opis = "Wizyta kontrolna", Pacjent = pacjent1, Rejestrator = rejestrator, Stan = StanWizyty.Zarejestrowana, Diagnoza = "" };
            session.Save(wizyta11);
            session.Save(wizyta12);
            session.Save(wizyta13);
            wizyta11.BadaniaFizyczne.Add(new BadanieFizyczne { DataWykonania = DateTime.Now.AddDays(-50), TypBadania = TypBadaniaF1, Wizyta = wizyta11, Wynik = "wszystko w normie" });
            wizyta11.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne { 
                Stan = StanBadania.AnulowaneLaborant, 
                DataAnulowania = DateTime.Now.AddDays(-50), 
                TypBadania = TypBadaniaL1, 
                Wizyta = wizyta11, 
                Laborant = laborant1, 
                Wynik = "Anulowano", 
                DataZlecenia = DateTime.Now.AddDays(-50) 
            });
            wizyta11.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne { 
                Stan = StanBadania.Zlecone, 
                TypBadania = TypBadaniaL2, 
                Wizyta = wizyta11, 
                DataZlecenia = DateTime.Now.AddDays(-50),
                UwagiLekarza = "Tylko dokładnie..."
            });
            wizyta11.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne { 
                Stan = StanBadania.Wykonane, 
                DataWykonania = DateTime.Now.AddDays(-50), 
                TypBadania = TypBadaniaL1, 
                Wizyta = wizyta11, 
                Laborant = laborant1, 
                Wynik = "pacjent posiada wysokie stężenie alkocholu we krwi", 
                DataZlecenia = DateTime.Now.AddDays(-50)
            });
            wizyta11.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne {
                Stan = StanBadania.Zatwierdzone,
                DataWykonania = DateTime.Now.AddDays(-50), 
                TypBadania = TypBadaniaL2, 
                Wizyta = wizyta11,
                Laborant = laborant2, 
                Wynik = "znakomity refleks",
                DataZlecenia = DateTime.Now.AddDays(-50), 
                KierownikLaboratorium = kierownik,
                DataZatwierdzenia = DateTime.Now.AddDays(-45) 
            });
            session.Save(wizyta11.BadaniaFizyczne.First());
            foreach (var x in wizyta11.BadaniaLaboratoryjne)
            {
                session.Save(x);
            }

            session.Save(wizyta11);
            session.Save(wizyta12);
            session.Save(wizyta13);

            var wizyta21 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-90), DataZakonczenia = DateTime.Now.AddDays(-40), Lekarz = lekarz2, Opis = "leczenie choroby zakaźnej", Pacjent = pacjent2, Rejestrator = rejestrator, Stan = StanWizyty.Wykonana, Diagnoza = "skierowanie na dalsze badania" };
            var wizyta22 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-40), DataZakonczenia = DateTime.Now.AddDays(-40), Lekarz = lekarz1, Opis = "Kontrola rozwoju zakażenia", Pacjent = pacjent2, Rejestrator = rejestrator, Stan = StanWizyty.Anulowana, Diagnoza = "" };
            var wizyta23 = new Wizyta { DataRejestracji = DateTime.Now.AddDays(-10), Lekarz = lekarz2, Opis = "Przygotowanie do amputacji", Pacjent = pacjent2, Rejestrator = rejestrator, Stan = StanWizyty.Zarejestrowana, Diagnoza = "" };
            session.Save(wizyta21);
            session.Save(wizyta22);
            session.Save(wizyta23);
            wizyta21.BadaniaFizyczne.Add(new BadanieFizyczne { DataWykonania = DateTime.Now.AddDays(-50), TypBadania = TypBadaniaF1, Wizyta = wizyta11, Wynik = "wszystko w normie" });
            wizyta21.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne {
                Stan = StanBadania.AnulowaneKierownik,
                DataAnulowania = DateTime.Now.AddDays(-40), 
                TypBadania = TypBadaniaL1, 
                Wizyta = wizyta21, 
                KierownikLaboratorium = kierownik, 
                UwagiKierownika = "anulowano na prośbę pacjenta",
                DataZlecenia = DateTime.Now.AddDays(-40) 
            });
            wizyta21.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne {
                Stan = StanBadania.Zlecone,
                TypBadania = TypBadaniaL2, 
                Wizyta = wizyta21, 
                DataZlecenia = DateTime.Now.AddDays(-40) 
            });
            wizyta21.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne {
                Stan = StanBadania.Wykonane, 
                DataWykonania = DateTime.Now.AddDays(-40), 
                TypBadania = TypBadaniaL1,
                Wizyta = wizyta21,
                Laborant = laborant2, 
                Wynik = "brak czerwonych krwinek",
                DataZlecenia = DateTime.Now.AddDays(-40)
            });
            wizyta21.BadaniaLaboratoryjne.Add(new BadanieLaboratoryjne {
                Stan = StanBadania.Zatwierdzone, 
                DataWykonania = DateTime.Now.AddDays(-40),
                TypBadania = TypBadaniaL2,
                Wizyta = wizyta21,
                Laborant = laborant1, 
                Wynik = "pacjent to fajtłapa", 
                DataZlecenia = DateTime.Now.AddDays(-40), 
                KierownikLaboratorium = kierownik,
                DataZatwierdzenia = DateTime.Now.AddDays(-30)
            });
            session.Save(wizyta21.BadaniaFizyczne.First());
            foreach (var x in wizyta21.BadaniaLaboratoryjne)
            {
                session.Save(x);
            }

            session.Save(wizyta21);
            session.Save(wizyta22);
            session.Save(wizyta23);

        }
        private ISession session;
    }
}
