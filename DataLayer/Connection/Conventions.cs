﻿using FluentNHibernate;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using FluentNHibernate.MappingModel.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataLayer.Connection
{
    /// <summary>
    /// konwencja nazewnicza tabel
    /// </summary>
    public class TableAndColumnNameConvention : IClassConvention, IPropertyConvention, IHasManyConvention, IReferenceConvention, IJoinedSubclassConvention
    {
        public void Apply(IClassInstance instance)
        {
            instance.Table(ToUndescore(instance.EntityType.Name));
        }
        public static string ToUndescore(string n)
        {
            string result = "";
            foreach (char x in n)
            {
                if (Char.IsUpper(x) && result.Count() > 0)
                {
                    result += "_";
                }
                result += Char.ToUpper(x);
            }
            return result;
        }

        public void Apply(IPropertyInstance instance)
        {
            instance.Column(ToUndescore(instance.Property.Name));
        }

        public void Apply(IOneToManyCollectionInstance instance)
        {
            instance.Key.Column(ToUndescore(instance.EntityType.Name) + "_ID");
        }

        public void Apply(IManyToOneInstance instance)
        {
            instance.Column(ToUndescore(instance.Property.Name) + "_ID");
            instance.ForeignKey("FK_" + ToUndescore(instance.EntityType.Name + instance.Name));
        }


        public void Apply(IJoinedSubclassInstance instance)
        {
            instance.Table(ToUndescore(instance.EntityType.Name));
            instance.Key.Column(ToUndescore(instance.EntityType.BaseType.Name) + "_ID");
            instance.Key.ForeignKey("FK_" + ToUndescore(instance.EntityType.Name + instance.EntityType.BaseType.Name));
        }
    }


    /// <summary>
    /// konwencja jak przechowywać enumy
    /// </summary>
    public class EnumConvention : IUserTypeConvention
    {
        //bez tego enum jest jako string (lol)
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Property.PropertyType.IsEnum);
        }

        public void Apply(IPropertyInstance target)
        {
            target.CustomType(target.Property.PropertyType);
        }
    }
    public class IdConvention : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            instance.Column("ID");
            instance.GeneratedBy.Native();
        }
    }
}
