﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.SqlCommand;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

namespace DataLayer.Connection
{
    public class Connector
    {
        public static string Encrypt(string x)
        {
            var bytes = System.Text.Encoding.UTF7.GetBytes(x);
            return System.Convert.ToBase64String(bytes);
        }
        public static string Decrypt(string x)
        {
            try
            {
                var bytes = System.Convert.FromBase64String(x);
                return System.Text.Encoding.UTF7.GetString(bytes);
            }
            catch (Exception e)
            {
                return "";
            }
        }
        private enum DatabaseType
        {
            Sqlite,
            Mysql
        }
        private class ConnectionData
        {
            public string ConnectionString;
            public DatabaseType DatabaseType;
        }
        private ConnectionData GetConnectionData()
        {
            foreach (var x in Enum.GetValues(typeof(DatabaseType)))
            {
                DatabaseType type = (DatabaseType)x;
                string name = type.ToString().ToLower();
                var cs = ConfigurationManager.ConnectionStrings[name];
                if (cs != null)
                {
                    Console.WriteLine("encrypted connection string: " + Encrypt(cs.ConnectionString));
                    return new ConnectionData { DatabaseType = type, ConnectionString = cs.ConnectionString };
                }
                name += "_encrypted";
                cs = ConfigurationManager.ConnectionStrings[name];
                if (cs != null)
                {
                    return new ConnectionData { DatabaseType = type, ConnectionString = Decrypt(cs.ConnectionString) };
                }
            }
            throw new ArgumentException("no connection data");
        }
        public Connector ( bool createDb)
		{
			FluentConfiguration configuration = Fluently.Configure ();
            var connectionData = GetConnectionData();
            Console.WriteLine("using " + connectionData.DatabaseType.ToString());
			if (connectionData.DatabaseType == DatabaseType.Mysql) {
                configuration = configuration.Database(FluentNHibernate.Cfg.Db.MySQLConfiguration.Standard
                .ConnectionString(connectionData.ConnectionString)
                .Driver<MysqlDriver>()
                .AdoNetBatchSize(50));
			} else if(connectionData.DatabaseType == DatabaseType.Sqlite)  {
                    configuration = configuration.Database(SQLiteConfiguration.Standard
                    .UsingFile(connectionData.ConnectionString)
                    .Driver<SqliteDriver>()
                    .AdoNetBatchSize(50));
			}else {
				throw new ArgumentException ("CONNECTION STRING");
			}

			 sessionFactory = configuration.Mappings(m => m.FluentMappings.AddFromAssemblyOf<Lekarz>()
					.Conventions.Add<TableAndColumnNameConvention>()
					.Conventions.Add<EnumConvention>()
					.Conventions.Add<IdConvention>())
				.ExposeConfiguration(cfg =>
					{
						var schemaExport = new SchemaExport(cfg);
#if DEBUG
						if (createDb)
						{
							schemaExport.Drop(SqlLogger.GetInstance(), true);
                            schemaExport.Create(SqlLogger.GetInstance(), true);
						}
#endif
					}).BuildSessionFactory();
             if (createDb)
             {
                 Fill();
             }
		}
			

		public void ExecuteTransaction(Action<ISession> f){
			using (var session = sessionFactory.OpenSession())
			using (var tx = session.BeginTransaction())
			{
				try{
					f(session);
					tx.Commit();
				}
#if !DEBUG
                catch (Exception e){

					tx.Rollback ();
					if (OnException != null) {
						OnException (e);
					} else {
						throw e;
					}
				}
#endif
                finally
                {

                }
			}
 		
		}

        private void Fill()
        {
            ExecuteTransaction(session =>
            {

                var c = new SampleDb(session);
                c.CreateSampleDb();

            });
        }
        public void DumbQuery()//nic nie robiące zapytanie poto aby zrobić lazy loading bibliotek NHibernate
        {
                ExecuteTransaction(session => {
                    session.Query<Konto>()
                        .Where(x => x.Id == 0)
                        .ToList();
                        
                });
        }
		private ISessionFactory sessionFactory;
		public Action<Exception> OnException { get; set; }
        public bool IsUserLogged { get; private set; }
        public int LoggedUserId { get; private set; }
	}
}
