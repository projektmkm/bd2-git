﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Connection
{
    /// <summary>
    /// w debug klasa ta loguje wszystkie zapytania do bazy. W release nic nie robi
    /// </summary>
    class SqliteDriver : NHibernate.Driver.SQLite20Driver
    {
#if DEBUG
        public override void AdjustCommand(System.Data.IDbCommand command)
        {
            SqlLogger.Log(command);
            base.AdjustCommand(command);
        }
#endif
    }
}
