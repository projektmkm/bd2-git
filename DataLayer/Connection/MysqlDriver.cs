﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Connection
{
    public class MysqlDriver : NHibernate.Driver.MySqlDataDriver
    {
#if DEBUG
        public override void AdjustCommand(System.Data.IDbCommand command)
        {
            SqlLogger.Log(command);
            base.AdjustCommand(command);
        }
#endif
    }
}
