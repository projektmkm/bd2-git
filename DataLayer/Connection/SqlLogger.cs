﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if DEBUG
namespace DataLayer.Connection
{
    class SqlLogger : TextWriter
    {
        private SqlLogger()
        {
            writer = new StreamWriter("query_log.txt",false, System.Text.Encoding.UTF8);
        }
        public static SqlLogger GetInstance()
        {
            return logger;
        }
        public static void Log(System.Data.IDbCommand command)
        {
            try
            {
                logger.WriteLine(QueryParser.QueryBuildDebug(command));
            }
            catch (Exception e)
            {
                logger.WriteLine(e.Message);
            }
        }
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }

        public override void Write(string value)
        {
            Console.Write(value);
            writer.Write(value);
        }

        public override void WriteLine(string value)
        {
            Console.WriteLine(value);
            writer.WriteLine(value);
        }
        protected override void Dispose(bool x)
        {
            writer.Flush();
            base.Dispose(x);
        }
        private StreamWriter writer;
        private static SqlLogger logger = new SqlLogger();
    }
}
#endif
