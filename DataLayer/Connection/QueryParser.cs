﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Connection
{
    class QueryParser
    {
        public static string QueryBuildDebug(System.Data.IDbCommand command)
        {
            string sql = command.CommandText;
            foreach (var param in command.Parameters)
            {
                var x = param as DbParameter;
                if (x != null)
                {
                    int idx = sql.IndexOf(x.ParameterName);
                    if (idx != -1)
                    {
                        string p1 = sql.Substring(0, idx);
                        string p2 = sql.Substring(idx + x.ParameterName.Length);
                        bool q = IsQuotableType(x.DbType);
                        sql = p1;
                        if (x.Value == null || (x.Value.ToString().Length == 0 && ! IsStringType(x.DbType)))
                        {
                            sql += " null ";
                        }
                        else
                        {
                            if (q)
                            {
                                sql += "'";
                            }
                            sql += x.Value;
                            if (q)
                            {
                                sql += "'";
                            }
                        }
                        sql += p2;
                    }
                }
                else
                {
                    throw new ArgumentException("Unable to load query parameters: " + command.CommandText);
                }
            }
            sql += ";";
            return sql;
        }
        private static bool IsQuotableType(System.Data.DbType t)
        {
            return IsStringType(t) || IsDateType(t);
        }
        private static bool IsStringType(System.Data.DbType t)
        {
            return t == System.Data.DbType.String || t == System.Data.DbType.AnsiString || t == System.Data.DbType.AnsiStringFixedLength || t == System.Data.DbType.StringFixedLength;
        }
        private static bool IsDateType(System.Data.DbType t)
        {
            return t == System.Data.DbType.DateTime || t == System.Data.DbType.DateTime2 || t == System.Data.DbType.DateTimeOffset ;
        }
    }
}
