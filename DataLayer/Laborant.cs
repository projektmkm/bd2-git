﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public  class Laborant : Konto
    {
        public virtual IList<BadanieLaboratoryjne> Badania { get; set; }
        public override KontoRola GetRole()
        {
            return KontoRola.Laborant;
        }
        public Laborant()
        {
            Badania = new List<BadanieLaboratoryjne>();
        }
    }
    public class LaborantMap : SubclassMap<Laborant>
    {
        public LaborantMap()
        {
            HasMany(x => x.Badania);
        }
    }
}
