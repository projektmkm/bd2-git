﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public enum StanBadania
    {
        Zlecone,
        Wykonane,
        Zatwierdzone, 
        AnulowaneLaborant,
        AnulowaneKierownik,
        Wykonywane
    }
    public class BadanieLaboratoryjne
    {
        public virtual int Id { get; set; }
        public virtual DateTime DataZlecenia { get; set; }
        public virtual DateTime? DataWykonania { get; set; }
        public virtual DateTime? DataZatwierdzenia { get; set; }
        public virtual DateTime? DataAnulowania { get; set; }
        public virtual string UwagiLekarza { get; set; }
        public virtual string Wynik { get; set; }
        public virtual string UwagiKierownika { get; set; }
        public virtual StanBadania Stan { get; set; }
        public virtual TypBadania TypBadania { get; set; }
        public virtual Wizyta Wizyta { get; set; }
        public virtual Laborant Laborant { get; set; }
        public virtual KierownikLaboratorium KierownikLaboratorium { get; set; }
    }
    public class BadanieLaboratoryjneMap : ClassMap<BadanieLaboratoryjne>
    {
        public BadanieLaboratoryjneMap()
        {
            Id(x => x.Id);
            Map(x => x.DataZlecenia)
                .Not.Nullable();
            Map(x => x.DataWykonania);
            Map(x => x.DataZatwierdzenia);
            Map(x => x.DataAnulowania);
            Map(x => x.UwagiLekarza)
                .CustomType("StringClob");
            Map(x => x.Wynik);
            Map(x => x.UwagiKierownika)
                .CustomType("StringClob");
            Map(x => x.Stan);
            References(x => x.TypBadania)
                .Not.Nullable();
            References(x => x.Wizyta)
                .Not.Nullable();
            References(x => x.Laborant);
            References(x => x.KierownikLaboratorium);
        }
    }
}
