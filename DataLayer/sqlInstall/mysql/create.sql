alter table BADANIE_FIZYCZNE  drop foreign key FK_BADANIE_FIZYCZNE_WIZYTA   ;
alter table BADANIE_FIZYCZNE  drop foreign key FK_BADANIE_FIZYCZNE_TYP_BADANIA   ;
alter table BADANIE_LABORATORYJNE  drop foreign key FK_BADANIE_LABORATORYJNE_TYP_BADANIA   ;
alter table BADANIE_LABORATORYJNE  drop foreign key FK_BADANIE_LABORATORYJNE_WIZYTA;
alter table BADANIE_LABORATORYJNE  drop foreign key FK_BADANIE_LABORATORYJNE_LABORANT;
alter table BADANIE_LABORATORYJNE  drop foreign key FK_BADANIE_LABORATORYJNE_KIEROWNIK_LABORATORIUM ;
alter table ADMINISTRATOR  drop foreign key FK_ADMINISTRATOR_KONTO ;
alter table KIEROWNIK_LABORATORIUM  drop foreign key FK_KIEROWNIK_LABORATORIUM_KONTO ;
alter table LABORANT  drop foreign key FK_LABORANT_KONTO ;
alter table LEKARZ  drop foreign key FK_LEKARZ_KONTO ;
alter table REJESTRATOR  drop foreign key FK_REJESTRATOR_KONTO  ;
alter table WIZYTA  drop foreign key FK_WIZYTA_REJESTRATOR ;
alter table WIZYTA  drop foreign key FK_WIZYTA_LEKARZ;
alter table WIZYTA  drop foreign key FK_WIZYTA_PACJENT;

    drop table if exists BADANIE_FIZYCZNE;
    drop table if exists BADANIE_LABORATORYJNE;
    drop table if exists KONTO;
    drop table if exists ADMINISTRATOR;
    drop table if exists KIEROWNIK_LABORATORIUM;
    drop table if exists LABORANT;
    drop table if exists LEKARZ;
    drop table if exists REJESTRATOR;
    drop table if exists PACJENT;
    drop table if exists TYP_BADANIA;
    drop table if exists WIZYTA;

    create table BADANIE_FIZYCZNE (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       DATA_WYKONANIA DATETIME not null,
       WYNIK VARCHAR(255) not null,
       WIZYTA_ID INTEGER not null,
       TYP_BADANIA_ID INTEGER not null,
       primary key (ID)
    );

    create table BADANIE_LABORATORYJNE (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       DATA_ZLECENIA DATETIME not null,
       DATA_WYKONANIA DATETIME,
       DATA_ZATWIERDZENIA DATETIME,
       DATA_ANULOWANIA DATETIME,
       UWAGI_LEKARZA VARCHAR(255),
       WYNIK VARCHAR(255),
       UWAGI_KIEROWNIKA VARCHAR(255),
       STAN INTEGER,
       TYP_BADANIA_ID INTEGER not null,
       WIZYTA_ID INTEGER not null,
       LABORANT_ID INTEGER,
       KIEROWNIK_LABORATORIUM_ID INTEGER,
       primary key (ID)
    );

   create table KONTO (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       LOGIN VARCHAR(255) not null,
       HASLO_HASH VARCHAR(255) not null,
       SALT VARCHAR(255) not null,
       DATA_KONCA DATETIME,
       IMIE VARCHAR(255) not null,
       NAZWISKO VARCHAR(255) not null,
       primary key (ID)
    )

    create table ADMINISTRATOR (
        KONTO_ID INTEGER not null,
       primary key (KONTO_ID)
    );

    create table KIEROWNIK_LABORATORIUM (
        KONTO_ID INTEGER not null,
       primary key (KONTO_ID)
    );

    create table LABORANT (
        KONTO_ID INTEGER not null,
       primary key (KONTO_ID)
    );

    create table LEKARZ (
        KONTO_ID INTEGER not null,
       primary key (KONTO_ID)
    );

    create table REJESTRATOR (
        KONTO_ID INTEGER not null,
       primary key (KONTO_ID)
    );

    create table PACJENT (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       IMIE VARCHAR(255) not null,
       NAZWISKO VARCHAR(255) not null,
       PESEL VARCHAR(255),
       ADRES VARCHAR(255),
       primary key (ID)
    );

    create table TYP_BADANIA (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       KOD VARCHAR(255) not null,
       NAZWA VARCHAR(255) not null,
       OPIS VARCHAR(255),
       TYP INTEGER not null,
       primary key (ID)
    );

    create table WIZYTA (
        ID INTEGER NOT NULL AUTO_INCREMENT,
       OPIS VARCHAR(255),
       DIAGNOZA VARCHAR(255),
       STAN INTEGER,
       DATA_REJESTRACJI DATETIME not null,
       DATA_ZAKONCZENIA DATETIME,
       REJESTRATOR_ID INTEGER not null,
       LEKARZ_ID INTEGER not null,
       PACJENT_ID INTEGER not null,
       primary key (ID)
    );

    alter table BADANIE_FIZYCZNE 
        add index (WIZYTA_ID), 
        add constraint FK_BADANIE_FIZYCZNE_WIZYTA 
        foreign key (WIZYTA_ID) 
        references WIZYTA (ID);

    alter table BADANIE_FIZYCZNE 
        add index (TYP_BADANIA_ID), 
        add constraint FK_BADANIE_FIZYCZNE_TYP_BADANIA 
        foreign key (TYP_BADANIA_ID) 
        references TYP_BADANIA (ID);

    alter table BADANIE_LABORATORYJNE 
        add index (TYP_BADANIA_ID), 
        add constraint FK_BADANIE_LABORATORYJNE_TYP_BADANIA 
        foreign key (TYP_BADANIA_ID) 
        references TYP_BADANIA (ID);

    alter table BADANIE_LABORATORYJNE 
        add index (WIZYTA_ID), 
        add constraint FK_BADANIE_LABORATORYJNE_WIZYTA 
        foreign key (WIZYTA_ID) 
        references WIZYTA (ID);

    alter table BADANIE_LABORATORYJNE 
        add index (LABORANT_ID), 
        add constraint FK_BADANIE_LABORATORYJNE_LABORANT 
        foreign key (LABORANT_ID) 
        references LABORANT (KONTO_ID);

    alter table BADANIE_LABORATORYJNE 
        add index (KIEROWNIK_LABORATORIUM_ID), 
        add constraint FK_BADANIE_LABORATORYJNE_KIEROWNIK_LABORATORIUM 
        foreign key (KIEROWNIK_LABORATORIUM_ID) 
        references KIEROWNIK_LABORATORIUM (KONTO_ID);

    alter table ADMINISTRATOR 
        add index (KONTO_ID), 
        add constraint FK_ADMINISTRATOR_KONTO 
        foreign key (KONTO_ID) 
        references KONTO (ID);

    alter table KIEROWNIK_LABORATORIUM 
        add index (KONTO_ID), 
        add constraint FK_KIEROWNIK_LABORATORIUM_KONTO 
        foreign key (KONTO_ID) 
        references KONTO (ID);

    alter table LABORANT 
        add index (KONTO_ID), 
        add constraint FK_LABORANT_KONTO 
        foreign key (KONTO_ID) 
        references KONTO (ID);

    alter table LEKARZ 
        add index (KONTO_ID), 
        add constraint FK_LEKARZ_KONTO 
        foreign key (KONTO_ID) 
        references KONTO (ID);

    alter table REJESTRATOR 
        add index (KONTO_ID), 
        add constraint FK_REJESTRATOR_KONTO 
        foreign key (KONTO_ID) 
        references KONTO (ID);

    alter table WIZYTA 
        add index (REJESTRATOR_ID), 
        add constraint FK_WIZYTA_REJESTRATOR 
        foreign key (REJESTRATOR_ID) 
        references REJESTRATOR (KONTO_ID);

    alter table WIZYTA 
        add index (LEKARZ_ID), 
        add constraint FK_WIZYTA_LEKARZ 
        foreign key (LEKARZ_ID) 
        references LEKARZ (KONTO_ID);

    alter table WIZYTA 
        add index (PACJENT_ID), 
        add constraint FK_WIZYTA_PACJENT 
        foreign key (PACJENT_ID) 
        references PACJENT (ID);