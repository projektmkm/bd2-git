﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Lekarz : Konto
    {
        public virtual IList<Wizyta> Wizyty { get; set; }
        public override KontoRola GetRole()
        {
            return KontoRola.Lekarz;
        }
        public Lekarz()
        {
            Wizyty = new List<Wizyta>();
        }
    }

    public class LekarzMap : SubclassMap<Lekarz>
    {
        public LekarzMap()
        {
            HasMany(x => x.Wizyty);
        }
    }
}
