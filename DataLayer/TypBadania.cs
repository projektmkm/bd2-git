﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class TypBadania
    {
        public virtual int Id { get; set; }
        public virtual string Kod { get; set; }
        public virtual string Nazwa { get; set; }
        public virtual string Opis { get; set; }
        public virtual BadanieTyp Typ { get; set; }
        public TypBadania()
        {
        }
    }
    public class TypBadaniaMap : ClassMap<TypBadania>
    {
        public TypBadaniaMap ()
        {
            Id(x => x.Id);
            Map(x => x.Kod)
                .Not.Nullable();
            Map(x => x.Nazwa)
                .Not.Nullable();
            Map(x => x.Opis)
                .CustomType("StringClob");
            Map(x => x.Typ)
                .Not.Nullable();
        }
    }

    public enum BadanieTyp
    {
        Fizyczne, 
        Laboratoryjne
    }
}
