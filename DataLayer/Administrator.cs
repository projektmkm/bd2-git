﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Administrator : Konto
    {
        public override KontoRola GetRole()
        {
            return KontoRola.Administrator;
        }
    }
    public class AdministratorMap: SubclassMap<Administrator>
    {
        public AdministratorMap()
        {

        }
    }
}
