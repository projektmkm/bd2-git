﻿using Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Forms;
namespace Bd2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ApplicationEngine e = null;
                var result = MessageBox.Show("otwieranie połączenia. czy użyć istniejącej już bazy ? (nie aby stworzyć nową)", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                     e = new ApplicationEngine(false, exHandler);
                }
                else if (result == DialogResult.No)
                {
                     e = new ApplicationEngine(true, exHandler);
                }
                else
                {
                    return;
                }
                MessageBox.Show("otwarto połączenie z bazą danych");
                var form = new bd2Gui.Logowanie();
                form.ShowDialog();
            }
            catch (Exception e)
            {
                exHandler(e);
            }
        }

        private static Action<Exception> exHandler = e =>
        {
            string msg = e.Message;
            e = e.InnerException;
            while (e != null)
            {
                msg += "\r\n===========================================\r\n";
                msg += e.Message;
                e = e.InnerException;
            }
            MessageBox.Show(msg, "exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
        };
    }

}
