﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Config
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {

            //Server=localhost; Port=3306; Database=przychodnia_db; Uid=root; Pwd=root;nt 
            int prt;
            
            string host = hostBox.Text;
            string port = portBox.Text;
            string pass = passBox.Text;
            if (!Int32.TryParse(port, out prt))
            {
                MessageBox.Show("Błędny port");
                return;
            }
            string connStr = "Server=" + host + "; Port=" + port + "; Database=przychodnia_db; Uid=client_user; Pwd=" + pass + ";";
            
            var bytes = System.Text.Encoding.UTF7.GetBytes(connStr);
            var connStrHashed = System.Convert.ToBase64String(bytes);

            //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None); // lub path
            var config = ConfigurationManager.OpenExeConfiguration("Przychodnia.exe");
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["mysql_encrypted"].ConnectionString = connStrHashed;
            //connectionStringsSection.ConnectionStrings["mysql"].ConnectionString = connStr;

            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");
            MessageBox.Show("Konfiguracja zapisana poprawnie");
            System.Windows.Forms.Application.Exit();
        }

    }
}
