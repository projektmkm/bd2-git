﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class WizytaRejestracja : Form
    {
        public WizytaRejestracja(ApplicationEngine e, int idPacjenta, int idRejestratora, Pacjenci parentForm)
        {
            InitializeComponent();
            engine = e;
            this.idPacjenta = idPacjenta;
            this.idRejestratora = idRejestratora;
            this.parentForm = parentForm;
            parentForm.Hide();

            new ManagerPacjentow.PoszukiwaczPacjentow(engine.PacjentManager)
                   .IdPacjenta(idPacjenta)
                   .Wykonaj(x => {
                       nameTextBox.Text = x.Imie;
                       surnameTextBox.Text = x.Nazwisko;
                       peselTextBox.Text = x.Pesel;
                       addressTextBox.Text = x.Adres;
                   });

            this.DatePicker.Value = DateTime.Now;
            LadujLekarzy();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshShort;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
            parentForm.Close();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Czy jestes pewien?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                engine.WizytaManager.DodajWizyte(idRejestratora, idLekarza, idPacjenta, DatePicker.Value);
                this.Close();
                parentForm.Close();
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            parentForm.Show();
        }

        private readonly ApplicationEngine engine;
        private readonly int idPacjenta;
        private readonly int idRejestratora;
        private readonly Form parentForm;
        private bool lekarzeZaladowani = false;

        private void listaLekarzy_SelectionChanged(object sender, EventArgs e)
        {
            if (listaLekarzy.SelectedRows.Count == 1)
            {
                if(lekarzeZaladowani == true)
                    LadujWizytyLekarza();
                registerButton.Enabled = true;
            }
            else
            {
                registerButton.Enabled = false;
            }
        }

        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void LadujWizytyLekarza()
        {
            listaWizytLekarza.Rows.Clear();
            if (listaLekarzy.SelectedRows.Count == 1)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
                    .IdLekarza(idLekarza)
                    .Wykonaj(x => {
                        foreach (var wizyta in x.Wizyty)
                        {
                            if ((wizyta.Stan != DataLayer.StanWizyty.Anulowana)
                                && (wizyta.DataRejestracji.Date.Equals(DatePicker.Value.Date)))
                            {
                                listaWizytLekarza.Rows.Add("", wizyta.DataRejestracji.ToShortTimeString(), wizyta.Pacjent.Nazwisko + " " + wizyta.Pacjent.Imie);
                            }
                        }
                        listaWizytLekarza.Sort(listaWizytLekarza.Columns[1], ListSortDirection.Ascending);
                        int numerWizyty = 1;
                        foreach (DataGridViewRow row in listaWizytLekarza.Rows)
                        {
                            row.Cells[0].Value = numerWizyty;
                            numerWizyty++;
                        }
                    });
            }
        }

        private void LadujLekarzy()
        {
            lekarzeZaladowani = false;
            listaLekarzy.Rows.Clear();

            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
                .Wykonaj(x => {
                    int liczbaDzisiejszychWizyt = 0;
                    foreach (var wizyta in x.Wizyty)
                    {
                        if (wizyta.DataRejestracji.Date.Equals(DatePicker.Value.Date))
                            liczbaDzisiejszychWizyt++;
                    }
                    listaLekarzy.Rows.Add(x.Id, x.Imie, x.Nazwisko, liczbaDzisiejszychWizyt);
                });
            lekarzeZaladowani = true;
        }

        private void Odswiez()
        {
            if (listaLekarzy.SelectedRows.Count == 1)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                LadujLekarzy();
                foreach (DataGridViewRow row in listaLekarzy.Rows)
                {
                    if ((int)row.Cells[0].Value == idLekarza)
                    {
                        row.Selected = true;
                        break;
                    }
                }
                LadujWizytyLekarza();
            }
        }

        private void WizytaRejestracja_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void listaWizytLekarza_SelectionChanged(object sender, EventArgs e)
        {
            listaWizytLekarza.ClearSelection();
        }
    }
}
