﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class PacjentReport : Form
    {
        public PacjentReport(ApplicationEngine e)
        {
            InitializeComponent();
            engine = e;
            data = engine.ReportGenerator.Pacjenci();
            dataGridView1.DataSource = data;

        }
        private ApplicationEngine engine;
        private DataTable data;

        private void button1_Click(object sender, EventArgs e)
        {
            string fname = ReportFileDialog.RaportSaveDialog();
            if (fname != null)
            {
                ReportGenerator.DataTableToPdf(data, "Raport pacjentów", fname);
            }
        }
    }
}
