﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class Admin : Form
    {
        public Admin(ApplicationEngine e)
        {
            InitializeComponent();
            engine = e;
        }

        private void Admin_Load(object sender, EventArgs e)
        {

        }
        private readonly ApplicationEngine engine;

        private void usersBt_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new AdminUżytkownicyLista(engine))
                form.ShowDialog();
            this.Show();
        }

        private void phisicTestBt_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new AdminBadaniaLista(engine, DataLayer.BadanieTyp.Fizyczne))
                form.ShowDialog();
            this.Show();
        }

        private void labTestBt_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new AdminBadaniaLista(engine, DataLayer.BadanieTyp.Laboratoryjne))
                form.ShowDialog();
            this.Show();
        }

        private void logoutBt_Click(object sender, EventArgs e)
        {
            engine.KontoManager.Wyloguj();
        }

        private void reportButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new WizytyReport(engine))
                form.ShowDialog();
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new TypyBadaniaReport(engine))
                form.ShowDialog();
            this.Show();
        }
    }
}
