﻿namespace Forms
{
    partial class LekarzWizytyLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dataZakonczenia2 = new System.Windows.Forms.DateTimePicker();
            this.dataRejestracji2 = new System.Windows.Forms.DateTimePicker();
            this.dataZakonczenia1 = new System.Windows.Forms.DateTimePicker();
            this.finDateLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.dataRejestracji1 = new System.Windows.Forms.DateTimePicker();
            this.labStateComboBox = new System.Windows.Forms.ComboBox();
            this.ListaWizyt = new System.Windows.Forms.DataGridView();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.docCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnButton = new System.Windows.Forms.Button();
            this.ShowButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.regDateLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.diagnosisTextBox = new System.Windows.Forms.TextBox();
            this.doctorComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.regDateCheckBox = new System.Windows.Forms.CheckBox();
            this.finDateCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.ListaWizyt)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(349, 73);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 135;
            this.label6.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(349, 32);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 134;
            this.label12.Text = "-";
            // 
            // dataZakonczenia2
            // 
            this.dataZakonczenia2.CustomFormat = "yyyy-MM-dd";
            this.dataZakonczenia2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZakonczenia2.Location = new System.Drawing.Point(362, 70);
            this.dataZakonczenia2.Name = "dataZakonczenia2";
            this.dataZakonczenia2.Size = new System.Drawing.Size(84, 20);
            this.dataZakonczenia2.TabIndex = 133;
            // 
            // dataRejestracji2
            // 
            this.dataRejestracji2.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji2.Location = new System.Drawing.Point(362, 29);
            this.dataRejestracji2.Name = "dataRejestracji2";
            this.dataRejestracji2.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji2.TabIndex = 132;
            // 
            // dataZakonczenia1
            // 
            this.dataZakonczenia1.CustomFormat = "yyyy-MM-dd";
            this.dataZakonczenia1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZakonczenia1.Location = new System.Drawing.Point(261, 70);
            this.dataZakonczenia1.Name = "dataZakonczenia1";
            this.dataZakonczenia1.Size = new System.Drawing.Size(84, 20);
            this.dataZakonczenia1.TabIndex = 131;
            // 
            // finDateLabel
            // 
            this.finDateLabel.AutoSize = true;
            this.finDateLabel.Location = new System.Drawing.Point(258, 53);
            this.finDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.finDateLabel.Name = "finDateLabel";
            this.finDateLabel.Size = new System.Drawing.Size(96, 13);
            this.finDateLabel.TabIndex = 130;
            this.finDateLabel.Text = "Data zakończenia:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(630, 127);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 129;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // dataRejestracji1
            // 
            this.dataRejestracji1.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji1.Location = new System.Drawing.Point(261, 29);
            this.dataRejestracji1.Name = "dataRejestracji1";
            this.dataRejestracji1.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji1.TabIndex = 128;
            // 
            // labStateComboBox
            // 
            this.labStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.labStateComboBox.FormattingEnabled = true;
            this.labStateComboBox.Location = new System.Drawing.Point(14, 71);
            this.labStateComboBox.Name = "labStateComboBox";
            this.labStateComboBox.Size = new System.Drawing.Size(170, 21);
            this.labStateComboBox.TabIndex = 122;
            // 
            // ListaWizyt
            // 
            this.ListaWizyt.AllowUserToAddRows = false;
            this.ListaWizyt.AllowUserToDeleteRows = false;
            this.ListaWizyt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListaWizyt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.Date1Col,
            this.Date2Col,
            this.docCol,
            this.labCol,
            this.StateCol});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Format = "g";
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ListaWizyt.DefaultCellStyle = dataGridViewCellStyle2;
            this.ListaWizyt.Location = new System.Drawing.Point(13, 156);
            this.ListaWizyt.MultiSelect = false;
            this.ListaWizyt.Name = "ListaWizyt";
            this.ListaWizyt.ReadOnly = true;
            this.ListaWizyt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ListaWizyt.Size = new System.Drawing.Size(740, 243);
            this.ListaWizyt.TabIndex = 120;
            this.ListaWizyt.SelectionChanged += new System.EventHandler(this.ListaWizyt_SelectionChanged);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "ID";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Width = 60;
            // 
            // Date1Col
            // 
            this.Date1Col.HeaderText = "Data rejestracji";
            this.Date1Col.Name = "Date1Col";
            this.Date1Col.ReadOnly = true;
            this.Date1Col.Width = 120;
            // 
            // Date2Col
            // 
            this.Date2Col.HeaderText = "Data zakończenia";
            this.Date2Col.Name = "Date2Col";
            this.Date2Col.ReadOnly = true;
            this.Date2Col.Width = 120;
            // 
            // docCol
            // 
            this.docCol.HeaderText = "Lekarz";
            this.docCol.Name = "docCol";
            this.docCol.ReadOnly = true;
            this.docCol.Width = 140;
            // 
            // labCol
            // 
            this.labCol.HeaderText = "Badania laboratoryjne";
            this.labCol.Name = "labCol";
            this.labCol.ReadOnly = true;
            this.labCol.Width = 140;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Diagnoza";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            this.StateCol.Width = 115;
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(630, 404);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(123, 23);
            this.returnButton.TabIndex = 118;
            this.returnButton.Text = "Powrót";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // ShowButton
            // 
            this.ShowButton.Enabled = false;
            this.ShowButton.Location = new System.Drawing.Point(501, 404);
            this.ShowButton.Name = "ShowButton";
            this.ShowButton.Size = new System.Drawing.Size(123, 23);
            this.ShowButton.TabIndex = 117;
            this.ShowButton.Text = "Pokaż";
            this.ShowButton.UseVisualStyleBackColor = true;
            this.ShowButton.Click += new System.EventHandler(this.ShowButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(501, 127);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(123, 23);
            this.SearchButton.TabIndex = 116;
            this.SearchButton.Text = "Szukaj";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // regDateLabel
            // 
            this.regDateLabel.AutoSize = true;
            this.regDateLabel.Location = new System.Drawing.Point(258, 12);
            this.regDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.regDateLabel.Name = "regDateLabel";
            this.regDateLabel.Size = new System.Drawing.Size(80, 13);
            this.regDateLabel.TabIndex = 115;
            this.regDateLabel.Text = "Data rejestracji:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 121;
            this.label4.Text = "Badania laboratoryjne:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 137;
            this.label2.Text = "Diagnoza:";
            // 
            // diagnosisTextBox
            // 
            this.diagnosisTextBox.Location = new System.Drawing.Point(14, 29);
            this.diagnosisTextBox.Name = "diagnosisTextBox";
            this.diagnosisTextBox.Size = new System.Drawing.Size(170, 20);
            this.diagnosisTextBox.TabIndex = 136;
            // 
            // doctorComboBox
            // 
            this.doctorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorComboBox.FormattingEnabled = true;
            this.doctorComboBox.Location = new System.Drawing.Point(501, 29);
            this.doctorComboBox.Name = "doctorComboBox";
            this.doctorComboBox.Size = new System.Drawing.Size(252, 21);
            this.doctorComboBox.TabIndex = 139;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(498, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 138;
            this.label1.Text = "Lekarz:";
            // 
            // regDateCheckBox
            // 
            this.regDateCheckBox.AutoSize = true;
            this.regDateCheckBox.Location = new System.Drawing.Point(452, 32);
            this.regDateCheckBox.Name = "regDateCheckBox";
            this.regDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.regDateCheckBox.TabIndex = 140;
            this.regDateCheckBox.UseVisualStyleBackColor = true;
            this.regDateCheckBox.CheckedChanged += new System.EventHandler(this.regDateCheckBox_CheckedChanged);
            // 
            // finDateCheckBox
            // 
            this.finDateCheckBox.AutoSize = true;
            this.finDateCheckBox.Location = new System.Drawing.Point(452, 73);
            this.finDateCheckBox.Name = "finDateCheckBox";
            this.finDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.finDateCheckBox.TabIndex = 141;
            this.finDateCheckBox.UseVisualStyleBackColor = true;
            this.finDateCheckBox.CheckedChanged += new System.EventHandler(this.finDateCheckBox_CheckedChanged);
            // 
            // LekarzWizytyLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 439);
            this.Controls.Add(this.finDateCheckBox);
            this.Controls.Add(this.regDateCheckBox);
            this.Controls.Add(this.doctorComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.diagnosisTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataZakonczenia2);
            this.Controls.Add(this.dataRejestracji2);
            this.Controls.Add(this.dataZakonczenia1);
            this.Controls.Add(this.finDateLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.dataRejestracji1);
            this.Controls.Add(this.labStateComboBox);
            this.Controls.Add(this.ListaWizyt);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.ShowButton);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.regDateLabel);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LekarzWizytyLista";
            this.ShowIcon = false;
            this.Text = "Historia wizyt pacjenta";
            this.Load += new System.EventHandler(this.LekarzWizytyLista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ListaWizyt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dataZakonczenia2;
        private System.Windows.Forms.DateTimePicker dataRejestracji2;
        private System.Windows.Forms.DateTimePicker dataZakonczenia1;
        private System.Windows.Forms.Label finDateLabel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DateTimePicker dataRejestracji1;
        private System.Windows.Forms.ComboBox labStateComboBox;
        private System.Windows.Forms.DataGridView ListaWizyt;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.Button ShowButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label regDateLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox diagnosisTextBox;
        private System.Windows.Forms.ComboBox doctorComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn docCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn labCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.CheckBox regDateCheckBox;
        private System.Windows.Forms.CheckBox finDateCheckBox;
    }
}