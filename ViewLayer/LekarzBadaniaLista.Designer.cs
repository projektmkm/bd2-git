﻿namespace ViewLayer
{
    partial class LekarzBadaniaLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listaBadan = new System.Windows.Forms.DataGridView();
            this.testIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showBt = new System.Windows.Forms.Button();
            this.backBt = new System.Windows.Forms.Button();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).BeginInit();
            this.SuspendLayout();
            // 
            // listaBadan
            // 
            this.listaBadan.AllowUserToAddRows = false;
            this.listaBadan.AllowUserToDeleteRows = false;
            this.listaBadan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaBadan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.testIdCol,
            this.CodeCol,
            this.NameCol,
            this.StateCol,
            this.ResultCol,
            this.TypeCol});
            this.listaBadan.Location = new System.Drawing.Point(12, 12);
            this.listaBadan.MultiSelect = false;
            this.listaBadan.Name = "listaBadan";
            this.listaBadan.ReadOnly = true;
            this.listaBadan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaBadan.Size = new System.Drawing.Size(579, 306);
            this.listaBadan.TabIndex = 0;
            this.listaBadan.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // testIdCol
            // 
            this.testIdCol.HeaderText = "ID Badania";
            this.testIdCol.Name = "testIdCol";
            this.testIdCol.ReadOnly = true;
            // 
            // CodeCol
            // 
            this.CodeCol.HeaderText = "Kod";
            this.CodeCol.Name = "CodeCol";
            this.CodeCol.ReadOnly = true;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Nazwa";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Width = 135;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Stan";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            this.StateCol.Width = 80;
            // 
            // ResultCol
            // 
            this.ResultCol.HeaderText = "Wynik";
            this.ResultCol.Name = "ResultCol";
            this.ResultCol.ReadOnly = true;
            this.ResultCol.Width = 120;
            // 
            // TypeCol
            // 
            this.TypeCol.HeaderText = "TypBadania";
            this.TypeCol.Name = "TypeCol";
            this.TypeCol.ReadOnly = true;
            this.TypeCol.Visible = false;
            // 
            // showBt
            // 
            this.showBt.Enabled = false;
            this.showBt.Location = new System.Drawing.Point(339, 324);
            this.showBt.Name = "showBt";
            this.showBt.Size = new System.Drawing.Size(123, 23);
            this.showBt.TabIndex = 29;
            this.showBt.Text = "Pokaż";
            this.showBt.UseVisualStyleBackColor = true;
            this.showBt.Click += new System.EventHandler(this.showBt_Click);
            // 
            // backBt
            // 
            this.backBt.Location = new System.Drawing.Point(468, 324);
            this.backBt.Name = "backBt";
            this.backBt.Size = new System.Drawing.Size(123, 23);
            this.backBt.TabIndex = 30;
            this.backBt.Text = "Powrót";
            this.backBt.UseVisualStyleBackColor = true;
            this.backBt.Click += new System.EventHandler(this.backBt_Click);
            // 
            // LekarzBadaniaLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 359);
            this.Controls.Add(this.backBt);
            this.Controls.Add(this.showBt);
            this.Controls.Add(this.listaBadan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LekarzBadaniaLista";
            this.ShowIcon = false;
            this.Text = "Lista badań pacjenta";
            this.Load += new System.EventHandler(this.LekarzBadaniaLista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView listaBadan;
        private System.Windows.Forms.Button showBt;
        private System.Windows.Forms.Button backBt;
        private System.Windows.Forms.DataGridViewTextBoxColumn testIdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeCol;
        private System.Windows.Forms.Timer autorefresh;
    }
}