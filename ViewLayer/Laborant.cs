﻿using BusinessLayer;
using DataLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using KontoIdItem = Forms.ComboBoxItem<int?>;
using StanBadaniaItem = Forms.ComboBoxItem<DataLayer.StanBadania?>;

namespace ViewLayer
{
    public partial class Laborant : Form
    {
        
        public Laborant(ApplicationEngine e, int idLaboranta)
        {
            InitializeComponent();
            engine = e;
            this.idLaboranta = idLaboranta;

            stateComboBox.Items.Add(new StanBadaniaItem { Text = " ", Value = null });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Zlecone.ToString(), Value = StanBadania.Zlecone });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Wykonywane.ToString(), Value = StanBadania.Wykonywane });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Wykonane.ToString(), Value = StanBadania.Wykonane });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Zatwierdzone.ToString(), Value = StanBadania.Zatwierdzone });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.AnulowaneLaborant.ToString(), Value = StanBadania.AnulowaneLaborant });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.AnulowaneKierownik.ToString(), Value = StanBadania.AnulowaneKierownik });

            KontoIdItem dowolny = new KontoIdItem { Text = " ", Value = null };
            doctorComboBox.Items.Add(dowolny);

            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
                .Wykonaj(x => {
                    doctorComboBox.Items.Add(new KontoIdItem { Text = x.Nazwisko + " " + x.Imie, Value = x.Id });
                });

            ustawDomyslneWartosci();
            LadujDomyslneBadania();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private readonly int idLaboranta;
        private readonly ApplicationEngine engine;
        private readonly Regex idRegex = new Regex(@"^\d*$");
        private bool odswiezSzukaneBadania = false;

        private void showBt_Click(object sender, EventArgs e)
        {
            int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            using (var form = new BadanieLaboratoryjnePokaz(engine, idBadania))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void performBt_Click(object sender, EventArgs e)
        {
            int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            using (var form = new BadanieLaboratoryjneLaborant(engine, idBadania, idLaboranta))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void searchBt_Click(object sender, EventArgs e)
        {
            ErrorProvider.Clear();
            if (IsUserInputValid())
            {
                odswiezSzukaneBadania = true;
                LadujSzukaneBadania();
            }else
            {
                ErrorProvider.SetError(examIDTextBox, "Nieprawidłowy numer ID");
            }
            
        }

        private bool IsUserInputValid()
        {
            return (idRegex.Match(examIDTextBox.Text).Success);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            engine.KontoManager.Wyloguj();
        }

        private void badanieList_SelectionChanged(object sender, EventArgs e)
        {
            if (listaBadan.SelectedRows.Count == 1)
            {
                showBt.Enabled = true;
                var stan = (StanBadania)listaBadan.SelectedRows[0].Cells[6].Value;
                if ((stan == StanBadania.Zlecone) || (stan == StanBadania.Wykonywane))
                    performBt.Enabled = true;
                else
                    performBt.Enabled = false;
            }
            else
                showBt.Enabled = performBt.Enabled = false;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ustawDomyslneWartosci();
            odswiezSzukaneBadania = false;
            LadujDomyslneBadania();
        }

        private void ustawDomyslneWartosci()
        {
            NameTextBox.Text = SurnameTextBox.Text = examIDTextBox.Text = string.Empty;
            dataZlecenia1.Value = DateTime.Now.AddYears(-1);
            dataZlecenia2.Value = DateTime.Now;
            doctorComboBox.SelectedIndex = 0;
            stateComboBox.SelectedIndex = 1;
            dataZlecenia1.Enabled = dataZlecenia2.Enabled = dateLabel.Enabled = dateCheckBox.Checked = false;
        }

        private void LadujDomyslneBadania()
        {
            listaBadan.Rows.Clear();

            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                .IdLaboranta(idLaboranta)
                .Stan(StanBadania.Wykonywane)
                .Wykonaj(x => {
                    listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.DataZlecenia, x.Wizyta.Pacjent.Imie,
                    x.Wizyta.Pacjent.Nazwisko, x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie, x.Stan);
                });
            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                .Stan(StanBadania.Zlecone)
                .Wykonaj(x => {
                    listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.DataZlecenia, x.Wizyta.Pacjent.Imie,
                    x.Wizyta.Pacjent.Nazwisko, x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie, x.Stan);
                });
            listaBadan.Sort(listaBadan.Columns[2], ListSortDirection.Descending);
            listaBadan.ClearSelection();
        }

        private void LadujSzukaneBadania()
        {
            listaBadan.Rows.Clear();
            var poszukiwacz = new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                    .ImiePacjenta(NameTextBox.Text)
                    .NazwiskoPacjenta(SurnameTextBox.Text);
            var stan = (stateComboBox.SelectedItem as StanBadaniaItem).Value;
            if (stan != null)
                poszukiwacz.Stan(stan.Value);
            if (dateCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataZlecenia(dataZlecenia1.Value)
                            .NajpozniejszaDataZlecenia(dataZlecenia2.Value);
            var idLekarza = (doctorComboBox.SelectedItem as KontoIdItem).Value;
            if (idLekarza != null)
                poszukiwacz.IdLekarza(idLekarza.Value);
            if (!string.IsNullOrEmpty(examIDTextBox.Text))
                poszukiwacz.IdBadania(int.Parse(examIDTextBox.Text));
            poszukiwacz.Wykonaj(x => {
                listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.DataZlecenia, x.Wizyta.Pacjent.Imie,
                    x.Wizyta.Pacjent.Nazwisko, x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie, x.Stan);
            });
            listaBadan.Sort(listaBadan.Columns[2], ListSortDirection.Descending);
            listaBadan.ClearSelection();
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if(listaBadan.SelectedRows.Count == 1)
            {
                int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value; //id zaznaczonego badania
                if (odswiezSzukaneBadania == true)
                {
                    ErrorProvider.Clear();
                    if (IsUserInputValid())
                    {
                        LadujSzukaneBadania();
                    }
                    else
                    {
                        ErrorProvider.SetError(examIDTextBox, "Nieprawidłowy numer ID");
                    }
                }
                else
                    LadujDomyslneBadania();
                foreach (DataGridViewRow row in listaBadan.Rows)
                {
                    if ((int)row.Cells[0].Value == idBadania)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                if (odswiezSzukaneBadania == true)
                {
                    ErrorProvider.Clear();
                    if (IsUserInputValid())
                    {
                        LadujSzukaneBadania();
                    }
                    else
                    {
                        ErrorProvider.SetError(examIDTextBox, "Nieprawidłowy numer ID");
                    }
                }
                else
                    LadujDomyslneBadania();
            }
        }

        private void Laborant_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void dateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dataZlecenia1.Enabled = dataZlecenia2.Enabled = dateLabel.Enabled =
                (dateCheckBox.Checked == true) ? true : false;
        }
    }
}
