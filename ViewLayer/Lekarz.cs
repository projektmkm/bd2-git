﻿using BusinessLayer;
using DataLayer;
using System;
using System.Windows.Forms;
using StanWizytyItem = Forms.ComboBoxItem<DataLayer.StanWizyty?>;

namespace ViewLayer
{
    public partial class Lekarz : Form
    {
        public Lekarz(ApplicationEngine e, int idLekarza)
        {
            InitializeComponent();
            engine = e;
            this.lekarzId = idLekarza;
            StateComboBox.Items.Add(new StanWizytyItem { Text = " ", Value = null });
            StateComboBox.Items.Add(new StanWizytyItem { Text = DataLayer.StanWizyty.Zarejestrowana.ToString(), Value = DataLayer.StanWizyty.Zarejestrowana });
            StateComboBox.Items.Add(new StanWizytyItem { Text = DataLayer.StanWizyty.WToku.ToString(), Value = DataLayer.StanWizyty.WToku });
            StateComboBox.Items.Add(new StanWizytyItem { Text = DataLayer.StanWizyty.Wykonana.ToString(), Value = DataLayer.StanWizyty.Wykonana });
            StateComboBox.Items.Add(new StanWizytyItem { Text = DataLayer.StanWizyty.Anulowana.ToString(), Value = DataLayer.StanWizyty.Anulowana });
            UstawDomyslneWartosci();
            LadujDomyslneWizytyLekarza();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private readonly ApplicationEngine engine;
        private readonly int lekarzId;
        private bool odswiezSzukaneWizyty = false;

        private void button1_Click(object sender, EventArgs e)
        {
            engine.KontoManager.Wyloguj();
        }


        private void searchButton_Click(object sender, EventArgs e)
        {
            odswiezSzukaneWizyty = true;
            LadujSzukaneWizyty();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Czy na pewno chcesz anulować tą wizytę?","Potwierdzenie anulowania", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int wizytaId = (int)ListaWizyt.SelectedRows[0].Cells[0].Value;
                engine.WizytaManager.AnulujWizyte(wizytaId);
                new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                    .IdWizyty(wizytaId)
                    .Wykonaj(x =>
                    {
                        ListaWizyt.SelectedRows[0].Cells[5].Value = x.Stan;
                        ListaWizyt.SelectedRows[0].Cells[4].Value = x.DataZakonczenia;
                    });
                cancelButton.Enabled = HandleButton.Enabled = false;
            }
        }

        private void ShowButton_Click(object sender, EventArgs e)
        {
            int idWizyty = (int)ListaWizyt.SelectedRows[0].Cells[0].Value;
            using (var form = new LekarzObsługa(engine, idWizyty, LekarzObsługa.Tryb.PokazWizyteLekarza))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void HandleButton_Click(object sender, EventArgs e)
        {
            int idWizyty = (int)ListaWizyt.SelectedRows[0].Cells[0].Value;
            using (var form = new LekarzObsługa(engine, idWizyty, LekarzObsługa.Tryb.ObsluzWizyte))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void LadujDomyslneWizytyLekarza()
        {
            ListaWizyt.Rows.Clear();
            var poszukiwacz = new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .IdLekarza(lekarzId)
                .Stan(StanWizyty.Zarejestrowana);
            poszukiwacz.Wykonaj(x => {
                ListaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.DataRejestracji, x.DataZakonczenia, x.Stan);
            });
            poszukiwacz.Stan(StanWizyty.WToku);
            poszukiwacz.Wykonaj(x => {
                ListaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.DataRejestracji, x.DataZakonczenia, x.Stan);
            });
            ListaWizyt.Sort(ListaWizyt.Columns[3], System.ComponentModel.ListSortDirection.Descending);
            ListaWizyt.ClearSelection();
        }

        private void LadujSzukaneWizyty()
        {
            ListaWizyt.Rows.Clear();
            var poszukiwacz = new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .ImiePacjenta(NameTextBox.Text)
                .NazwiskoPacjenta(SurnameTextBox.Text)
                .IdLekarza(lekarzId);
            if (regDateCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataRejestracji(dataRejestracji1.Value)
                            .NajpozniejszaDataRejestracji(dataRejestracji2.Value);
            if(finDateCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataZakonczenia(dataZakonczenia1.Value)
                            .NajpozniejszaDataZakonczenia(dataZakonczenia2.Value);    
            var stan = (StateComboBox.SelectedItem as StanWizytyItem).Value;
            if (stan != null)
                poszukiwacz.Stan(stan.Value);
            poszukiwacz.Wykonaj(x => {
                ListaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.DataRejestracji, x.DataZakonczenia, x.Stan);
            });
            ListaWizyt.Sort(ListaWizyt.Columns[3], System.ComponentModel.ListSortDirection.Descending);
            ListaWizyt.ClearSelection();
        }

        private void VisitsList_SelectionChanged(object sender, EventArgs e)
        {
            if(ListaWizyt.SelectedRows.Count == 1)
            {
                StanWizyty stan = (StanWizyty)ListaWizyt.SelectedRows[0].Cells[5].Value;
                if((stan == StanWizyty.Zarejestrowana)||(stan == StanWizyty.WToku))
                {
                    ShowButton.Enabled = cancelButton.Enabled = HandleButton.Enabled = true;
                }
                else
                {
                    ShowButton.Enabled = true;
                    cancelButton.Enabled = HandleButton.Enabled = false;
                }     
            }
            else
            {
                ShowButton.Enabled = cancelButton.Enabled = HandleButton.Enabled = false;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            UstawDomyslneWartosci();
            odswiezSzukaneWizyty = false;
            LadujDomyslneWizytyLekarza();
        }

        private void UstawDomyslneWartosci()
        {
            StateComboBox.SelectedIndex = 1;
            NameTextBox.Text = SurnameTextBox.Text = string.Empty;
            dataRejestracji1.Value = dataRejestracji1.Value = dataZakonczenia1.Value = dataZakonczenia2.Value = DateTime.Now;
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled = regDateCheckBox.Checked =
                finDateLabel.Enabled = dataZakonczenia1.Enabled = dataZakonczenia2.Enabled = finDateCheckBox.Checked = false;
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if(ListaWizyt.SelectedRows.Count == 1)
            {
                int idWizyty = (int)ListaWizyt.SelectedRows[0].Cells[0].Value; //id zaznaczonej wizyty
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujDomyslneWizytyLekarza();
                foreach (DataGridViewRow row in ListaWizyt.Rows)
                {
                    if ((int)row.Cells[0].Value == idWizyty)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujDomyslneWizytyLekarza();
            }
        }

        private void Lekarz_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void regDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled =
                (regDateCheckBox.Checked == true) ? true : false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            finDateLabel.Enabled = dataZakonczenia1.Enabled = dataZakonczenia2.Enabled =
                (finDateCheckBox.Checked == true) ? true : false;
        }
    }
}
