﻿namespace ViewLayer
{
    partial class WizytaRejestracja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.visitBox = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listaWizytLekarza = new System.Windows.Forms.DataGridView();
            this.LpCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listaLekarzy = new System.Windows.Forms.DataGridView();
            this.DocIDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VisitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatePicker = new System.Windows.Forms.DateTimePicker();
            this.patientDataBox = new System.Windows.Forms.GroupBox();
            this.peselLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.peselTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.returnButton = new System.Windows.Forms.Button();
            this.registerButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.visitBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaWizytLekarza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaLekarzy)).BeginInit();
            this.patientDataBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // visitBox
            // 
            this.visitBox.Controls.Add(this.label3);
            this.visitBox.Controls.Add(this.listaWizytLekarza);
            this.visitBox.Controls.Add(this.label2);
            this.visitBox.Controls.Add(this.label1);
            this.visitBox.Controls.Add(this.listaLekarzy);
            this.visitBox.Controls.Add(this.DatePicker);
            this.visitBox.Location = new System.Drawing.Point(12, 135);
            this.visitBox.Name = "visitBox";
            this.visitBox.Size = new System.Drawing.Size(797, 291);
            this.visitBox.TabIndex = 17;
            this.visitBox.TabStop = false;
            this.visitBox.Text = "Wizyta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(428, 88);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Wizyty lekarza:";
            // 
            // listaWizytLekarza
            // 
            this.listaWizytLekarza.AllowUserToAddRows = false;
            this.listaWizytLekarza.AllowUserToDeleteRows = false;
            this.listaWizytLekarza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaWizytLekarza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LpCol,
            this.TimeCol,
            this.PatientCol});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listaWizytLekarza.DefaultCellStyle = dataGridViewCellStyle1;
            this.listaWizytLekarza.Enabled = false;
            this.listaWizytLekarza.Location = new System.Drawing.Point(431, 105);
            this.listaWizytLekarza.MultiSelect = false;
            this.listaWizytLekarza.Name = "listaWizytLekarza";
            this.listaWizytLekarza.ReadOnly = true;
            this.listaWizytLekarza.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaWizytLekarza.Size = new System.Drawing.Size(345, 167);
            this.listaWizytLekarza.TabIndex = 18;
            this.listaWizytLekarza.SelectionChanged += new System.EventHandler(this.listaWizytLekarza_SelectionChanged);
            // 
            // LpCol
            // 
            this.LpCol.HeaderText = "#";
            this.LpCol.Name = "LpCol";
            this.LpCol.ReadOnly = true;
            this.LpCol.Width = 60;
            // 
            // TimeCol
            // 
            this.TimeCol.HeaderText = "Godzina";
            this.TimeCol.Name = "TimeCol";
            this.TimeCol.ReadOnly = true;
            this.TimeCol.Width = 120;
            // 
            // PatientCol
            // 
            this.PatientCol.HeaderText = "Pacjent";
            this.PatientCol.Name = "PatientCol";
            this.PatientCol.ReadOnly = true;
            this.PatientCol.Width = 120;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 88);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Lekarz:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Data i godzina:";
            // 
            // listaLekarzy
            // 
            this.listaLekarzy.AllowUserToAddRows = false;
            this.listaLekarzy.AllowUserToDeleteRows = false;
            this.listaLekarzy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaLekarzy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DocIDCol,
            this.nameCol,
            this.surnameCol,
            this.VisitCol});
            this.listaLekarzy.Location = new System.Drawing.Point(24, 105);
            this.listaLekarzy.MultiSelect = false;
            this.listaLekarzy.Name = "listaLekarzy";
            this.listaLekarzy.ReadOnly = true;
            this.listaLekarzy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaLekarzy.Size = new System.Drawing.Size(344, 167);
            this.listaLekarzy.TabIndex = 4;
            this.listaLekarzy.SelectionChanged += new System.EventHandler(this.listaLekarzy_SelectionChanged);
            // 
            // DocIDCol
            // 
            this.DocIDCol.HeaderText = "ID";
            this.DocIDCol.Name = "DocIDCol";
            this.DocIDCol.ReadOnly = true;
            this.DocIDCol.Visible = false;
            // 
            // nameCol
            // 
            this.nameCol.HeaderText = "Imię";
            this.nameCol.Name = "nameCol";
            this.nameCol.ReadOnly = true;
            // 
            // surnameCol
            // 
            this.surnameCol.HeaderText = "Nazwisko";
            this.surnameCol.Name = "surnameCol";
            this.surnameCol.ReadOnly = true;
            // 
            // VisitCol
            // 
            this.VisitCol.HeaderText = "Wizyty";
            this.VisitCol.Name = "VisitCol";
            this.VisitCol.ReadOnly = true;
            // 
            // DatePicker
            // 
            this.DatePicker.CustomFormat = "yyyy-MM-dd  HH:mm";
            this.DatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DatePicker.Location = new System.Drawing.Point(24, 53);
            this.DatePicker.Name = "DatePicker";
            this.DatePicker.Size = new System.Drawing.Size(192, 20);
            this.DatePicker.TabIndex = 0;
            this.DatePicker.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            this.DatePicker.ValueChanged += new System.EventHandler(this.DatePicker_ValueChanged);
            // 
            // patientDataBox
            // 
            this.patientDataBox.Controls.Add(this.peselLabel);
            this.patientDataBox.Controls.Add(this.surnameLabel);
            this.patientDataBox.Controls.Add(this.addressLabel);
            this.patientDataBox.Controls.Add(this.nameLabel);
            this.patientDataBox.Controls.Add(this.surnameTextBox);
            this.patientDataBox.Controls.Add(this.nameTextBox);
            this.patientDataBox.Controls.Add(this.peselTextBox);
            this.patientDataBox.Controls.Add(this.addressTextBox);
            this.patientDataBox.Location = new System.Drawing.Point(12, 11);
            this.patientDataBox.Name = "patientDataBox";
            this.patientDataBox.Size = new System.Drawing.Size(797, 118);
            this.patientDataBox.TabIndex = 16;
            this.patientDataBox.TabStop = false;
            this.patientDataBox.Text = "Dane pacjenta";
            // 
            // peselLabel
            // 
            this.peselLabel.AutoSize = true;
            this.peselLabel.Location = new System.Drawing.Point(289, 22);
            this.peselLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.peselLabel.Name = "peselLabel";
            this.peselLabel.Size = new System.Drawing.Size(36, 13);
            this.peselLabel.TabIndex = 14;
            this.peselLabel.Text = "Pesel:";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(21, 63);
            this.surnameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(56, 13);
            this.surnameLabel.TabIndex = 13;
            this.surnameLabel.Text = "Nazwisko:";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(289, 63);
            this.addressLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(37, 13);
            this.addressLabel.TabIndex = 15;
            this.addressLabel.Text = "Adres:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(21, 22);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(29, 13);
            this.nameLabel.TabIndex = 12;
            this.nameLabel.Text = "Imię:";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(24, 80);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.ReadOnly = true;
            this.surnameTextBox.Size = new System.Drawing.Size(189, 20);
            this.surnameTextBox.TabIndex = 9;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(24, 39);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(189, 20);
            this.nameTextBox.TabIndex = 8;
            // 
            // peselTextBox
            // 
            this.peselTextBox.Location = new System.Drawing.Point(292, 39);
            this.peselTextBox.Name = "peselTextBox";
            this.peselTextBox.ReadOnly = true;
            this.peselTextBox.Size = new System.Drawing.Size(188, 20);
            this.peselTextBox.TabIndex = 10;
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(292, 80);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.ReadOnly = true;
            this.addressTextBox.Size = new System.Drawing.Size(188, 20);
            this.addressTextBox.TabIndex = 11;
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(715, 432);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(94, 23);
            this.returnButton.TabIndex = 15;
            this.returnButton.Text = "Powrót";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // registerButton
            // 
            this.registerButton.Enabled = false;
            this.registerButton.Location = new System.Drawing.Point(515, 432);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(94, 23);
            this.registerButton.TabIndex = 14;
            this.registerButton.Text = "Rejestruj";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(615, 432);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(94, 23);
            this.backButton.TabIndex = 18;
            this.backButton.Text = "Wstecz";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // WizytaRejestracja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 467);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.visitBox);
            this.Controls.Add(this.patientDataBox);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.registerButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "WizytaRejestracja";
            this.ShowIcon = false;
            this.Text = "Rejestracja wizyty";
            this.Load += new System.EventHandler(this.WizytaRejestracja_Load);
            this.visitBox.ResumeLayout(false);
            this.visitBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaWizytLekarza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listaLekarzy)).EndInit();
            this.patientDataBox.ResumeLayout(false);
            this.patientDataBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox visitBox;
        private System.Windows.Forms.DataGridView listaLekarzy;
        private System.Windows.Forms.DateTimePicker DatePicker;
        private System.Windows.Forms.GroupBox patientDataBox;
        private System.Windows.Forms.Label peselLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox peselTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView listaWizytLekarza;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocIDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn VisitCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn LpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientCol;
        private System.Windows.Forms.Timer autorefresh;
    }
}