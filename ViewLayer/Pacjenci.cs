﻿using BusinessLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class Pacjenci : Form
    {
        public enum Tryb : byte { ZarzadzaniePacjentami, UstawieniaWizyty };
        private readonly Regex peselRegex = new Regex(@"^\d{11}$");
        private readonly ApplicationEngine engine;
        private readonly int idRejestratora;
        
        public Pacjenci(ApplicationEngine e, int idRejestratora, Tryb tryb)
        {
            InitializeComponent();
            engine = e;
            this.idRejestratora = idRejestratora;
            Laduj();
            PatientsList.MultiSelect = false;
            if(tryb == Tryb.ZarzadzaniePacjentami)
            {
                this.patientsManagerButtons.Visible = this.patientsManagerButtons.Enabled = true;
                this.visitConfigButtons.Visible = this.visitConfigButtons.Enabled = false;
                this.Text = "Pacjenci";
            }
            else
            {
                this.patientsManagerButtons.Visible = this.patientsManagerButtons.Enabled = false;
                this.visitConfigButtons.Visible = this.visitConfigButtons.Enabled = true;
                this.Text = "Wybierz pacjenta";
            }

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void Laduj()
        {
            PatientsList.Rows.Clear();
            new ManagerPacjentow.PoszukiwaczPacjentow(engine.PacjentManager)
                .Imie(NameTextBox.Text)
                .Nazwisko(SurnameTextBox.Text)
                .Pesel(PeselTextBox.Text)
                .Wykonaj(x => {
                    PatientsList.Rows.Add(x.Id, x.Imie, x.Nazwisko, x.Adres, x.Pesel);
                });
            PatientsList.ClearSelection();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var form = new Pacjent(engine))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
                
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                ErrorProvider.Clear();
                Laduj();
            }else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private bool IsUserInputValid()
        {
            return peselRegex.Match(PeselTextBox.Text).Success || (string.IsNullOrEmpty(PeselTextBox.Text));
        }

        private void ReturnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            var idPacjenta = (int)PatientsList.SelectedRows[0].Cells[0].Value;
            using (var form = new Pacjent(engine, idPacjenta))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }
        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if (PatientsList.SelectedRows.Count == 1)
            {
                var idPacjenta = (int)PatientsList.SelectedRows[0].Cells[0].Value;
                if (IsUserInputValid())
                {
                    ErrorProvider.Clear();
                    Laduj();
                }
                else
                {
                    ZaznaczNieprawidloweDane();
                }
                foreach (DataGridViewRow row in PatientsList.Rows)
                {
                    if ((int)row.Cells[0].Value == idPacjenta)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                Laduj();
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            var idPacjenta = (int)PatientsList.SelectedRows[0].Cells[0].Value;
            using (var form = new WizytaRejestracja(engine, idPacjenta, idRejestratora, this))
            {
                autorefresh.Stop();
                form.ShowDialog();
            }
            autorefresh.Start();
        }

        private void PatientsList_SelectionChanged(object sender, EventArgs e)
        {
            EditButton.Enabled = nextButton.Enabled = (PatientsList.SelectedRows.Count == 1) ? true : false;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            NameTextBox.Text = SurnameTextBox.Text = PeselTextBox.Text = string.Empty;
            Laduj();
        }

        private void Pacjenci_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (peselRegex.Match(PeselTextBox.Text).Success == false && !string.IsNullOrEmpty(PeselTextBox.Text))
                ErrorProvider.SetError(PeselTextBox, "Podaj prawidłowy PESEL \nlub zostaw puste pole");
        }
    }
}
