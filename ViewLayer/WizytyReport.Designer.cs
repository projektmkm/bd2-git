﻿namespace ViewLayer
{
    partial class WizytyReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Imie = new System.Windows.Forms.TextBox();
            this.Nazwisko = new System.Windows.Forms.TextBox();
            this.Pesel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DataOd = new System.Windows.Forms.DateTimePicker();
            this.DataDo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Stan = new System.Windows.Forms.ComboBox();
            this.Rejestrator = new System.Windows.Forms.ComboBox();
            this.Lekarz = new System.Windows.Forms.ComboBox();
            this.DataEnabled = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1014, 580);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 22);
            this.button1.TabIndex = 0;
            this.button1.Text = "Zapisz jako pdf";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 208);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1158, 366);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(588, 173);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Szukaj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(669, 173);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Wyczyść";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Imie
            // 
            this.Imie.Location = new System.Drawing.Point(176, 12);
            this.Imie.Name = "Imie";
            this.Imie.Size = new System.Drawing.Size(406, 20);
            this.Imie.TabIndex = 4;
            // 
            // Nazwisko
            // 
            this.Nazwisko.Location = new System.Drawing.Point(176, 38);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(406, 20);
            this.Nazwisko.TabIndex = 5;
            // 
            // Pesel
            // 
            this.Pesel.Location = new System.Drawing.Point(176, 64);
            this.Pesel.Name = "Pesel";
            this.Pesel.Size = new System.Drawing.Size(406, 20);
            this.Pesel.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Imię pacjenta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Nazwisko pacjenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Pesel pacjenta";
            // 
            // DataOd
            // 
            this.DataOd.Location = new System.Drawing.Point(176, 91);
            this.DataOd.Name = "DataOd";
            this.DataOd.Size = new System.Drawing.Size(200, 20);
            this.DataOd.TabIndex = 14;
            // 
            // DataDo
            // 
            this.DataDo.Location = new System.Drawing.Point(382, 91);
            this.DataDo.Name = "DataDo";
            this.DataDo.Size = new System.Drawing.Size(200, 20);
            this.DataDo.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Zakres Dat";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Stan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Rejestrator";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Lekarz";
            // 
            // Stan
            // 
            this.Stan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Stan.FormattingEnabled = true;
            this.Stan.Location = new System.Drawing.Point(176, 124);
            this.Stan.Name = "Stan";
            this.Stan.Size = new System.Drawing.Size(406, 21);
            this.Stan.TabIndex = 20;
            // 
            // Rejestrator
            // 
            this.Rejestrator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Rejestrator.FormattingEnabled = true;
            this.Rejestrator.Location = new System.Drawing.Point(176, 148);
            this.Rejestrator.Name = "Rejestrator";
            this.Rejestrator.Size = new System.Drawing.Size(406, 21);
            this.Rejestrator.TabIndex = 21;
            // 
            // Lekarz
            // 
            this.Lekarz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Lekarz.FormattingEnabled = true;
            this.Lekarz.Location = new System.Drawing.Point(176, 175);
            this.Lekarz.Name = "Lekarz";
            this.Lekarz.Size = new System.Drawing.Size(406, 21);
            this.Lekarz.TabIndex = 22;
            // 
            // DataEnabled
            // 
            this.DataEnabled.AutoSize = true;
            this.DataEnabled.Checked = true;
            this.DataEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DataEnabled.Location = new System.Drawing.Point(157, 94);
            this.DataEnabled.Name = "DataEnabled";
            this.DataEnabled.Size = new System.Drawing.Size(15, 14);
            this.DataEnabled.TabIndex = 23;
            this.DataEnabled.UseVisualStyleBackColor = true;
            this.DataEnabled.CheckedChanged += new System.EventHandler(this.DataEnabled_CheckedChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(11, 580);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 24;
            this.button4.Text = "Powrót";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // WizytyReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 607);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.DataEnabled);
            this.Controls.Add(this.Lekarz);
            this.Controls.Add(this.Rejestrator);
            this.Controls.Add(this.Stan);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DataDo);
            this.Controls.Add(this.DataOd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pesel);
            this.Controls.Add(this.Nazwisko);
            this.Controls.Add(this.Imie);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "WizytyReport";
            this.ShowIcon = false;
            this.Text = "Raport wizyt";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox Imie;
        private System.Windows.Forms.TextBox Nazwisko;
        private System.Windows.Forms.TextBox Pesel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DataOd;
        private System.Windows.Forms.DateTimePicker DataDo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox Stan;
        private System.Windows.Forms.ComboBox Rejestrator;
        private System.Windows.Forms.ComboBox Lekarz;
        private System.Windows.Forms.CheckBox DataEnabled;
        private System.Windows.Forms.Button button4;
    }
}