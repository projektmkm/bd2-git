﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewLayer;
//using Forms;
namespace Bd2
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            try
            {
                var e = new ApplicationLoader(exHandler);
                var form = new ViewLayer.Logowanie(e);
                form.ShowDialog();
            }
#if !DEBUG
            catch (Exception e)
            {
                exHandler(e);
            }
#endif
            finally
            {

            }

        }

        private static Action<Exception> exHandler = e =>
        {
            string msg = e.Message;
            e = e.InnerException;
            while (e != null)
            {
                msg += "\r\n===========================================\r\n";
                msg += e.Message;
                e = e.InnerException;
            }
            MessageBox.Show(msg, "exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
        };
    }

}
