﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public static class ReportFileDialog
    {
        public static string RaportSaveDialog()
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.FileName = "report.pdf";
            savefile.Filter = "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                return savefile.FileName;
            }
            else
            {
                return null;
            }
        }
    }
}
