﻿using BusinessLayer;
using DataLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using RolaKontaItem = Forms.ComboBoxItem<DataLayer.KontoRola?>;

namespace ViewLayer
{
    public partial class AdminUżytkownicyLista : Form
    {
        private readonly Regex LoginRegex = new Regex(@"^(\S)+$");
        private readonly ApplicationEngine engine;
        private bool odswiezSzukanychUzytkownikow = false;


        public AdminUżytkownicyLista(ApplicationEngine e)
        {
            InitializeComponent();
            engine = e;
            LadujWszystkichUzytkownikow();
            roleComboBox.Items.Add(new RolaKontaItem { Text = " ", Value = null });
            roleComboBox.Items.Add(new RolaKontaItem { Text = KontoRola.Administrator.ToString(), Value = KontoRola.Administrator });
            roleComboBox.Items.Add(new RolaKontaItem { Text = KontoRola.Lekarz.ToString(), Value = KontoRola.Lekarz });
            roleComboBox.Items.Add(new RolaKontaItem { Text = KontoRola.Laborant.ToString(), Value = KontoRola.Laborant });
            roleComboBox.Items.Add(new RolaKontaItem { Text = KontoRola.KierownikLaboratorium.ToString(), Value = KontoRola.KierownikLaboratorium });
            roleComboBox.Items.Add(new RolaKontaItem { Text = KontoRola.Rejestrator.ToString(), Value = KontoRola.Rejestrator });
            roleComboBox.SelectedIndex = 0;
        }

        private void LadujWszystkichUzytkownikow()
        {
            listaUzytkownikow.Rows.Clear();
            new ManagerKont.PoszukiwaczKont(engine.KontoManager)
                .Wykonaj(x => {
                    listaUzytkownikow.Rows.Add(x.Id, x.Login, x.Imie, x.Nazwisko, x.GetRole(), x.DataKonca);
                });
            listaUzytkownikow.ClearSelection();
        }

        private void LadujSzukanychUzytkownikow()
        {
            listaUzytkownikow.Rows.Clear();
            var poszukiwacz = new ManagerKont.PoszukiwaczKont(engine.KontoManager)
                .Imie(nameTextBox.Text)
                .Nazwisko(surnameTextBox.Text)
                .Login(loginTextBox.Text);
            var rola = (roleComboBox.SelectedItem as RolaKontaItem).Value;
            if (rola != null)
                poszukiwacz.Rola(rola.Value);
            poszukiwacz.Wykonaj(x => {
                listaUzytkownikow.Rows.Add(x.Id, x.Login, x.Imie, x.Nazwisko, x.GetRole(), x.DataKonca);
            });
            listaUzytkownikow.ClearSelection();
        }

        private void backBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchBt_Click(object sender, EventArgs e)
        {
            odswiezSzukanychUzytkownikow = true;
            LadujSzukanychUzytkownikow();
        }

        private void modifyBt_Click(object sender, EventArgs e)
        {
            var idUzytkownika = (int)listaUzytkownikow.SelectedRows[0].Cells[0].Value;
            using (var form = new AdminUżytkownik(engine, idUzytkownika))
            {
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void addBt_Click(object sender, EventArgs e)
        {
            using(var form = new AdminUżytkownik(engine))
            {
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void userList_SelectionChanged(object sender, EventArgs e)
        {
            modifyBt.Enabled = (listaUzytkownikow.SelectedRows.Count == 1) ? true : false;
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            if (listaUzytkownikow.SelectedRows.Count > 0)
            {
                var idUzytkownika = (int)listaUzytkownikow.SelectedRows[0].Cells[0].Value;
                if (odswiezSzukanychUzytkownikow == true)
                    LadujSzukanychUzytkownikow();
                else
                    LadujWszystkichUzytkownikow();
                foreach (DataGridViewRow row in listaUzytkownikow.Rows)
                {
                    if ((int)row.Cells[0].Value == idUzytkownika)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            nameTextBox.Text = surnameTextBox.Text = loginTextBox.Text = string.Empty;
            roleComboBox.SelectedIndex = 0;
            odswiezSzukanychUzytkownikow = false;
            LadujWszystkichUzytkownikow();
        }
    }
}
