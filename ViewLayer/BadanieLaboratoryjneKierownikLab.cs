﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class BadanieLaboratoryjneKierownikLab : Form
    {
        public BadanieLaboratoryjneKierownikLab(ApplicationEngine e, int idBadania, int idKierownika)
        {
            InitializeComponent();
            engine = e;
            this.idBadania = idBadania;
            this.idKierownika = idKierownika;

            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                .IdBadania(idBadania)
                .Wykonaj(x => {
                    idTextBox.Text = x.Id.ToString();
                    codeTextBox.Text = x.TypBadania.Kod;
                    nameTextBox.Text = x.TypBadania.Nazwa;
                    stateTextBox.Text = x.Stan.ToString();
                    docNoteTextBox.Text = x.UwagiLekarza;
                    resultTextBox.Text = x.Wynik;
                    okButton.Enabled = (x.Stan == DataLayer.StanBadania.Wykonane) ? true : false;
                });
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ErrorProvider.Clear();
            if (!string.IsNullOrWhiteSpace(noteTextBox.Text))
            {
                if (MessageBox.Show("Czy jesteś pewien?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.BadanieLaboratoryjneManager.AnulujBadanieKierownik(idBadania, idKierownika, noteTextBox.Text);
                    this.Close();
                }
            }else
            {
                ErrorProvider.SetError(noteTextBox, "Podaj przyczynę anulowania");
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorProvider.Clear();
            if (MessageBox.Show("Czy jesteś pewien?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                engine.BadanieLaboratoryjneManager.ZatwierdzBadanie(idBadania, idKierownika, noteTextBox.Text);
                this.Close();
            }
        }

        private readonly ApplicationEngine engine;
        private readonly int idBadania, idKierownika;
    }
}
