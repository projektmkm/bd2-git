﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewLayer;

namespace Forms
{
    public partial class Rejestrator : Form
    {
        private readonly ApplicationEngine engine;
        private readonly int idRejestratora;
        public Rejestrator(ApplicationEngine e, int idRejestratora)
        {
            InitializeComponent();
            engine = e;
            this.idRejestratora = idRejestratora;
        }

        private void regVisButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new Pacjenci(engine, idRejestratora, Pacjenci.Tryb.UstawieniaWizyty))
                form.ShowDialog();
            this.Show();
        }

        private void visitsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new Wizyty(engine, idRejestratora))
                form.ShowDialog();
            this.Show();
        }

        private void patientsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var form = new Pacjenci(engine, idRejestratora, Pacjenci.Tryb.ZarzadzaniePacjentami))
                form.ShowDialog();
            this.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            engine.KontoManager.Wyloguj();
        }
    }
}
