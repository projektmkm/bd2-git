﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComboItem = Forms.ComboBoxItem<int?>;

namespace ViewLayer
{
    public partial class WizytyReport : Form
    {

        public WizytyReport(ApplicationEngine e)
        {
            InitializeComponent();
            engine = e;
            loadCombo();
            clear();
            load();
        }
        private void loadCombo()
        {
            Stan.Items.Add(new ComboItem { Text = " ", Value = null });
            foreach (var e in Enum.GetValues(typeof(DataLayer.StanWizyty)))
            {
                Stan.Items.Add(new ComboItem { Text = ReportGenerator.ToSpaceCase(Enum.GetName(typeof(DataLayer.StanWizyty), e)), Value = (int)e });
            }
            Lekarz.Items.Add(new ComboItem { Text = " ", Value = null });
            new ManagerKont.PoszukiwaczKont(engine.KontoManager)
                .Rola(DataLayer.KontoRola.Lekarz)
                .Wykonaj(x => {
                    Lekarz.Items.Add(new ComboItem { Text = x.Imie + " " + x.Nazwisko, Value = x.Id });
                });

            new ManagerKont.PoszukiwaczKont(engine.KontoManager)
                .Rola(DataLayer.KontoRola.Rejestrator)
                .Wykonaj(x => {
                    Rejestrator.Items.Add(new ComboItem { Text = x.Imie + " " + x.Nazwisko, Value = x.Id });
                });
        }
        private void load()
        {
            DateTime? d1 = DataOd.Value;
            DateTime? d2 = DataDo.Value;
            if (!this.DataEnabled.Checked)
            {
                d1 = null;
                d2 = null;
            }
            data = engine.ReportGenerator.Wizyty(Imie.Text, Nazwisko.Text, Pesel.Text, d1, d2, (Stan.SelectedItem as ComboItem).Value, (Lekarz.SelectedItem as ComboItem).Value, (Rejestrator.SelectedItem as ComboItem).Value);
            this.dataGridView1.DataSource = data;
            button1.Enabled = data.Rows.Count != 0;
        }
        private void clear()
        {
            this.Imie.Text = "";
            this.Nazwisko.Text = "";
            this.Pesel.Text = "";
            Stan.SelectedIndex = 0;
            Lekarz.SelectedIndex = 0;
            Rejestrator.SelectedIndex = 0;
            DateTime t2 = DateTime.Now;
            DateTime t1 = t2.AddDays(-7);
            DataOd.Value = t1;
            DataDo.Value = t2;
            this.DataEnabled.Checked = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string fname = ReportFileDialog.RaportSaveDialog();
            if (fname != null)
            {
                var selected = new HashSet<int>();
                foreach (var row in dataGridView1.SelectedRows)
                {
                    var row2 = row as DataGridViewRow;
                    selected.Add(row2.Index);
                }
                if (selected.Count == 0)
                {
                    //selected = null;
                    foreach (var row in dataGridView1.Rows)
                    {
                        var row2 = row as DataGridViewRow;
                        selected.Add(row2.Index);
                    }
                }
                //ReportGenerator.DataTableToPdf(data, "Raport wizyt", fname, selected);
                try
                {
                    engine.ReportGenerator.WizytyReportPdf(selected.ToList(), fname, DataOd.Value, DataDo.Value);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "pdf export error");
                }
            }
        }
        private ApplicationEngine engine;
        DataTable data;

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            clear();
            load();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DataEnabled_CheckedChanged(object sender, EventArgs e)
        {
            DataOd.Enabled = DataDo.Enabled = (DataEnabled.Checked == true) ? true : false;
        }
    }
}
