﻿using BusinessLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class AdminUżytkownik : Form
    {
        private readonly Regex LoginRegex = new Regex(@"^(\S)+$");
        private readonly Regex PasswordRegex = new Regex(@"^(\S)+$");
        private readonly ApplicationEngine engine;
        private readonly int? idUzytkownika;
        private string poprzedniLoginUzytkownika;

        public AdminUżytkownik(ApplicationEngine e, int? idUzytkownika = null)
        {
            engine = e;
            InitializeComponent();
            roleComboBox.DataSource = Enum.GetValues(typeof(DataLayer.KontoRola));
            roleComboBox.SelectedIndex = 0;
            this.idUzytkownika = idUzytkownika;
            if (this.idUzytkownika != null)
            {
                this.Text = "Edytuj użytkownika";
                new ManagerKont.PoszukiwaczKont(engine.KontoManager)
                    .IdKonta(idUzytkownika.Value)
                    .Wykonaj(x => {
                        nameTextBox.Text = x.Imie;
                        surnameTextBox.Text = x.Nazwisko;
                        loginTextBox.Text = x.Login;
                        expDatePicker.Value = x.DataKonca.Value;
                        roleComboBox.SelectedItem = x.GetRole();
                    });
                roleComboBox.Enabled = passwdLabel.Visible = false;
                passwordCheckbox.Visible = true;
                poprzedniLoginUzytkownika = loginTextBox.Text;
            }
            else
            {
                this.Text = "Dodaj użytkownika";
                roleComboBox.Enabled = passwdLabel.Visible = passwordTextBox.Enabled = passwordCheckbox.Checked = true;
                passwordCheckbox.Visible = false;
            }
        }

        private void backBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool IsUserInputValid()
        {
            return (LoginRegex.Match(loginTextBox.Text).Success) && (passwordCheckbox.Checked ? PasswordRegex.Match(passwordTextBox.Text).Success : true)
                && (!string.IsNullOrWhiteSpace(nameTextBox.Text)) && (!string.IsNullOrWhiteSpace(surnameTextBox.Text));
        }

        private void acceptBt_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                if (idUzytkownika != null)
                {
                    if(poprzedniLoginUzytkownika != loginTextBox.Text)
                    {
                        if (!engine.KontoManager.CzyLoginWolny(loginTextBox.Text)){ 
                            MessageBox.Show("Podany login jest już zajęty!");
                            return;
                        }
                    }
                    string noweHaslo = (passwordCheckbox.Checked == true) ? passwordTextBox.Text : null;
                    engine.KontoManager.EdytujKonto(idUzytkownika.Value,
                                nameTextBox.Text,
                                surnameTextBox.Text,
                                loginTextBox.Text,
                                expDatePicker.Value,
                                noweHaslo);
                    Close();
                }
                else
                {
                    if (engine.KontoManager.CzyLoginWolny(loginTextBox.Text))
                    {
                        engine.KontoManager.DodajKonto(nameTextBox.Text,
                            surnameTextBox.Text,
                            loginTextBox.Text,
                            passwordTextBox.Text,
                            (KontoRola)roleComboBox.SelectedItem,
                            expDatePicker.Value);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Podany login jest już zajęty!");
                    }
                }
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (LoginRegex.Match(loginTextBox.Text).Success == false)
                ErrorProvider.SetError(loginTextBox, "Nieprawidłowy login");
            if (passwordCheckbox.Checked && PasswordRegex.Match(passwordTextBox.Text).Success == false)
                ErrorProvider.SetError(passwordTextBox, "Nieprawidłowe hasło");
            if (string.IsNullOrWhiteSpace(nameTextBox.Text))
                ErrorProvider.SetError(nameTextBox, "Podaj imię");
            if (string.IsNullOrWhiteSpace(surnameTextBox.Text))
                ErrorProvider.SetError(surnameTextBox, "Podaj nazwisko");
        }

        private void passwordCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            passwordTextBox.Enabled = passwordCheckbox.Checked;
            passwordTextBox.Text = "";
        }
    }
}
