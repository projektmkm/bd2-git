﻿using BusinessLayer;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Forms
{

    public partial class Pacjent : Form
    {
        public Pacjent(ApplicationEngine e, int? pacjentId = null)
        {
            InitializeComponent();
            engine = e;
            this.pacjentId = pacjentId;
            if (pacjentId != null)
            {
                this.Text = "Edytuj pacjenta";
                new ManagerPacjentow.PoszukiwaczPacjentow(engine.PacjentManager)
                    .IdPacjenta(pacjentId.Value)
                    .Wykonaj(x => {
                        NameTextBox.Text = x.Imie;
                        SurnameTextBox.Text = x.Nazwisko;
                        PeselTextBox.Text = x.Pesel;
                        addressTextBox.Text = x.Adres;
                    });
            }else
            {
                this.Text = "Nowy pacjent";
            }
        }

        private readonly Regex peselRegex = new Regex(@"^\d{11}$");

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                if (pacjentId != null)
                {
                    engine.PacjentManager.EdytujPacjenta(pacjentId.Value, NameTextBox.Text, SurnameTextBox.Text, PeselTextBox.Text, addressTextBox.Text);
                }
                else
                {
                    engine.PacjentManager.NowyPacjent( NameTextBox.Text, SurnameTextBox.Text, PeselTextBox.Text, addressTextBox.Text);
                }
                Close();
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private bool IsUserInputValid()
        {
            return peselRegex.Match(PeselTextBox.Text).Success && (!string.IsNullOrWhiteSpace(NameTextBox.Text))
                && (!string.IsNullOrWhiteSpace(SurnameTextBox.Text)) && (!string.IsNullOrWhiteSpace(addressTextBox.Text));
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (peselRegex.Match(PeselTextBox.Text).Success == false)
                ErrorProvider.SetError(PeselTextBox, "Nieprawidłowy PESEL\n(wymagany ciąg 11 cyfr)");
            if (string.IsNullOrWhiteSpace(NameTextBox.Text))
                ErrorProvider.SetError(NameTextBox, "Podaj imię");
            if (string.IsNullOrWhiteSpace(SurnameTextBox.Text))
                ErrorProvider.SetError(SurnameTextBox, "Podaj nazwisko");
            if (string.IsNullOrWhiteSpace(addressTextBox.Text))
                ErrorProvider.SetError(addressTextBox, "Podaj adres");
        }
        private readonly ApplicationEngine engine;
        private int? pacjentId;
    }
}
