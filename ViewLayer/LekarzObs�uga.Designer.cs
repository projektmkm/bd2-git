﻿namespace ViewLayer
{
    partial class LekarzObsługa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.patientDataBox = new System.Windows.Forms.GroupBox();
            this.peselLabel = new System.Windows.Forms.Label();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.peselTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.finDateTextBox = new System.Windows.Forms.TextBox();
            this.finDateLabel = new System.Windows.Forms.Label();
            this.showVisitsButton = new System.Windows.Forms.Button();
            this.showTestsButton = new System.Windows.Forms.Button();
            this.labTestButton = new System.Windows.Forms.Button();
            this.phisicTestButton = new System.Windows.Forms.Button();
            this.diagnosisTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.stateTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.regDateTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.finishButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.returnButton = new System.Windows.Forms.Button();
            this.visitHandleButtons = new System.Windows.Forms.Panel();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.patientDataBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.visitHandleButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // patientDataBox
            // 
            this.patientDataBox.Controls.Add(this.peselLabel);
            this.patientDataBox.Controls.Add(this.surnameLabel);
            this.patientDataBox.Controls.Add(this.addressLabel);
            this.patientDataBox.Controls.Add(this.nameLabel);
            this.patientDataBox.Controls.Add(this.surnameTextBox);
            this.patientDataBox.Controls.Add(this.nameTextBox);
            this.patientDataBox.Controls.Add(this.peselTextBox);
            this.patientDataBox.Controls.Add(this.addressTextBox);
            this.patientDataBox.Location = new System.Drawing.Point(12, 12);
            this.patientDataBox.Name = "patientDataBox";
            this.patientDataBox.Size = new System.Drawing.Size(527, 118);
            this.patientDataBox.TabIndex = 17;
            this.patientDataBox.TabStop = false;
            this.patientDataBox.Text = "Dane pacjenta";
            // 
            // peselLabel
            // 
            this.peselLabel.AutoSize = true;
            this.peselLabel.Location = new System.Drawing.Point(289, 22);
            this.peselLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.peselLabel.Name = "peselLabel";
            this.peselLabel.Size = new System.Drawing.Size(36, 13);
            this.peselLabel.TabIndex = 14;
            this.peselLabel.Text = "Pesel:";
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(21, 63);
            this.surnameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(56, 13);
            this.surnameLabel.TabIndex = 13;
            this.surnameLabel.Text = "Nazwisko:";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(289, 63);
            this.addressLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(37, 13);
            this.addressLabel.TabIndex = 15;
            this.addressLabel.Text = "Adres:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(21, 22);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(29, 13);
            this.nameLabel.TabIndex = 12;
            this.nameLabel.Text = "Imię:";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(24, 80);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.ReadOnly = true;
            this.surnameTextBox.Size = new System.Drawing.Size(189, 20);
            this.surnameTextBox.TabIndex = 9;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(24, 39);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(189, 20);
            this.nameTextBox.TabIndex = 8;
            // 
            // peselTextBox
            // 
            this.peselTextBox.Location = new System.Drawing.Point(292, 39);
            this.peselTextBox.Name = "peselTextBox";
            this.peselTextBox.ReadOnly = true;
            this.peselTextBox.Size = new System.Drawing.Size(188, 20);
            this.peselTextBox.TabIndex = 10;
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(292, 80);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.ReadOnly = true;
            this.addressTextBox.Size = new System.Drawing.Size(188, 20);
            this.addressTextBox.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.finDateTextBox);
            this.groupBox1.Controls.Add(this.finDateLabel);
            this.groupBox1.Controls.Add(this.showVisitsButton);
            this.groupBox1.Controls.Add(this.showTestsButton);
            this.groupBox1.Controls.Add(this.labTestButton);
            this.groupBox1.Controls.Add(this.phisicTestButton);
            this.groupBox1.Controls.Add(this.diagnosisTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.descriptionTextBox);
            this.groupBox1.Controls.Add(this.stateTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.regDateTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(527, 331);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wizyta";
            // 
            // finDateTextBox
            // 
            this.finDateTextBox.Location = new System.Drawing.Point(292, 124);
            this.finDateTextBox.Name = "finDateTextBox";
            this.finDateTextBox.ReadOnly = true;
            this.finDateTextBox.Size = new System.Drawing.Size(189, 20);
            this.finDateTextBox.TabIndex = 41;
            // 
            // finDateLabel
            // 
            this.finDateLabel.AutoSize = true;
            this.finDateLabel.Location = new System.Drawing.Point(289, 107);
            this.finDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.finDateLabel.Name = "finDateLabel";
            this.finDateLabel.Size = new System.Drawing.Size(96, 13);
            this.finDateLabel.TabIndex = 42;
            this.finDateLabel.Text = "Data zakończenia:";
            // 
            // showVisitsButton
            // 
            this.showVisitsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.showVisitsButton.Location = new System.Drawing.Point(404, 232);
            this.showVisitsButton.Name = "showVisitsButton";
            this.showVisitsButton.Size = new System.Drawing.Size(105, 45);
            this.showVisitsButton.TabIndex = 40;
            this.showVisitsButton.Text = "Pokaż wizyty";
            this.showVisitsButton.UseVisualStyleBackColor = true;
            this.showVisitsButton.Click += new System.EventHandler(this.showVisitsButton_Click);
            // 
            // showTestsButton
            // 
            this.showTestsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.showTestsButton.Location = new System.Drawing.Point(293, 232);
            this.showTestsButton.Name = "showTestsButton";
            this.showTestsButton.Size = new System.Drawing.Size(105, 45);
            this.showTestsButton.TabIndex = 39;
            this.showTestsButton.Text = "Pokaż badania";
            this.showTestsButton.UseVisualStyleBackColor = true;
            this.showTestsButton.Click += new System.EventHandler(this.showTestsButton_Click);
            // 
            // labTestButton
            // 
            this.labTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labTestButton.Location = new System.Drawing.Point(404, 181);
            this.labTestButton.Name = "labTestButton";
            this.labTestButton.Size = new System.Drawing.Size(105, 45);
            this.labTestButton.TabIndex = 38;
            this.labTestButton.Text = "Zleć badanie laboratoryjne";
            this.labTestButton.UseVisualStyleBackColor = true;
            this.labTestButton.Click += new System.EventHandler(this.labTestButton_Click);
            // 
            // phisicTestButton
            // 
            this.phisicTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.phisicTestButton.Location = new System.Drawing.Point(293, 181);
            this.phisicTestButton.Name = "phisicTestButton";
            this.phisicTestButton.Size = new System.Drawing.Size(105, 45);
            this.phisicTestButton.TabIndex = 37;
            this.phisicTestButton.Text = "Wykonaj badanie fizykalne";
            this.phisicTestButton.UseVisualStyleBackColor = true;
            this.phisicTestButton.Click += new System.EventHandler(this.phisicTestButton_Click);
            // 
            // diagnosisTextBox
            // 
            this.diagnosisTextBox.Location = new System.Drawing.Point(24, 181);
            this.diagnosisTextBox.Multiline = true;
            this.diagnosisTextBox.Name = "diagnosisTextBox";
            this.diagnosisTextBox.Size = new System.Drawing.Size(234, 120);
            this.diagnosisTextBox.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 164);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Diagnoza:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Opis:";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(24, 42);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(234, 102);
            this.descriptionTextBox.TabIndex = 33;
            // 
            // stateTextBox
            // 
            this.stateTextBox.Location = new System.Drawing.Point(292, 42);
            this.stateTextBox.Name = "stateTextBox";
            this.stateTextBox.ReadOnly = true;
            this.stateTextBox.Size = new System.Drawing.Size(189, 20);
            this.stateTextBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Stan:";
            // 
            // regDateTextBox
            // 
            this.regDateTextBox.Location = new System.Drawing.Point(292, 83);
            this.regDateTextBox.Name = "regDateTextBox";
            this.regDateTextBox.ReadOnly = true;
            this.regDateTextBox.Size = new System.Drawing.Size(189, 20);
            this.regDateTextBox.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Data rejestracji:";
            // 
            // finishButton
            // 
            this.finishButton.Location = new System.Drawing.Point(1, 3);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(123, 23);
            this.finishButton.TabIndex = 30;
            this.finishButton.Text = "Zakończ";
            this.finishButton.UseVisualStyleBackColor = true;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(130, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(123, 23);
            this.cancelButton.TabIndex = 29;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(416, 473);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(123, 23);
            this.returnButton.TabIndex = 28;
            this.returnButton.Text = "Powrót";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // visitHandleButtons
            // 
            this.visitHandleButtons.Controls.Add(this.finishButton);
            this.visitHandleButtons.Controls.Add(this.cancelButton);
            this.visitHandleButtons.Location = new System.Drawing.Point(158, 470);
            this.visitHandleButtons.Name = "visitHandleButtons";
            this.visitHandleButtons.Size = new System.Drawing.Size(256, 30);
            this.visitHandleButtons.TabIndex = 31;
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.ErrorProvider.ContainerControl = this;
            // 
            // LekarzObsługa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 508);
            this.Controls.Add(this.visitHandleButtons);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.patientDataBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LekarzObsługa";
            this.ShowIcon = false;
            this.Text = "Wizyta";
            this.patientDataBox.ResumeLayout(false);
            this.patientDataBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.visitHandleButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox patientDataBox;
        private System.Windows.Forms.Label peselLabel;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox peselTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox stateTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button showVisitsButton;
        private System.Windows.Forms.Button showTestsButton;
        private System.Windows.Forms.Button labTestButton;
        private System.Windows.Forms.Button phisicTestButton;
        private System.Windows.Forms.TextBox diagnosisTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox regDateTextBox;
        private System.Windows.Forms.Button finishButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.TextBox finDateTextBox;
        private System.Windows.Forms.Label finDateLabel;
        private System.Windows.Forms.Panel visitHandleButtons;
        private System.Windows.Forms.ErrorProvider ErrorProvider;
    }
}