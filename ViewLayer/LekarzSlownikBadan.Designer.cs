﻿namespace ViewLayer
{
    partial class LekarzSlownikBadan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.slownikBadanList = new System.Windows.Forms.DataGridView();
            this.acceptBt = new System.Windows.Forms.Button();
            this.backBt = new System.Windows.Forms.Button();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.slownikBadanList)).BeginInit();
            this.SuspendLayout();
            // 
            // slownikBadanList
            // 
            this.slownikBadanList.AllowUserToAddRows = false;
            this.slownikBadanList.AllowUserToDeleteRows = false;
            this.slownikBadanList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slownikBadanList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.CodeCol,
            this.NameCol,
            this.Description});
            this.slownikBadanList.Location = new System.Drawing.Point(12, 12);
            this.slownikBadanList.MultiSelect = false;
            this.slownikBadanList.Name = "slownikBadanList";
            this.slownikBadanList.ReadOnly = true;
            this.slownikBadanList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.slownikBadanList.Size = new System.Drawing.Size(405, 199);
            this.slownikBadanList.TabIndex = 43;
            this.slownikBadanList.SelectionChanged += new System.EventHandler(this.slownikBadanList_SelectionChanged);
            // 
            // acceptBt
            // 
            this.acceptBt.Enabled = false;
            this.acceptBt.Location = new System.Drawing.Point(165, 226);
            this.acceptBt.Name = "acceptBt";
            this.acceptBt.Size = new System.Drawing.Size(123, 23);
            this.acceptBt.TabIndex = 42;
            this.acceptBt.Text = "Wybierz";
            this.acceptBt.UseVisualStyleBackColor = true;
            this.acceptBt.Click += new System.EventHandler(this.acceptBt_Click);
            // 
            // backBt
            // 
            this.backBt.Location = new System.Drawing.Point(294, 226);
            this.backBt.Name = "backBt";
            this.backBt.Size = new System.Drawing.Size(123, 23);
            this.backBt.TabIndex = 41;
            this.backBt.Text = "Powrót";
            this.backBt.UseVisualStyleBackColor = true;
            this.backBt.Click += new System.EventHandler(this.backBt_Click);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "ID";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Visible = false;
            // 
            // CodeCol
            // 
            this.CodeCol.HeaderText = "Kod";
            this.CodeCol.Name = "CodeCol";
            this.CodeCol.ReadOnly = true;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Nazwa";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.HeaderText = "Opis";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 160;
            // 
            // LekarzSlownikBadan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 261);
            this.Controls.Add(this.slownikBadanList);
            this.Controls.Add(this.acceptBt);
            this.Controls.Add(this.backBt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LekarzSlownikBadan";
            this.ShowIcon = false;
            this.Text = "Słownik badań";
            ((System.ComponentModel.ISupportInitialize)(this.slownikBadanList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView slownikBadanList;
        private System.Windows.Forms.Button acceptBt;
        private System.Windows.Forms.Button backBt;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
    }
}