﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewLayer;
using KontoIdItem = Forms.ComboBoxItem<int?>;
using LabYesNoItem = Forms.ComboBoxItem<bool?>;

namespace Forms
{
    public partial class LekarzWizytyLista : Form
    {
        public LekarzWizytyLista(ApplicationEngine e, int idPacjenta, int idObslugiwanejWizyty)
        {
            InitializeComponent();
            engine = e;
            this.idPacjenta = idPacjenta;
            this.idObslugiwanejWizyty = idObslugiwanejWizyty;

            KontoIdItem dowolny = new KontoIdItem { Text = " ", Value = null };
            doctorComboBox.Items.Add(dowolny);

            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
               .Wykonaj(x => {
                   doctorComboBox.Items.Add(new KontoIdItem { Text = x.Nazwisko + " " + x.Imie, Value = x.Id });
               });

            labStateComboBox.Items.Add(new LabYesNoItem { Text = " ", Value = null });
            labStateComboBox.Items.Add(new LabYesNoItem { Text = "Tak", Value = true });
            labStateComboBox.Items.Add(new LabYesNoItem { Text = "Nie", Value = false });

            UstawDomyslneWartosci();
            LadujWizyty();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void clearButton_Click(object sender, EventArgs e)
        {
            UstawDomyslneWartosci();
            odswiezSzukaneWizyty = false;
            LadujWizyty();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            odswiezSzukaneWizyty = true;
            LadujSzukaneWizyty();
        }

        private void ShowButton_Click(object sender, EventArgs e)
        {
            int idWizyty = (int)ListaWizyt.SelectedRows[0].Cells[0].Value;
            using (var form = new LekarzObsługa(engine, idWizyty, LekarzObsługa.Tryb.PokarzWizytePacjenta))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void UstawDomyslneWartosci()
        {
            diagnosisTextBox.Text = string.Empty;
            dataRejestracji1.Value = dataZakonczenia1.Value = DateTime.Now.AddYears(-1);
            dataRejestracji2.Value = dataZakonczenia2.Value = DateTime.Now;
            doctorComboBox.SelectedIndex = labStateComboBox.SelectedIndex = 0;
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled = regDateCheckBox.Checked =
               finDateLabel.Enabled = dataZakonczenia1.Enabled = dataZakonczenia2.Enabled = finDateCheckBox.Checked = false;
        }

        private void ListaWizyt_SelectionChanged(object sender, EventArgs e)
        {
            ShowButton.Enabled = (ListaWizyt.SelectedRows.Count == 1) ? true : false;
        }

        private void LadujWizyty()
        {
            ListaWizyt.Rows.Clear();
            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .IdPacjenta(idPacjenta)
                .Stan(DataLayer.StanWizyty.Wykonana)
                .Wykonaj(x => {
                    if (x.IdWizyty != idObslugiwanejWizyty)
                        ListaWizyt.Rows.Add(x.IdWizyty, x.DataRejestracji, x.DataZakonczenia, x.Lekarz.Nazwisko + " " + x.Lekarz.Imie,
                            x.BadaniaLaboratoryjne.Count, x.Diagnoza);
                });
        }

        private void LadujSzukaneWizyty()
        {
            ListaWizyt.Rows.Clear();
            var poszukiwacz = new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .Diagnoza(diagnosisTextBox.Text)
                .IdPacjenta(idPacjenta)
                .Stan(DataLayer.StanWizyty.Wykonana);
            if (regDateCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataRejestracji(dataRejestracji1.Value)
                            .NajpozniejszaDataRejestracji(dataRejestracji2.Value);
            if (finDateCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataZakonczenia(dataZakonczenia1.Value)
                            .NajpozniejszaDataZakonczenia(dataZakonczenia2.Value);
            var idLekarza = (doctorComboBox.SelectedItem as KontoIdItem).Value;
            if (idLekarza != null)
                poszukiwacz.IdLekarza(idLekarza.Value);
            var labYesNo = (labStateComboBox.SelectedItem as LabYesNoItem).Value;
            if (labYesNo != null)
                poszukiwacz.PosiadaBadaniaLaboratoryjne(labYesNo.Value);
            poszukiwacz.Wykonaj(x => {
                if (x.IdWizyty != idObslugiwanejWizyty)
                    ListaWizyt.Rows.Add(x.IdWizyty, x.DataRejestracji, x.DataZakonczenia, x.Lekarz.Nazwisko + " " + x.Lekarz.Imie,
                        x.BadaniaLaboratoryjne.Count, x.Diagnoza);
            });
            ListaWizyt.Sort(ListaWizyt.Columns[1], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if(ListaWizyt.SelectedRows.Count == 1)
            {
                int idWizyty = (int)ListaWizyt.SelectedRows[0].Cells[0].Value;
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujWizyty();
                foreach (DataGridViewRow row in ListaWizyt.Rows)
                {
                    if ((int)row.Cells[0].Value == idWizyty)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujWizyty();
            }
        }

        private readonly ApplicationEngine engine;
        private readonly int idPacjenta;
        private readonly int idObslugiwanejWizyty;
        private bool odswiezSzukaneWizyty = false;

        private void LekarzWizytyLista_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void regDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled =
                (regDateCheckBox.Checked == true) ? true : false;
        }

        private void finDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            finDateLabel.Enabled = dataZakonczenia1.Enabled = dataZakonczenia2.Enabled =
                (finDateCheckBox.Checked == true) ? true : false;
        }
    }
}
