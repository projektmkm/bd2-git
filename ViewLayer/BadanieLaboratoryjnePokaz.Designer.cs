﻿namespace Forms
{
    partial class BadanieLaboratoryjnePokaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stateTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.date3TextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.date2TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.labNoteTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.date1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.docNoteTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.codeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.returnButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.date4TextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // stateTextBox
            // 
            this.stateTextBox.Location = new System.Drawing.Point(152, 26);
            this.stateTextBox.Name = "stateTextBox";
            this.stateTextBox.ReadOnly = true;
            this.stateTextBox.Size = new System.Drawing.Size(124, 20);
            this.stateTextBox.TabIndex = 157;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(149, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 156;
            this.label10.Text = "Stan:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(299, 91);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 155;
            this.label9.Text = "Data zatwierdzenia:";
            // 
            // date3TextBox
            // 
            this.date3TextBox.Location = new System.Drawing.Point(302, 108);
            this.date3TextBox.Name = "date3TextBox";
            this.date3TextBox.ReadOnly = true;
            this.date3TextBox.Size = new System.Drawing.Size(211, 20);
            this.date3TextBox.TabIndex = 154;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 153;
            this.label6.Text = "Data wykonania:";
            // 
            // date2TextBox
            // 
            this.date2TextBox.Location = new System.Drawing.Point(302, 67);
            this.date2TextBox.Name = "date2TextBox";
            this.date2TextBox.ReadOnly = true;
            this.date2TextBox.Size = new System.Drawing.Size(211, 20);
            this.date2TextBox.TabIndex = 152;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 376);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 13);
            this.label8.TabIndex = 151;
            this.label8.Text = "Uwagi kierownika laboratorium:";
            // 
            // labNoteTextBox
            // 
            this.labNoteTextBox.Location = new System.Drawing.Point(14, 393);
            this.labNoteTextBox.Multiline = true;
            this.labNoteTextBox.Name = "labNoteTextBox";
            this.labNoteTextBox.ReadOnly = true;
            this.labNoteTextBox.Size = new System.Drawing.Size(499, 77);
            this.labNoteTextBox.TabIndex = 150;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 149;
            this.label7.Text = "Wynik:";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(14, 197);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.Size = new System.Drawing.Size(499, 77);
            this.resultTextBox.TabIndex = 148;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(299, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 147;
            this.label3.Text = "Data zlecenia:";
            // 
            // date1TextBox
            // 
            this.date1TextBox.Location = new System.Drawing.Point(302, 26);
            this.date1TextBox.Name = "date1TextBox";
            this.date1TextBox.ReadOnly = true;
            this.date1TextBox.Size = new System.Drawing.Size(211, 20);
            this.date1TextBox.TabIndex = 146;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 145;
            this.label2.Text = "Nazwa:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(13, 108);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(124, 20);
            this.nameTextBox.TabIndex = 144;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 278);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 143;
            this.label5.Text = "Uwagi lekarza:";
            // 
            // docNoteTextBox
            // 
            this.docNoteTextBox.Location = new System.Drawing.Point(14, 295);
            this.docNoteTextBox.Multiline = true;
            this.docNoteTextBox.Name = "docNoteTextBox";
            this.docNoteTextBox.ReadOnly = true;
            this.docNoteTextBox.Size = new System.Drawing.Size(499, 77);
            this.docNoteTextBox.TabIndex = 142;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 50);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 141;
            this.label4.Text = "Kod:";
            // 
            // codeTextBox
            // 
            this.codeTextBox.Location = new System.Drawing.Point(13, 67);
            this.codeTextBox.Name = "codeTextBox";
            this.codeTextBox.ReadOnly = true;
            this.codeTextBox.Size = new System.Drawing.Size(124, 20);
            this.codeTextBox.TabIndex = 140;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 139;
            this.label1.Text = "ID Badania:";
            // 
            // idTextBox
            // 
            this.idTextBox.Location = new System.Drawing.Point(13, 26);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.ReadOnly = true;
            this.idTextBox.Size = new System.Drawing.Size(124, 20);
            this.idTextBox.TabIndex = 138;
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(438, 476);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(75, 23);
            this.returnButton.TabIndex = 137;
            this.returnButton.Text = "Powrót";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(299, 132);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 159;
            this.label11.Text = "Data anulowania:";
            // 
            // date4TextBox
            // 
            this.date4TextBox.Location = new System.Drawing.Point(302, 149);
            this.date4TextBox.Name = "date4TextBox";
            this.date4TextBox.ReadOnly = true;
            this.date4TextBox.Size = new System.Drawing.Size(211, 20);
            this.date4TextBox.TabIndex = 158;
            // 
            // BadanieLaboratoryjnePokaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 509);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.date4TextBox);
            this.Controls.Add(this.stateTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.date3TextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.date2TextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labNoteTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.date1TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.docNoteTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.codeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(this.returnButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "BadanieLaboratoryjnePokaz";
            this.ShowIcon = false;
            this.Text = "Badanie laboratoryjne";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox stateTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox date3TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox date2TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox labNoteTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox date1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox docNoteTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox codeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox date4TextBox;
    }
}