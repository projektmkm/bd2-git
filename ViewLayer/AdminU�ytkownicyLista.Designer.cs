﻿namespace ViewLayer
{
    partial class AdminUżytkownicyLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.roleComboBox = new System.Windows.Forms.ComboBox();
            this.listaUzytkownikow = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoginCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expDateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addBt = new System.Windows.Forms.Button();
            this.modifyBt = new System.Windows.Forms.Button();
            this.backBt = new System.Windows.Forms.Button();
            this.searchBt = new System.Windows.Forms.Button();
            this.loginLabel = new System.Windows.Forms.Label();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listaUzytkownikow)).BeginInit();
            this.SuspendLayout();
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(13, 54);
            this.surnameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(56, 13);
            this.surnameLabel.TabIndex = 76;
            this.surnameLabel.Text = "Nazwisko:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(13, 12);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(29, 13);
            this.nameLabel.TabIndex = 75;
            this.nameLabel.Text = "Imię:";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Location = new System.Drawing.Point(16, 72);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(218, 20);
            this.surnameTextBox.TabIndex = 74;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(16, 30);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(218, 20);
            this.nameTextBox.TabIndex = 73;
            // 
            // roleComboBox
            // 
            this.roleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.roleComboBox.FormattingEnabled = true;
            this.roleComboBox.Location = new System.Drawing.Point(276, 72);
            this.roleComboBox.Name = "roleComboBox";
            this.roleComboBox.Size = new System.Drawing.Size(170, 21);
            this.roleComboBox.TabIndex = 72;
            // 
            // listaUzytkownikow
            // 
            this.listaUzytkownikow.AllowUserToAddRows = false;
            this.listaUzytkownikow.AllowUserToDeleteRows = false;
            this.listaUzytkownikow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaUzytkownikow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.LoginCol,
            this.NameCol,
            this.SurnameCol,
            this.RoleCol,
            this.expDateCol});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listaUzytkownikow.DefaultCellStyle = dataGridViewCellStyle1;
            this.listaUzytkownikow.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.listaUzytkownikow.Location = new System.Drawing.Point(16, 150);
            this.listaUzytkownikow.MultiSelect = false;
            this.listaUzytkownikow.Name = "listaUzytkownikow";
            this.listaUzytkownikow.ReadOnly = true;
            this.listaUzytkownikow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaUzytkownikow.Size = new System.Drawing.Size(716, 225);
            this.listaUzytkownikow.TabIndex = 71;
            this.listaUzytkownikow.SelectionChanged += new System.EventHandler(this.userList_SelectionChanged);
            // 
            // ID
            // 
            this.ID.HeaderText = "IDCol";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // LoginCol
            // 
            this.LoginCol.HeaderText = "Login";
            this.LoginCol.Name = "LoginCol";
            this.LoginCol.ReadOnly = true;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Width = 130;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            this.SurnameCol.Width = 143;
            // 
            // RoleCol
            // 
            this.RoleCol.HeaderText = "Rola";
            this.RoleCol.Name = "RoleCol";
            this.RoleCol.ReadOnly = true;
            // 
            // expDateCol
            // 
            this.expDateCol.HeaderText = "Data ważności";
            this.expDateCol.Name = "expDateCol";
            this.expDateCol.ReadOnly = true;
            this.expDateCol.Width = 200;
            // 
            // addBt
            // 
            this.addBt.Location = new System.Drawing.Point(352, 381);
            this.addBt.Name = "addBt";
            this.addBt.Size = new System.Drawing.Size(123, 23);
            this.addBt.TabIndex = 70;
            this.addBt.Text = "Dodaj";
            this.addBt.UseVisualStyleBackColor = true;
            this.addBt.Click += new System.EventHandler(this.addBt_Click);
            // 
            // modifyBt
            // 
            this.modifyBt.Enabled = false;
            this.modifyBt.Location = new System.Drawing.Point(480, 381);
            this.modifyBt.Name = "modifyBt";
            this.modifyBt.Size = new System.Drawing.Size(123, 23);
            this.modifyBt.TabIndex = 69;
            this.modifyBt.Text = "Edytuj";
            this.modifyBt.UseVisualStyleBackColor = true;
            this.modifyBt.Click += new System.EventHandler(this.modifyBt_Click);
            // 
            // backBt
            // 
            this.backBt.Location = new System.Drawing.Point(609, 381);
            this.backBt.Name = "backBt";
            this.backBt.Size = new System.Drawing.Size(123, 23);
            this.backBt.TabIndex = 68;
            this.backBt.Text = "Powrót";
            this.backBt.UseVisualStyleBackColor = true;
            this.backBt.Click += new System.EventHandler(this.backBt_Click);
            // 
            // searchBt
            // 
            this.searchBt.Location = new System.Drawing.Point(480, 121);
            this.searchBt.Name = "searchBt";
            this.searchBt.Size = new System.Drawing.Size(123, 23);
            this.searchBt.TabIndex = 67;
            this.searchBt.Text = "Szukaj";
            this.searchBt.UseVisualStyleBackColor = true;
            this.searchBt.Click += new System.EventHandler(this.searchBt_Click);
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Location = new System.Drawing.Point(273, 12);
            this.loginLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(36, 13);
            this.loginLabel.TabIndex = 64;
            this.loginLabel.Text = "Login:";
            // 
            // loginTextBox
            // 
            this.loginTextBox.Location = new System.Drawing.Point(276, 30);
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(218, 20);
            this.loginTextBox.TabIndex = 63;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Rola:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(609, 121);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 77;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // AdminUżytkownicyLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 416);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.roleComboBox);
            this.Controls.Add(this.listaUzytkownikow);
            this.Controls.Add(this.addBt);
            this.Controls.Add(this.modifyBt);
            this.Controls.Add(this.backBt);
            this.Controls.Add(this.searchBt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loginLabel);
            this.Controls.Add(this.loginTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AdminUżytkownicyLista";
            this.ShowIcon = false;
            this.Text = "Lista użytkowników";
            ((System.ComponentModel.ISupportInitialize)(this.listaUzytkownikow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.ComboBox roleComboBox;
        private System.Windows.Forms.DataGridView listaUzytkownikow;
        private System.Windows.Forms.Button addBt;
        private System.Windows.Forms.Button modifyBt;
        private System.Windows.Forms.Button backBt;
        private System.Windows.Forms.Button searchBt;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.TextBox loginTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoginCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn expDateCol;
    }
}