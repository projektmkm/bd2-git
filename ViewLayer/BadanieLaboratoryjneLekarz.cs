﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewLayer;

namespace Forms
{
    public partial class BadanieLaboratoryjneLekarz : Form
    {
        public BadanieLaboratoryjneLekarz(ApplicationEngine e, int idWizyty)
        {
            InitializeComponent();
            engine = e;
            this.idWizyty = idWizyty;
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {

            if (idTypuBadania != null)
            {
                ErrorProvider.Clear();
                if (MessageBox.Show("Czy jesteś pewien?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.WizytaManager.DodajBadanieLaboratoryjne(idWizyty, idTypuBadania.Value, noteTextBox.Text);
                    this.Close();
                }
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (idTypuBadania == null)
                ErrorProvider.SetError(testDictionaryBt, "Uzupełnij typ badania");
        }

        private void testDictionaryBt_Click(object sender, EventArgs e)
        {
            using (var form = new LekarzSlownikBadan(engine, LekarzSlownikBadan.Tryb.Lab, x => idTypuBadania = x))
                form.ShowDialog();
            if (idTypuBadania != null)
            {
                new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .IdBadania(idTypuBadania.Value)
                .Wykonaj(x => {
                    codeTextBox.Text = x.Kod;
                    nameTextBox.Text = x.Nazwa;
                });
            }
        }

        private readonly ApplicationEngine engine;
        private readonly int idWizyty;
        private int? idTypuBadania = null;
    }
}
