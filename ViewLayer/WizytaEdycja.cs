﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class WizytaEdycja : Form
    {
        public WizytaEdycja(ApplicationEngine e, int idWizyty)
        {
            InitializeComponent();
            engine = e;
            this.idWizyty = idWizyty;
            int idAktualnegoLekarza = 0;
            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                    .IdWizyty(idWizyty)
                    .Wykonaj(x =>
                    {
                        nameTextBox.Text = x.Pacjent.Imie;
                        surnameTextBox.Text = x.Pacjent.Nazwisko;
                        peselTextBox.Text = x.Pacjent.Pesel;
                        addressTextBox.Text = x.Pacjent.Adres;
                        this.DatePicker.Value = x.DataRejestracji;
                        idAktualnegoLekarza = x.Lekarz.Id;
                    });
          
            LadujLekarzy();
            this.Load += ((x,y) => {
                foreach (DataGridViewRow row in listaLekarzy.Rows)
                {
                    if ((int)row.Cells[0].Value == idAktualnegoLekarza)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            });
            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshShort;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private readonly ApplicationEngine engine;
        private readonly int idWizyty;
        private bool lekarzeZaladowani = false;

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy jesteś pewien?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                engine.WizytaManager.EdytujWizyte(idWizyty, idLekarza, DatePicker.Value);
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy jesteś pewien?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                engine.WizytaManager.AnulujWizyte(idWizyty);
                this.Close();
            }
        }

        private void listaLekarzy_SelectionChanged(object sender, EventArgs e)
        {
            if(listaLekarzy.SelectedRows.Count == 1)
            {
                if (lekarzeZaladowani == true)
                    LadujWizytyLekarza();
                saveButton.Enabled = true;
            }
            else
            {
                saveButton.Enabled = false;
            }
        }

        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void LadujWizytyLekarza()
        {
            listaWizytLekarza.Rows.Clear();
            if (listaLekarzy.SelectedRows.Count == 1)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
                    .IdLekarza(idLekarza)
                    .Wykonaj(x => {
                        foreach (var wizyta in x.Wizyty)
                        {
                            if ((wizyta.Stan != DataLayer.StanWizyty.Anulowana)
                                && (wizyta.DataRejestracji.Date.Equals(DatePicker.Value.Date))
                                && (wizyta.IdWizyty != idWizyty))
                            {
                                listaWizytLekarza.Rows.Add("", wizyta.DataRejestracji.ToShortTimeString(), wizyta.Pacjent.Nazwisko + " " + wizyta.Pacjent.Imie);
                            }
                        }
                        listaWizytLekarza.Sort(listaWizytLekarza.Columns[1], ListSortDirection.Ascending);
                        int numerWizyty = 1;
                        foreach (DataGridViewRow row in listaWizytLekarza.Rows)
                        {
                            row.Cells[0].Value = numerWizyty;
                            numerWizyty++;
                        }
                    });
            }
        }

        private void LadujLekarzy()
        {
            lekarzeZaladowani = false;
            listaLekarzy.Rows.Clear();
            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
               .Wykonaj(x => {
                   int liczbaDzisiejszychWizyt = 0;
                   foreach (var wizyta in x.Wizyty)
                   {
                       if (wizyta.DataRejestracji.Date.Equals(DatePicker.Value.Date))
                           liczbaDzisiejszychWizyt++;
                   }
                   listaLekarzy.Rows.Add(x.Id, x.Imie, x.Nazwisko, liczbaDzisiejszychWizyt);
               });

            lekarzeZaladowani = true;
        }

        private void Odswiez()
        {
            if (listaLekarzy.SelectedRows.Count == 1)
            {
                int idLekarza = (int)listaLekarzy.SelectedRows[0].Cells[0].Value;
                LadujLekarzy();
                foreach (DataGridViewRow row in listaLekarzy.Rows)
                {
                    if ((int)row.Cells[0].Value == idLekarza)
                    {
                        row.Selected = true;
                        break;
                    }
                }
                LadujWizytyLekarza();
            }
        }

        private void WizytaEdycja_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void listaWizytLekarza_SelectionChanged(object sender, EventArgs e)
        {
            listaWizytLekarza.ClearSelection();
        }
    }
}
