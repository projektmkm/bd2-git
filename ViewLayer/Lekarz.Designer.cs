﻿namespace ViewLayer
{
    partial class Lekarz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataRejestracji1 = new System.Windows.Forms.DateTimePicker();
            this.regDateLabel = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.ShowButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.HandleButton = new System.Windows.Forms.Button();
            this.ListaWizyt = new System.Windows.Forms.DataGridView();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.StateComboBox = new System.Windows.Forms.ComboBox();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.dataZakonczenia1 = new System.Windows.Forms.DateTimePicker();
            this.finDateLabel = new System.Windows.Forms.Label();
            this.dataRejestracji2 = new System.Windows.Forms.DateTimePicker();
            this.dataZakonczenia2 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.regDateCheckBox = new System.Windows.Forms.CheckBox();
            this.finDateCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.ListaWizyt)).BeginInit();
            this.SuspendLayout();
            // 
            // dataRejestracji1
            // 
            this.dataRejestracji1.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji1.Enabled = false;
            this.dataRejestracji1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji1.Location = new System.Drawing.Point(239, 28);
            this.dataRejestracji1.Name = "dataRejestracji1";
            this.dataRejestracji1.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji1.TabIndex = 78;
            // 
            // regDateLabel
            // 
            this.regDateLabel.AutoSize = true;
            this.regDateLabel.Enabled = false;
            this.regDateLabel.Location = new System.Drawing.Point(236, 11);
            this.regDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.regDateLabel.Name = "regDateLabel";
            this.regDateLabel.Size = new System.Drawing.Size(80, 13);
            this.regDateLabel.TabIndex = 18;
            this.regDateLabel.Text = "Data rejestracji:";
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(384, 126);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(123, 23);
            this.SearchButton.TabIndex = 19;
            this.SearchButton.Text = "Szukaj";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // ShowButton
            // 
            this.ShowButton.Enabled = false;
            this.ShowButton.Location = new System.Drawing.Point(513, 404);
            this.ShowButton.Name = "ShowButton";
            this.ShowButton.Size = new System.Drawing.Size(123, 23);
            this.ShowButton.TabIndex = 20;
            this.ShowButton.Text = "Pokaż";
            this.ShowButton.UseVisualStyleBackColor = true;
            this.ShowButton.Click += new System.EventHandler(this.ShowButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Enabled = false;
            this.cancelButton.Location = new System.Drawing.Point(384, 404);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(123, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // HandleButton
            // 
            this.HandleButton.Enabled = false;
            this.HandleButton.Location = new System.Drawing.Point(255, 404);
            this.HandleButton.Name = "HandleButton";
            this.HandleButton.Size = new System.Drawing.Size(123, 23);
            this.HandleButton.TabIndex = 22;
            this.HandleButton.Text = "Obsługa";
            this.HandleButton.UseVisualStyleBackColor = true;
            this.HandleButton.Click += new System.EventHandler(this.HandleButton_Click);
            // 
            // ListaWizyt
            // 
            this.ListaWizyt.AllowUserToAddRows = false;
            this.ListaWizyt.AllowUserToDeleteRows = false;
            this.ListaWizyt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListaWizyt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.NameCol,
            this.SurnameCol,
            this.Date1Col,
            this.Date2Col,
            this.StateCol});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ListaWizyt.DefaultCellStyle = dataGridViewCellStyle1;
            this.ListaWizyt.Location = new System.Drawing.Point(12, 155);
            this.ListaWizyt.MultiSelect = false;
            this.ListaWizyt.Name = "ListaWizyt";
            this.ListaWizyt.ReadOnly = true;
            this.ListaWizyt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ListaWizyt.Size = new System.Drawing.Size(624, 243);
            this.ListaWizyt.TabIndex = 23;
            this.ListaWizyt.SelectionChanged += new System.EventHandler(this.VisitsList_SelectionChanged);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "ID";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Visible = false;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Width = 110;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            this.SurnameCol.Width = 115;
            // 
            // Date1Col
            // 
            this.Date1Col.HeaderText = "Data rejestracji";
            this.Date1Col.Name = "Date1Col";
            this.Date1Col.ReadOnly = true;
            this.Date1Col.Width = 120;
            // 
            // Date2Col
            // 
            this.Date2Col.HeaderText = "Data zakończenia";
            this.Date2Col.Name = "Date2Col";
            this.Date2Col.ReadOnly = true;
            this.Date2Col.Width = 120;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Stan wizyty";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            this.StateCol.Width = 115;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(486, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Stan wizyty:";
            // 
            // StateComboBox
            // 
            this.StateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateComboBox.FormattingEnabled = true;
            this.StateComboBox.Location = new System.Drawing.Point(489, 27);
            this.StateComboBox.Name = "StateComboBox";
            this.StateComboBox.Size = new System.Drawing.Size(147, 21);
            this.StateComboBox.TabIndex = 43;
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(13, 404);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(123, 23);
            this.LogoutButton.TabIndex = 44;
            this.LogoutButton.Text = "Wyloguj";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(14, 28);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(170, 20);
            this.NameTextBox.TabIndex = 45;
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(14, 69);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(170, 20);
            this.SurnameTextBox.TabIndex = 46;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Imię:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Nazwisko:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(513, 126);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 79;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // dataZakonczenia1
            // 
            this.dataZakonczenia1.CustomFormat = "yyyy-MM-dd";
            this.dataZakonczenia1.Enabled = false;
            this.dataZakonczenia1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZakonczenia1.Location = new System.Drawing.Point(239, 69);
            this.dataZakonczenia1.Name = "dataZakonczenia1";
            this.dataZakonczenia1.Size = new System.Drawing.Size(84, 20);
            this.dataZakonczenia1.TabIndex = 81;
            // 
            // finDateLabel
            // 
            this.finDateLabel.AutoSize = true;
            this.finDateLabel.Enabled = false;
            this.finDateLabel.Location = new System.Drawing.Point(236, 52);
            this.finDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.finDateLabel.Name = "finDateLabel";
            this.finDateLabel.Size = new System.Drawing.Size(96, 13);
            this.finDateLabel.TabIndex = 80;
            this.finDateLabel.Text = "Data zakończenia:";
            // 
            // dataRejestracji2
            // 
            this.dataRejestracji2.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji2.Enabled = false;
            this.dataRejestracji2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji2.Location = new System.Drawing.Point(340, 28);
            this.dataRejestracji2.Name = "dataRejestracji2";
            this.dataRejestracji2.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji2.TabIndex = 82;
            // 
            // dataZakonczenia2
            // 
            this.dataZakonczenia2.CustomFormat = "yyyy-MM-dd";
            this.dataZakonczenia2.Enabled = false;
            this.dataZakonczenia2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZakonczenia2.Location = new System.Drawing.Point(340, 69);
            this.dataZakonczenia2.Name = "dataZakonczenia2";
            this.dataZakonczenia2.Size = new System.Drawing.Size(84, 20);
            this.dataZakonczenia2.TabIndex = 83;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 113;
            this.label12.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(327, 72);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 114;
            this.label6.Text = "-";
            // 
            // regDateCheckBox
            // 
            this.regDateCheckBox.AutoSize = true;
            this.regDateCheckBox.Location = new System.Drawing.Point(430, 31);
            this.regDateCheckBox.Name = "regDateCheckBox";
            this.regDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.regDateCheckBox.TabIndex = 116;
            this.regDateCheckBox.UseVisualStyleBackColor = true;
            this.regDateCheckBox.CheckedChanged += new System.EventHandler(this.regDateCheckBox_CheckedChanged);
            // 
            // finDateCheckBox
            // 
            this.finDateCheckBox.AutoSize = true;
            this.finDateCheckBox.Location = new System.Drawing.Point(430, 72);
            this.finDateCheckBox.Name = "finDateCheckBox";
            this.finDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.finDateCheckBox.TabIndex = 117;
            this.finDateCheckBox.UseVisualStyleBackColor = true;
            this.finDateCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // Lekarz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 439);
            this.Controls.Add(this.finDateCheckBox);
            this.Controls.Add(this.regDateCheckBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataZakonczenia2);
            this.Controls.Add(this.dataRejestracji2);
            this.Controls.Add(this.dataZakonczenia1);
            this.Controls.Add(this.finDateLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.dataRejestracji1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.StateComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ListaWizyt);
            this.Controls.Add(this.HandleButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.ShowButton);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.regDateLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Lekarz";
            this.ShowIcon = false;
            this.Text = "Lekarz";
            this.Load += new System.EventHandler(this.Lekarz_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ListaWizyt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dataRejestracji1;
        private System.Windows.Forms.Label regDateLabel;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Button ShowButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button HandleButton;
        private System.Windows.Forms.DataGridView ListaWizyt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox StateComboBox;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DateTimePicker dataZakonczenia1;
        private System.Windows.Forms.Label finDateLabel;
        private System.Windows.Forms.DateTimePicker dataRejestracji2;
        private System.Windows.Forms.DateTimePicker dataZakonczenia2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.CheckBox regDateCheckBox;
        private System.Windows.Forms.CheckBox finDateCheckBox;
    }
}

