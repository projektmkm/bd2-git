﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class BadanieLaboratoryjneLaborant : Form
    {
        public BadanieLaboratoryjneLaborant(ApplicationEngine e, int idBadania, int idLaboranta)
        {
            InitializeComponent();
            engine = e;
            this.idBadania = idBadania;
            this.idLaboranta = idLaboranta;
            engine.BadanieLaboratoryjneManager.RozpocznijBadanie(idBadania, this.idLaboranta);
            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
               .IdBadania(this.idBadania)
               .Wykonaj(x => {
                   idTextBox.Text = x.Id.ToString();
                   codeTextBox.Text = x.TypBadania.Kod;
                   nameTextBox.Text = x.TypBadania.Nazwa;
                   stateTextBox.Text = x.Stan.ToString();
                   docNoteTextBox.Text = x.UwagiLekarza;
               });
        }


        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ErrorProvider.Clear();
            if (IsUserInputValid())
            {
                if (MessageBox.Show("Czy jesteś pewien?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.BadanieLaboratoryjneManager.AnulujBadanieLaborant(idBadania, idLaboranta, resultTextBox.Text);
                    this.Close();
                }
            }else
            {
                ErrorProvider.SetError(resultTextBox, "Podaj powód anulowania badania");
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorProvider.Clear();
            if (IsUserInputValid())
            {
                if (MessageBox.Show("Czy jesteś pewien?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.BadanieLaboratoryjneManager.ZakonczBadanie(idBadania, resultTextBox.Text);
                    this.Close();
                }
            }
            else
            {
                ErrorProvider.SetError(resultTextBox, "Uzupełnij wynik badania");
            }
            
        }

        private bool IsUserInputValid()
        {
            return (!string.IsNullOrWhiteSpace(resultTextBox.Text));
        }

        private readonly ApplicationEngine engine;
        private readonly int idBadania, idLaboranta;
    }
}
