﻿namespace ViewLayer
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportButton = new System.Windows.Forms.Button();
            this.labTestButton = new System.Windows.Forms.Button();
            this.phisicTextButton = new System.Windows.Forms.Button();
            this.usersButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // reportButton
            // 
            this.reportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.reportButton.Location = new System.Drawing.Point(42, 165);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(105, 45);
            this.reportButton.TabIndex = 39;
            this.reportButton.Text = "Raport wizyty";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // labTestButton
            // 
            this.labTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labTestButton.Location = new System.Drawing.Point(42, 114);
            this.labTestButton.Name = "labTestButton";
            this.labTestButton.Size = new System.Drawing.Size(105, 45);
            this.labTestButton.TabIndex = 38;
            this.labTestButton.Text = "Uzupełnij badania laboratoryjne";
            this.labTestButton.UseVisualStyleBackColor = true;
            this.labTestButton.Click += new System.EventHandler(this.labTestBt_Click);
            // 
            // phisicTextButton
            // 
            this.phisicTextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.phisicTextButton.Location = new System.Drawing.Point(42, 63);
            this.phisicTextButton.Name = "phisicTextButton";
            this.phisicTextButton.Size = new System.Drawing.Size(105, 45);
            this.phisicTextButton.TabIndex = 37;
            this.phisicTextButton.Text = "Uzupełnij badania fizykalne";
            this.phisicTextButton.UseVisualStyleBackColor = true;
            this.phisicTextButton.Click += new System.EventHandler(this.phisicTestBt_Click);
            // 
            // usersButton
            // 
            this.usersButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usersButton.Location = new System.Drawing.Point(42, 12);
            this.usersButton.Name = "usersButton";
            this.usersButton.Size = new System.Drawing.Size(105, 45);
            this.usersButton.TabIndex = 35;
            this.usersButton.Text = "Zarządzaj użytkownikami";
            this.usersButton.UseVisualStyleBackColor = true;
            this.usersButton.Click += new System.EventHandler(this.usersBt_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(42, 216);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(105, 23);
            this.logoutButton.TabIndex = 40;
            this.logoutButton.Text = "Wyloguj";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutBt_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(189, 247);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.reportButton);
            this.Controls.Add(this.labTestButton);
            this.Controls.Add(this.phisicTextButton);
            this.Controls.Add(this.usersButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Admin";
            this.ShowIcon = false;
            this.Text = "Administrator";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Button labTestButton;
        private System.Windows.Forms.Button phisicTextButton;
        private System.Windows.Forms.Button usersButton;
        private System.Windows.Forms.Button logoutButton;
    }
}