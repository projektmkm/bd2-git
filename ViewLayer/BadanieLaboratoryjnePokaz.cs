﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class BadanieLaboratoryjnePokaz : Form
    {
        public BadanieLaboratoryjnePokaz(ApplicationEngine e, int idBadania)
        {
            InitializeComponent();
            engine = e;

            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
              .IdBadania(idBadania)
              .Wykonaj(x => {
                  idTextBox.Text = x.Id.ToString();
                  codeTextBox.Text = x.TypBadania.Kod;
                  nameTextBox.Text = x.TypBadania.Nazwa;
                  stateTextBox.Text = x.Stan.ToString();
                  labNoteTextBox.Text = x.UwagiKierownika;
                  docNoteTextBox.Text = x.UwagiLekarza;
                  resultTextBox.Text = x.Wynik;
                  switch (x.Stan)
                  {
                      case DataLayer.StanBadania.Zlecone:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = date3TextBox.Text = date4TextBox.Text = "-";
                          break;
                      case DataLayer.StanBadania.Wykonywane:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = date3TextBox.Text = date4TextBox.Text = "-";
                          break;
                      case DataLayer.StanBadania.Wykonane:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = x.DataWykonania.ToString();
                          date3TextBox.Text = date4TextBox.Text = "-";
                          break;
                      case DataLayer.StanBadania.Zatwierdzone:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = x.DataWykonania.ToString();
                          date3TextBox.Text = x.DataZatwierdzenia.ToString();
                          date4TextBox.Text = "-";
                          break;
                      case DataLayer.StanBadania.AnulowaneLaborant:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = date3TextBox.Text = "-";
                          date4TextBox.Text = x.DataAnulowania.ToString();
                          break;
                      case DataLayer.StanBadania.AnulowaneKierownik:
                          date1TextBox.Text = x.DataZlecenia.ToString();
                          date2TextBox.Text = x.DataWykonania.ToString();
                          date3TextBox.Text = "-";
                          date4TextBox.Text = x.DataAnulowania.ToString();
                          break;
                  }
              });   
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private readonly ApplicationEngine engine;
    }
}
