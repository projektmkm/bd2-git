﻿using BusinessLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class LekarzObsługa : Form
    {
        public LekarzObsługa(ApplicationEngine e, int idWizyty, Tryb tryb)
        {
            engine = e;
            this.idWizyty = idWizyty;
            InitializeComponent();

            if(tryb == Tryb.ObsluzWizyte){
                phisicTestButton.Enabled = labTestButton.Enabled = visitHandleButtons.Visible = true;
                descriptionTextBox.ReadOnly = diagnosisTextBox.ReadOnly = false;
                engine.WizytaManager.RozpocznijWizyte(idWizyty);
            }
            else
            {
                phisicTestButton.Enabled = labTestButton.Enabled = visitHandleButtons.Visible = false;
                descriptionTextBox.ReadOnly = diagnosisTextBox.ReadOnly = true;
                showVisitsButton.Enabled = (tryb == Tryb.PokarzWizytePacjenta) ? false : true;
            }

            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                   .IdWizyty(idWizyty)
                   .Wykonaj(x =>
                   {
                       this.idPacjenta = x.Pacjent.Id;
                       nameTextBox.Text = x.Pacjent.Imie;
                       surnameTextBox.Text = x.Pacjent.Nazwisko;
                       peselTextBox.Text = x.Pacjent.Pesel;
                       addressTextBox.Text = x.Pacjent.Adres;
                       descriptionTextBox.Text = x.Opis;
                       diagnosisTextBox.Text = x.Diagnoza;
                       stateTextBox.Text = x.Stan.ToString();
                       regDateTextBox.Text = x.DataRejestracji.ToString();
                       if (x.DataZakonczenia != null)
                           finDateTextBox.Text = x.DataZakonczenia.ToString();
                       else
                           finDateLabel.Visible = finDateTextBox.Visible = false;
                   });
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Czy jesteś pewnien?","Potwierdzenie anulowania", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                engine.WizytaManager.AnulujWizyte(idWizyty, descriptionTextBox.Text);
                this.Close();
            }
            
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                if (MessageBox.Show("Czy jesteś pewnien?", "Potwierdzenie zakończenia", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.WizytaManager.ZakonczWizyte(idWizyty, descriptionTextBox.Text, diagnosisTextBox.Text);
                    this.Close();
                }
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
           
        }

        private void showVisitsButton_Click(object sender, EventArgs e)
        {
            using (var form = new LekarzWizytyLista(engine, idPacjenta, idWizyty))
                form.ShowDialog();
        }

        private void showTestsButton_Click(object sender, EventArgs e)
        {
            using (var form = new LekarzBadaniaLista(engine, idWizyty))
                form.ShowDialog();
        }

        private void labTestButton_Click(object sender, EventArgs e)
        {
            using (var form = new BadanieLaboratoryjneLekarz(engine, idWizyty))
                form.ShowDialog();
        }

        private void phisicTestButton_Click(object sender, EventArgs e)
        {
            using (var form = new BadanieFizykalne(engine, idWizyty, BadanieFizykalne.Tryb.Nowe))
                form.ShowDialog();
        }

        private bool IsUserInputValid()
        {
            return (!string.IsNullOrWhiteSpace(diagnosisTextBox.Text)) && (!string.IsNullOrWhiteSpace(descriptionTextBox.Text));
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (string.IsNullOrWhiteSpace(diagnosisTextBox.Text))
                ErrorProvider.SetError(diagnosisTextBox, "Uzupełnij diagnozę");
            if (string.IsNullOrWhiteSpace(descriptionTextBox.Text))
                ErrorProvider.SetError(descriptionTextBox, "Uzupełnij opis");
        }

        private readonly ApplicationEngine engine;
        private readonly int idWizyty;
        private int idPacjenta;
        public enum Tryb : byte { PokazWizyteLekarza, PokarzWizytePacjenta, ObsluzWizyte };
    }
}
