﻿namespace ViewLayer
{
    partial class AdminBadaniaLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backBt = new System.Windows.Forms.Button();
            this.modifyBt = new System.Windows.Forms.Button();
            this.deleteBt = new System.Windows.Forms.Button();
            this.listaBadan = new System.Windows.Forms.DataGridView();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addBt = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).BeginInit();
            this.SuspendLayout();
            // 
            // backBt
            // 
            this.backBt.Location = new System.Drawing.Point(400, 281);
            this.backBt.Name = "backBt";
            this.backBt.Size = new System.Drawing.Size(100, 23);
            this.backBt.TabIndex = 58;
            this.backBt.Text = "Powrót";
            this.backBt.UseVisualStyleBackColor = true;
            this.backBt.Click += new System.EventHandler(this.backBt_Click);
            // 
            // modifyBt
            // 
            this.modifyBt.Enabled = false;
            this.modifyBt.Location = new System.Drawing.Point(188, 281);
            this.modifyBt.Name = "modifyBt";
            this.modifyBt.Size = new System.Drawing.Size(100, 23);
            this.modifyBt.TabIndex = 57;
            this.modifyBt.Text = "Edytuj";
            this.modifyBt.UseVisualStyleBackColor = true;
            this.modifyBt.Click += new System.EventHandler(this.modifyBt_Click);
            // 
            // deleteBt
            // 
            this.deleteBt.Enabled = false;
            this.deleteBt.Location = new System.Drawing.Point(294, 281);
            this.deleteBt.Name = "deleteBt";
            this.deleteBt.Size = new System.Drawing.Size(100, 23);
            this.deleteBt.TabIndex = 56;
            this.deleteBt.Text = "Usuń";
            this.deleteBt.UseVisualStyleBackColor = true;
            this.deleteBt.Click += new System.EventHandler(this.deleteBt_Click);
            // 
            // listaBadan
            // 
            this.listaBadan.AllowUserToAddRows = false;
            this.listaBadan.AllowUserToDeleteRows = false;
            this.listaBadan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaBadan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.CodeCol,
            this.NameCol,
            this.DescriptionCol});
            this.listaBadan.Location = new System.Drawing.Point(12, 17);
            this.listaBadan.MultiSelect = false;
            this.listaBadan.Name = "listaBadan";
            this.listaBadan.ReadOnly = true;
            this.listaBadan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaBadan.Size = new System.Drawing.Size(488, 258);
            this.listaBadan.TabIndex = 55;
            this.listaBadan.SelectionChanged += new System.EventHandler(this.listaBadan_SelectionChanged);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "ID";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Visible = false;
            // 
            // CodeCol
            // 
            this.CodeCol.HeaderText = "Kod";
            this.CodeCol.Name = "CodeCol";
            this.CodeCol.ReadOnly = true;
            this.CodeCol.Width = 80;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Nazwa";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            // 
            // DescriptionCol
            // 
            this.DescriptionCol.HeaderText = "Opis";
            this.DescriptionCol.Name = "DescriptionCol";
            this.DescriptionCol.ReadOnly = true;
            this.DescriptionCol.Width = 265;
            // 
            // addBt
            // 
            this.addBt.Location = new System.Drawing.Point(82, 281);
            this.addBt.Name = "addBt";
            this.addBt.Size = new System.Drawing.Size(100, 23);
            this.addBt.TabIndex = 59;
            this.addBt.Text = "Dodaj";
            this.addBt.UseVisualStyleBackColor = true;
            this.addBt.Click += new System.EventHandler(this.addBt_Click);
            // 
            // AdminBadaniaLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 316);
            this.Controls.Add(this.addBt);
            this.Controls.Add(this.backBt);
            this.Controls.Add(this.modifyBt);
            this.Controls.Add(this.deleteBt);
            this.Controls.Add(this.listaBadan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AdminBadaniaLista";
            this.ShowIcon = false;
            this.Text = "Słownik badań";
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button backBt;
        private System.Windows.Forms.Button modifyBt;
        private System.Windows.Forms.Button deleteBt;
        private System.Windows.Forms.DataGridView listaBadan;
        private System.Windows.Forms.Button addBt;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionCol;
    }
}