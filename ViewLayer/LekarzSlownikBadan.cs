﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class LekarzSlownikBadan : Form
    {
        public enum Tryb : byte
        {
            Lab,
            Fiz
        }
        public LekarzSlownikBadan(ApplicationEngine e, Tryb tryb, Action<int> s)
        {
            InitializeComponent();
            this.engine = e;
            this.ustawIdTypuBadania = s;

            if (tryb == Tryb.Fiz)
            {
                this.Text = "Słownik badań fizykalnych";
                new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .TypBadania(DataLayer.BadanieTyp.Fizyczne)
                .Wykonaj(x => {
                    slownikBadanList.Rows.Add(x.Id, x.Kod, x.Nazwa, x.Opis);
                });
                slownikBadanList.Sort(slownikBadanList.Columns[1], ListSortDirection.Ascending);
            }
            else
            {
                this.Text = "Słownik badań laboratoryjnych";
                new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .TypBadania(DataLayer.BadanieTyp.Laboratoryjne)
                .Wykonaj(x => {
                    slownikBadanList.Rows.Add(x.Id, x.Kod, x.Nazwa, x.Opis);
                });
                slownikBadanList.Sort(slownikBadanList.Columns[1], ListSortDirection.Ascending);
            }
        }

        private void backBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void acceptBt_Click(object sender, EventArgs e)
        {
            int id = (int)slownikBadanList.SelectedRows[0].Cells[0].Value;
            ustawIdTypuBadania(id);
            this.Close();
        }

        private void slownikBadanList_SelectionChanged(object sender, EventArgs e)
        {
            acceptBt.Enabled = (slownikBadanList.SelectedRows.Count == 1) ? true : false;
        }

        private readonly ApplicationEngine engine;
        private Action<int> ustawIdTypuBadania;
    }
}
