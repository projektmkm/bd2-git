﻿namespace ViewLayer
{
    partial class Pacjenci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.PeselTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            this.ReturnButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.PatientsList = new System.Windows.Forms.DataGridView();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeselCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddButton = new System.Windows.Forms.Button();
            this.patientsManagerButtons = new System.Windows.Forms.Panel();
            this.visitConfigButtons = new System.Windows.Forms.Panel();
            this.nextButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PatientsList)).BeginInit();
            this.patientsManagerButtons.SuspendLayout();
            this.visitConfigButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(12, 33);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(170, 20);
            this.NameTextBox.TabIndex = 0;
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(12, 74);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(170, 20);
            this.SurnameTextBox.TabIndex = 1;
            // 
            // PeselTextBox
            // 
            this.PeselTextBox.Location = new System.Drawing.Point(236, 33);
            this.PeselTextBox.Name = "PeselTextBox";
            this.PeselTextBox.Size = new System.Drawing.Size(170, 20);
            this.PeselTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Imię:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nazwisko:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(233, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Pesel:";
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(324, 101);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(123, 23);
            this.SearchButton.TabIndex = 6;
            this.SearchButton.Text = "Szukaj";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // ReturnButton
            // 
            this.ReturnButton.Location = new System.Drawing.Point(449, 365);
            this.ReturnButton.Name = "ReturnButton";
            this.ReturnButton.Size = new System.Drawing.Size(123, 23);
            this.ReturnButton.TabIndex = 7;
            this.ReturnButton.Text = "Powrót";
            this.ReturnButton.UseVisualStyleBackColor = true;
            this.ReturnButton.Click += new System.EventHandler(this.ReturnButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Location = new System.Drawing.Point(131, 3);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(123, 23);
            this.EditButton.TabIndex = 8;
            this.EditButton.Text = "Edytuj";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // PatientsList
            // 
            this.PatientsList.AllowUserToAddRows = false;
            this.PatientsList.AllowUserToDeleteRows = false;
            this.PatientsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PatientsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.NameCol,
            this.SurnameCol,
            this.addressCol,
            this.PeselCol});
            this.PatientsList.Location = new System.Drawing.Point(12, 130);
            this.PatientsList.MultiSelect = false;
            this.PatientsList.Name = "PatientsList";
            this.PatientsList.ReadOnly = true;
            this.PatientsList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PatientsList.Size = new System.Drawing.Size(560, 229);
            this.PatientsList.TabIndex = 10;
            this.PatientsList.SelectionChanged += new System.EventHandler(this.PatientsList_SelectionChanged);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "ID";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Visible = false;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Width = 120;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            this.SurnameCol.Width = 120;
            // 
            // addressCol
            // 
            this.addressCol.HeaderText = "Adres";
            this.addressCol.Name = "addressCol";
            this.addressCol.ReadOnly = true;
            this.addressCol.Width = 155;
            // 
            // PeselCol
            // 
            this.PeselCol.HeaderText = "Pesel";
            this.PeselCol.Name = "PeselCol";
            this.PeselCol.ReadOnly = true;
            this.PeselCol.Width = 120;
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(2, 3);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(123, 23);
            this.AddButton.TabIndex = 11;
            this.AddButton.Text = "Nowy";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // patientsManagerButtons
            // 
            this.patientsManagerButtons.Controls.Add(this.EditButton);
            this.patientsManagerButtons.Controls.Add(this.AddButton);
            this.patientsManagerButtons.Location = new System.Drawing.Point(191, 362);
            this.patientsManagerButtons.Name = "patientsManagerButtons";
            this.patientsManagerButtons.Size = new System.Drawing.Size(256, 30);
            this.patientsManagerButtons.TabIndex = 12;
            // 
            // visitConfigButtons
            // 
            this.visitConfigButtons.Controls.Add(this.nextButton);
            this.visitConfigButtons.Location = new System.Drawing.Point(191, 362);
            this.visitConfigButtons.Name = "visitConfigButtons";
            this.visitConfigButtons.Size = new System.Drawing.Size(256, 30);
            this.visitConfigButtons.TabIndex = 13;
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(131, 3);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(123, 23);
            this.nextButton.TabIndex = 8;
            this.nextButton.Text = "Dalej";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(449, 101);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 14;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.ErrorProvider.ContainerControl = this;
            // 
            // Pacjenci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 395);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.visitConfigButtons);
            this.Controls.Add(this.patientsManagerButtons);
            this.Controls.Add(this.PatientsList);
            this.Controls.Add(this.ReturnButton);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PeselTextBox);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.NameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Pacjenci";
            this.ShowIcon = false;
            this.Text = "Pacjenci";
            this.Load += new System.EventHandler(this.Pacjenci_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PatientsList)).EndInit();
            this.patientsManagerButtons.ResumeLayout(false);
            this.visitConfigButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.TextBox PeselTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Button ReturnButton;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.DataGridView PatientsList;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Panel patientsManagerButtons;
        private System.Windows.Forms.Panel visitConfigButtons;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeselCol;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.ErrorProvider ErrorProvider;
    }
}

