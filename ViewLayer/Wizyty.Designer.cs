﻿namespace Forms
{
    partial class Wizyty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataRejestracji1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listaWizyt = new System.Windows.Forms.DataGridView();
            this.IDCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoctorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.ReturnButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.regDateLabel = new System.Windows.Forms.Label();
            this.DoctorComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.dataRejestracji2 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.regDateCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.listaWizyt)).BeginInit();
            this.SuspendLayout();
            // 
            // dataRejestracji1
            // 
            this.dataRejestracji1.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji1.Enabled = false;
            this.dataRejestracji1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji1.Location = new System.Drawing.Point(435, 28);
            this.dataRejestracji1.Name = "dataRejestracji1";
            this.dataRejestracji1.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji1.TabIndex = 92;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 91;
            this.label2.Text = "Nazwisko:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "Imię:";
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(14, 69);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(170, 20);
            this.SurnameTextBox.TabIndex = 89;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(14, 28);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(170, 20);
            this.NameTextBox.TabIndex = 88;
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(305, 353);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(123, 23);
            this.AddButton.TabIndex = 87;
            this.AddButton.Text = "Dodaj";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // stateComboBox
            // 
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(216, 27);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(143, 21);
            this.stateComboBox.TabIndex = 86;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 85;
            this.label4.Text = "Stan wizyty:";
            // 
            // listaWizyt
            // 
            this.listaWizyt.AllowUserToAddRows = false;
            this.listaWizyt.AllowUserToDeleteRows = false;
            this.listaWizyt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaWizyt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDCol,
            this.NameCol,
            this.SurnameCol,
            this.StateCol,
            this.DateCol,
            this.DoctorColumn});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listaWizyt.DefaultCellStyle = dataGridViewCellStyle1;
            this.listaWizyt.Location = new System.Drawing.Point(13, 133);
            this.listaWizyt.Name = "listaWizyt";
            this.listaWizyt.ReadOnly = true;
            this.listaWizyt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaWizyt.Size = new System.Drawing.Size(674, 214);
            this.listaWizyt.TabIndex = 84;
            this.listaWizyt.SelectionChanged += new System.EventHandler(this.VisitsList_SelectionChanged);
            // 
            // IDCol
            // 
            this.IDCol.HeaderText = "Id";
            this.IDCol.Name = "IDCol";
            this.IDCol.ReadOnly = true;
            this.IDCol.Visible = false;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.Width = 120;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            this.SurnameCol.Width = 120;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Stan";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            this.StateCol.Width = 120;
            // 
            // DateCol
            // 
            this.DateCol.HeaderText = "Data rejestracji";
            this.DateCol.Name = "DateCol";
            this.DateCol.ReadOnly = true;
            this.DateCol.Width = 120;
            // 
            // DoctorColumn
            // 
            this.DoctorColumn.HeaderText = "Lekarz";
            this.DoctorColumn.Name = "DoctorColumn";
            this.DoctorColumn.ReadOnly = true;
            this.DoctorColumn.Width = 150;
            // 
            // EditButton
            // 
            this.EditButton.Enabled = false;
            this.EditButton.Location = new System.Drawing.Point(435, 353);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(123, 23);
            this.EditButton.TabIndex = 83;
            this.EditButton.Text = "Edytuj";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Enabled = false;
            this.cancelButton.Location = new System.Drawing.Point(176, 353);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(123, 23);
            this.cancelButton.TabIndex = 82;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ReturnButton
            // 
            this.ReturnButton.Location = new System.Drawing.Point(564, 353);
            this.ReturnButton.Name = "ReturnButton";
            this.ReturnButton.Size = new System.Drawing.Size(123, 23);
            this.ReturnButton.TabIndex = 81;
            this.ReturnButton.Text = "Powrót";
            this.ReturnButton.UseVisualStyleBackColor = true;
            this.ReturnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(435, 104);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(123, 23);
            this.SearchButton.TabIndex = 80;
            this.SearchButton.Text = "Szukaj";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.searchBt_Click);
            // 
            // regDateLabel
            // 
            this.regDateLabel.AutoSize = true;
            this.regDateLabel.Enabled = false;
            this.regDateLabel.Location = new System.Drawing.Point(432, 11);
            this.regDateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.regDateLabel.Name = "regDateLabel";
            this.regDateLabel.Size = new System.Drawing.Size(80, 13);
            this.regDateLabel.TabIndex = 79;
            this.regDateLabel.Text = "Data rejestracji:";
            // 
            // DoctorComboBox
            // 
            this.DoctorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DoctorComboBox.FormattingEnabled = true;
            this.DoctorComboBox.Location = new System.Drawing.Point(216, 68);
            this.DoctorComboBox.Name = "DoctorComboBox";
            this.DoctorComboBox.Size = new System.Drawing.Size(189, 21);
            this.DoctorComboBox.TabIndex = 93;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(213, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 94;
            this.label5.Text = "Lekarz:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(564, 104);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 95;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // dataRejestracji2
            // 
            this.dataRejestracji2.CustomFormat = "yyyy-MM-dd";
            this.dataRejestracji2.Enabled = false;
            this.dataRejestracji2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataRejestracji2.Location = new System.Drawing.Point(540, 28);
            this.dataRejestracji2.Name = "dataRejestracji2";
            this.dataRejestracji2.Size = new System.Drawing.Size(84, 20);
            this.dataRejestracji2.TabIndex = 96;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(525, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 114;
            this.label12.Text = "-";
            // 
            // regDateCheckBox
            // 
            this.regDateCheckBox.AutoSize = true;
            this.regDateCheckBox.Location = new System.Drawing.Point(630, 31);
            this.regDateCheckBox.Name = "regDateCheckBox";
            this.regDateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.regDateCheckBox.TabIndex = 115;
            this.regDateCheckBox.UseVisualStyleBackColor = true;
            this.regDateCheckBox.CheckedChanged += new System.EventHandler(this.regDateCheckBox_CheckedChanged);
            // 
            // Wizyty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 388);
            this.Controls.Add(this.regDateCheckBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataRejestracji2);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DoctorComboBox);
            this.Controls.Add(this.dataRejestracji1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listaWizyt);
            this.Controls.Add(this.EditButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.ReturnButton);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.regDateLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Wizyty";
            this.ShowIcon = false;
            this.Text = "Wizyty";
            this.Load += new System.EventHandler(this.Wizyty_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaWizyt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dataRejestracji1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView listaWizyt;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button ReturnButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label regDateLabel;
        private System.Windows.Forms.ComboBox DoctorComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DateTimePicker dataRejestracji2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DoctorColumn;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.CheckBox regDateCheckBox;
    }
}