﻿namespace ViewLayer
{
    partial class Laborant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataZlecenia1 = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.examIDTextBox = new System.Windows.Forms.TextBox();
            this.listaBadan = new System.Windows.Forms.DataGridView();
            this.IdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoctorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.performBt = new System.Windows.Forms.Button();
            this.showBt = new System.Windows.Forms.Button();
            this.searchBt = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.doctorComboBox = new System.Windows.Forms.ComboBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.dataZlecenia2 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.dateCheckBox = new System.Windows.Forms.CheckBox();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // dataZlecenia1
            // 
            this.dataZlecenia1.CustomFormat = "yyyy-MM-dd";
            this.dataZlecenia1.Enabled = false;
            this.dataZlecenia1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZlecenia1.Location = new System.Drawing.Point(562, 27);
            this.dataZlecenia1.Name = "dataZlecenia1";
            this.dataZlecenia1.Size = new System.Drawing.Size(84, 20);
            this.dataZlecenia1.TabIndex = 70;
            this.dataZlecenia1.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Enabled = false;
            this.dateLabel.Location = new System.Drawing.Point(559, 9);
            this.dateLabel.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(75, 13);
            this.dateLabel.TabIndex = 69;
            this.dateLabel.Text = "Data zlecenia:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(235, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "ID badania:";
            // 
            // examIDTextBox
            // 
            this.examIDTextBox.Location = new System.Drawing.Point(238, 68);
            this.examIDTextBox.Name = "examIDTextBox";
            this.examIDTextBox.Size = new System.Drawing.Size(124, 20);
            this.examIDTextBox.TabIndex = 66;
            // 
            // listaBadan
            // 
            this.listaBadan.AllowUserToAddRows = false;
            this.listaBadan.AllowUserToDeleteRows = false;
            this.listaBadan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaBadan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCol,
            this.CodeCol,
            this.DateCol1,
            this.NameCol,
            this.SurnameCol,
            this.DoctorCol,
            this.StateCol});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listaBadan.DefaultCellStyle = dataGridViewCellStyle1;
            this.listaBadan.Location = new System.Drawing.Point(15, 139);
            this.listaBadan.MultiSelect = false;
            this.listaBadan.Name = "listaBadan";
            this.listaBadan.ReadOnly = true;
            this.listaBadan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaBadan.Size = new System.Drawing.Size(800, 302);
            this.listaBadan.TabIndex = 64;
            this.listaBadan.SelectionChanged += new System.EventHandler(this.badanieList_SelectionChanged);
            // 
            // IdCol
            // 
            this.IdCol.HeaderText = "Id";
            this.IdCol.Name = "IdCol";
            this.IdCol.ReadOnly = true;
            this.IdCol.Width = 50;
            // 
            // CodeCol
            // 
            this.CodeCol.HeaderText = "Kod";
            this.CodeCol.Name = "CodeCol";
            this.CodeCol.ReadOnly = true;
            this.CodeCol.Width = 80;
            // 
            // DateCol1
            // 
            this.DateCol1.HeaderText = "Data zlecenia";
            this.DateCol1.Name = "DateCol1";
            this.DateCol1.ReadOnly = true;
            this.DateCol1.Width = 110;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            this.SurnameCol.Width = 110;
            // 
            // DoctorCol
            // 
            this.DoctorCol.HeaderText = "Lekarz";
            this.DoctorCol.Name = "DoctorCol";
            this.DoctorCol.ReadOnly = true;
            this.DoctorCol.Width = 205;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Stan";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            // 
            // performBt
            // 
            this.performBt.Enabled = false;
            this.performBt.Location = new System.Drawing.Point(563, 447);
            this.performBt.Name = "performBt";
            this.performBt.Size = new System.Drawing.Size(123, 23);
            this.performBt.TabIndex = 63;
            this.performBt.Text = "Wykonaj";
            this.performBt.UseVisualStyleBackColor = true;
            this.performBt.Click += new System.EventHandler(this.performBt_Click);
            // 
            // showBt
            // 
            this.showBt.Enabled = false;
            this.showBt.Location = new System.Drawing.Point(692, 447);
            this.showBt.Name = "showBt";
            this.showBt.Size = new System.Drawing.Size(123, 23);
            this.showBt.TabIndex = 62;
            this.showBt.Text = "Pokaż";
            this.showBt.UseVisualStyleBackColor = true;
            this.showBt.Click += new System.EventHandler(this.showBt_Click);
            // 
            // searchBt
            // 
            this.searchBt.Location = new System.Drawing.Point(563, 110);
            this.searchBt.Name = "searchBt";
            this.searchBt.Size = new System.Drawing.Size(123, 23);
            this.searchBt.TabIndex = 61;
            this.searchBt.Text = "Szukaj";
            this.searchBt.UseVisualStyleBackColor = true;
            this.searchBt.Click += new System.EventHandler(this.searchBt_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(15, 447);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(123, 23);
            this.logoutButton.TabIndex = 72;
            this.logoutButton.Text = "Wyloguj";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 78;
            this.label2.Text = "Nazwisko pacjenta:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 10);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 77;
            this.label7.Text = "Imię pacjenta:";
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(15, 68);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(176, 20);
            this.SurnameTextBox.TabIndex = 76;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(15, 27);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(176, 20);
            this.NameTextBox.TabIndex = 75;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "Lekarz:";
            // 
            // doctorComboBox
            // 
            this.doctorComboBox.FormattingEnabled = true;
            this.doctorComboBox.Location = new System.Drawing.Point(238, 26);
            this.doctorComboBox.Name = "doctorComboBox";
            this.doctorComboBox.Size = new System.Drawing.Size(252, 21);
            this.doctorComboBox.TabIndex = 68;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(692, 110);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(123, 23);
            this.clearButton.TabIndex = 79;
            this.clearButton.Text = "Wyczyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // dataZlecenia2
            // 
            this.dataZlecenia2.CustomFormat = "yyyy-MM-dd";
            this.dataZlecenia2.Enabled = false;
            this.dataZlecenia2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZlecenia2.Location = new System.Drawing.Point(659, 27);
            this.dataZlecenia2.Name = "dataZlecenia2";
            this.dataZlecenia2.Size = new System.Drawing.Size(84, 20);
            this.dataZlecenia2.TabIndex = 80;
            this.dataZlecenia2.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(648, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 112;
            this.label12.Text = "-";
            // 
            // dateCheckBox
            // 
            this.dateCheckBox.AutoSize = true;
            this.dateCheckBox.Location = new System.Drawing.Point(749, 30);
            this.dateCheckBox.Name = "dateCheckBox";
            this.dateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.dateCheckBox.TabIndex = 118;
            this.dateCheckBox.UseVisualStyleBackColor = true;
            this.dateCheckBox.CheckedChanged += new System.EventHandler(this.dateCheckBox_CheckedChanged);
            // 
            // stateComboBox
            // 
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(562, 68);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(124, 21);
            this.stateComboBox.TabIndex = 120;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(559, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 119;
            this.label1.Text = "Stan:";
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.ErrorProvider.ContainerControl = this;
            // 
            // Laborant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 482);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateCheckBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dataZlecenia2);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.dataZlecenia1);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.doctorComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.examIDTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listaBadan);
            this.Controls.Add(this.performBt);
            this.Controls.Add(this.showBt);
            this.Controls.Add(this.searchBt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Laborant";
            this.ShowIcon = false;
            this.Text = "Laborant";
            this.Load += new System.EventHandler(this.Laborant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dataZlecenia1;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox examIDTextBox;
        private System.Windows.Forms.DataGridView listaBadan;
        private System.Windows.Forms.Button performBt;
        private System.Windows.Forms.Button showBt;
        private System.Windows.Forms.Button searchBt;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox doctorComboBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DateTimePicker dataZlecenia2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DoctorCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.CheckBox dateCheckBox;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider ErrorProvider;
    }
}