﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class BadanieFizykalne : Form
    {
        /// <summary>
        /// </summary>
        /// <param name="e"></param>
        /// <param name="id">Jezeli uruchamiane w trybie "Pokaz", id dotyczy badania fizykalnego.W trybie "Nowe" id dotyczy wizyty, podczas ktorej badanie jest wykonywane.</param>
        /// <param name="m"></param>
        public BadanieFizykalne(ApplicationEngine e, int id, Tryb m)
        {
            InitializeComponent();
            this.engine = e;
            if (m == Tryb.Pokaz)
            {
                this.acceptBt.Visible = this.dictionaryButton.Visible = false;
                this.resultsTextBox.ReadOnly = true;

                new ManagerBadanFizycznych.PoszukiwaczBadanFizycznych(engine.BadanieFizyczneManager)
                    .IdBadania(id)
                    .Wykonaj(x => {
                        codeTextBox.Text = x.TypBadania.Kod;
                        nameTextBox.Text = x.TypBadania.Nazwa;
                        resultsTextBox.Text = x.Wynik;
                    });                
            }
            else
            {
                this.idWizyty = id;
            }
        }

        private void cancelBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void acceptBt_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid() && (idTypuBadania != null))
            {
                ErrorProvider.Clear();
                if (MessageBox.Show("Czy jesteś pewien?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    engine.WizytaManager.DodajBadanieFizykalne(idWizyty, idTypuBadania.Value, resultsTextBox.Text);
                    this.Close();
                }
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
            
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (string.IsNullOrWhiteSpace(resultsTextBox.Text))
                ErrorProvider.SetError(resultsTextBox, "Uzupełnij wynik badania");
            if(idTypuBadania == null)
                ErrorProvider.SetError(dictionaryButton, "Uzupełnij typ badania");
        }

        private bool IsUserInputValid()
        {
            return (!string.IsNullOrWhiteSpace(resultsTextBox.Text));
        }

        private void dictionaryButton_Click(object sender, EventArgs e)
        {
            using (var form = new LekarzSlownikBadan(engine, LekarzSlownikBadan.Tryb.Fiz, x => idTypuBadania = x))
                form.ShowDialog();
            if(idTypuBadania != null)
            {
                new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .IdBadania(idTypuBadania.Value)
                .Wykonaj(x => {
                    codeTextBox.Text = x.Kod;
                    nameTextBox.Text = x.Nazwa;
                });
            }
        }
        public enum Tryb : byte { Nowe, Pokaz };
        private readonly ApplicationEngine engine;
        private int idWizyty;
        private int? idTypuBadania = null;
    }
}
