﻿using BusinessLayer;
using DataLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class AdminBadaniaLista : Form
    {
        private readonly BadanieTyp typBadan;
        private readonly ApplicationEngine engine;
        public AdminBadaniaLista(ApplicationEngine e, BadanieTyp typ)
        {
            InitializeComponent();
            engine = e;
            typBadan = typ;
            if (typBadan == BadanieTyp.Fizyczne)
                this.Text = "Słownik badań fizykalnych";
            else
                this.Text = "Słownik badań laboratoryjnych";
            LadujBadania();
        }

        private void LadujBadania()
        {
            listaBadan.Rows.Clear();
            new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .TypBadania(typBadan)
                .Wykonaj(x => {
                    listaBadan.Rows.Add(x.Id, x.Kod, x.Nazwa, x.Opis);
                });
            listaBadan.Sort(listaBadan.Columns[1], ListSortDirection.Ascending);
        }

        private void backBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void deleteBt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć ten element?", "Uwaga!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
                if (engine.SlownikBadanManager.MoznaUsunac(idBadania))
                    engine.SlownikBadanManager.UsunTypBadania(idBadania);
                else
                    MessageBox.Show("Nie mozna usunąć tego badania!");
            }
            LadujBadania();
        }

        private void modifyBt_Click(object sender, EventArgs e)
        {
            var idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            using (var form = new AdminBadanie(engine, typBadan, idBadania))
            {
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void addBt_Click(object sender, EventArgs e)
        {
            using (var form = new AdminBadanie(engine, typBadan))
            {
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void listaBadan_SelectionChanged(object sender, EventArgs e)
        {
            modifyBt.Enabled = deleteBt.Enabled = (listaBadan.SelectedRows.Count == 1) ? true : false;
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            if (listaBadan.SelectedRows.Count > 0)
            {
                var idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
                LadujBadania();
                foreach (DataGridViewRow row in listaBadan.Rows)
                {
                    if ((int)row.Cells[0].Value == idBadania)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                LadujBadania();
            }
        }
    }
}
