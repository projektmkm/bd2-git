﻿namespace ViewLayer
{
    partial class KierownikLab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.dataZatwierdzeniaLabel = new System.Windows.Forms.Label();
            this.dataZatwierdzenia1 = new System.Windows.Forms.DateTimePicker();
            this.logoutButton = new System.Windows.Forms.Button();
            this.dataWykonania1 = new System.Windows.Forms.DateTimePicker();
            this.dataZlecenia1 = new System.Windows.Forms.DateTimePicker();
            this.dataZleceniaLabel = new System.Windows.Forms.Label();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.examIDTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listaBadan = new System.Windows.Forms.DataGridView();
            this.IdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurnameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCol4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LaboratnCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LekarzCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showBt = new System.Windows.Forms.Button();
            this.searchBt = new System.Windows.Forms.Button();
            this.dataWykonaniaLabel = new System.Windows.Forms.Label();
            this.dataAnulowania1 = new System.Windows.Forms.DateTimePicker();
            this.dataAnulowaniaLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.laborantComboBox = new System.Windows.Forms.ComboBox();
            this.clear = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.LekarzComboBox = new System.Windows.Forms.ComboBox();
            this.dataZlecenia2 = new System.Windows.Forms.DateTimePicker();
            this.dataWykonania2 = new System.Windows.Forms.DateTimePicker();
            this.dataZatwierdzenia2 = new System.Windows.Forms.DateTimePicker();
            this.dataAnulowania2 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.autorefresh = new System.Windows.Forms.Timer(this.components);
            this.dataZleceniaCheckBox = new System.Windows.Forms.CheckBox();
            this.dataWykonaniaCheckBox = new System.Windows.Forms.CheckBox();
            this.dataZatwierdzeniaCheckBox = new System.Windows.Forms.CheckBox();
            this.dataAnulowaniaCheckBox = new System.Windows.Forms.CheckBox();
            this.performButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 97;
            this.label2.Text = "Nazwisko pacjenta:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 96;
            this.label7.Text = "Imię pacjenta:";
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(15, 70);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(176, 20);
            this.SurnameTextBox.TabIndex = 95;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(15, 27);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(176, 20);
            this.NameTextBox.TabIndex = 94;
            // 
            // dataZatwierdzeniaLabel
            // 
            this.dataZatwierdzeniaLabel.AutoSize = true;
            this.dataZatwierdzeniaLabel.Enabled = false;
            this.dataZatwierdzeniaLabel.Location = new System.Drawing.Point(644, 10);
            this.dataZatwierdzeniaLabel.Name = "dataZatwierdzeniaLabel";
            this.dataZatwierdzeniaLabel.Size = new System.Drawing.Size(100, 13);
            this.dataZatwierdzeniaLabel.TabIndex = 93;
            this.dataZatwierdzeniaLabel.Text = "Data zatwierdzenia:";
            // 
            // dataZatwierdzenia1
            // 
            this.dataZatwierdzenia1.CustomFormat = "yyyy-MM-dd";
            this.dataZatwierdzenia1.Enabled = false;
            this.dataZatwierdzenia1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZatwierdzenia1.Location = new System.Drawing.Point(647, 27);
            this.dataZatwierdzenia1.Name = "dataZatwierdzenia1";
            this.dataZatwierdzenia1.Size = new System.Drawing.Size(84, 20);
            this.dataZatwierdzenia1.TabIndex = 92;
            this.dataZatwierdzenia1.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(15, 574);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(123, 23);
            this.logoutButton.TabIndex = 91;
            this.logoutButton.Text = "Wyloguj";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // dataWykonania1
            // 
            this.dataWykonania1.CustomFormat = "yyyy-MM-dd";
            this.dataWykonania1.Enabled = false;
            this.dataWykonania1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataWykonania1.Location = new System.Drawing.Point(407, 70);
            this.dataWykonania1.Name = "dataWykonania1";
            this.dataWykonania1.Size = new System.Drawing.Size(84, 20);
            this.dataWykonania1.TabIndex = 90;
            this.dataWykonania1.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataZlecenia1
            // 
            this.dataZlecenia1.CustomFormat = "yyyy-MM-dd";
            this.dataZlecenia1.Enabled = false;
            this.dataZlecenia1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZlecenia1.Location = new System.Drawing.Point(407, 27);
            this.dataZlecenia1.Name = "dataZlecenia1";
            this.dataZlecenia1.Size = new System.Drawing.Size(84, 20);
            this.dataZlecenia1.TabIndex = 89;
            this.dataZlecenia1.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataZleceniaLabel
            // 
            this.dataZleceniaLabel.AutoSize = true;
            this.dataZleceniaLabel.Enabled = false;
            this.dataZleceniaLabel.Location = new System.Drawing.Point(404, 10);
            this.dataZleceniaLabel.Name = "dataZleceniaLabel";
            this.dataZleceniaLabel.Size = new System.Drawing.Size(75, 13);
            this.dataZleceniaLabel.TabIndex = 88;
            this.dataZleceniaLabel.Text = "Data zlecenia:";
            // 
            // stateComboBox
            // 
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(226, 69);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(124, 21);
            this.stateComboBox.TabIndex = 87;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(223, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 86;
            this.label5.Text = "ID badania:";
            // 
            // examIDTextBox
            // 
            this.examIDTextBox.Location = new System.Drawing.Point(226, 27);
            this.examIDTextBox.Name = "examIDTextBox";
            this.examIDTextBox.Size = new System.Drawing.Size(124, 20);
            this.examIDTextBox.TabIndex = 85;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(223, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 84;
            this.label4.Text = "Stan:";
            // 
            // listaBadan
            // 
            this.listaBadan.AllowUserToAddRows = false;
            this.listaBadan.AllowUserToDeleteRows = false;
            this.listaBadan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listaBadan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCol,
            this.CodeCol,
            this.NameCol,
            this.SurnameCol,
            this.StateCol,
            this.DateCol1,
            this.DateCol2,
            this.DateCol3,
            this.DateCol4,
            this.LaboratnCol,
            this.LekarzCol});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.listaBadan.DefaultCellStyle = dataGridViewCellStyle1;
            this.listaBadan.Location = new System.Drawing.Point(15, 158);
            this.listaBadan.MultiSelect = false;
            this.listaBadan.Name = "listaBadan";
            this.listaBadan.ReadOnly = true;
            this.listaBadan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listaBadan.Size = new System.Drawing.Size(1119, 410);
            this.listaBadan.TabIndex = 83;
            this.listaBadan.SelectionChanged += new System.EventHandler(this.badanieList_SelectionChanged);
            // 
            // IdCol
            // 
            this.IdCol.HeaderText = "Id";
            this.IdCol.Name = "IdCol";
            this.IdCol.ReadOnly = true;
            this.IdCol.Width = 50;
            // 
            // CodeCol
            // 
            this.CodeCol.HeaderText = "Kod";
            this.CodeCol.Name = "CodeCol";
            this.CodeCol.ReadOnly = true;
            this.CodeCol.Width = 80;
            // 
            // NameCol
            // 
            this.NameCol.HeaderText = "Imię";
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            // 
            // SurnameCol
            // 
            this.SurnameCol.HeaderText = "Nazwisko";
            this.SurnameCol.Name = "SurnameCol";
            this.SurnameCol.ReadOnly = true;
            // 
            // StateCol
            // 
            this.StateCol.HeaderText = "Stan";
            this.StateCol.Name = "StateCol";
            this.StateCol.ReadOnly = true;
            // 
            // DateCol1
            // 
            this.DateCol1.HeaderText = "Data zlecenia";
            this.DateCol1.Name = "DateCol1";
            this.DateCol1.ReadOnly = true;
            this.DateCol1.Width = 110;
            // 
            // DateCol2
            // 
            this.DateCol2.HeaderText = "Data wykonania";
            this.DateCol2.Name = "DateCol2";
            this.DateCol2.ReadOnly = true;
            this.DateCol2.Width = 110;
            // 
            // DateCol3
            // 
            this.DateCol3.HeaderText = "Data zatwierdzenia";
            this.DateCol3.Name = "DateCol3";
            this.DateCol3.ReadOnly = true;
            // 
            // DateCol4
            // 
            this.DateCol4.HeaderText = "Data anulowania";
            this.DateCol4.Name = "DateCol4";
            this.DateCol4.ReadOnly = true;
            // 
            // LaboratnCol
            // 
            this.LaboratnCol.HeaderText = "Laborant";
            this.LaboratnCol.Name = "LaboratnCol";
            this.LaboratnCol.ReadOnly = true;
            this.LaboratnCol.Width = 125;
            // 
            // LekarzCol
            // 
            this.LekarzCol.HeaderText = "Lekarz";
            this.LekarzCol.Name = "LekarzCol";
            this.LekarzCol.ReadOnly = true;
            // 
            // showBt
            // 
            this.showBt.Enabled = false;
            this.showBt.Location = new System.Drawing.Point(1011, 574);
            this.showBt.Name = "showBt";
            this.showBt.Size = new System.Drawing.Size(123, 23);
            this.showBt.TabIndex = 81;
            this.showBt.Text = "Pokaż";
            this.showBt.UseVisualStyleBackColor = true;
            this.showBt.Click += new System.EventHandler(this.showBt_Click);
            // 
            // searchBt
            // 
            this.searchBt.Location = new System.Drawing.Point(882, 129);
            this.searchBt.Name = "searchBt";
            this.searchBt.Size = new System.Drawing.Size(123, 23);
            this.searchBt.TabIndex = 80;
            this.searchBt.Text = "Szukaj";
            this.searchBt.UseVisualStyleBackColor = true;
            this.searchBt.Click += new System.EventHandler(this.searchBt_Click);
            // 
            // dataWykonaniaLabel
            // 
            this.dataWykonaniaLabel.AutoSize = true;
            this.dataWykonaniaLabel.Enabled = false;
            this.dataWykonaniaLabel.Location = new System.Drawing.Point(404, 52);
            this.dataWykonaniaLabel.Name = "dataWykonaniaLabel";
            this.dataWykonaniaLabel.Size = new System.Drawing.Size(87, 13);
            this.dataWykonaniaLabel.TabIndex = 79;
            this.dataWykonaniaLabel.Text = "Data wykonania:";
            // 
            // dataAnulowania1
            // 
            this.dataAnulowania1.CustomFormat = "yyyy-MM-dd";
            this.dataAnulowania1.Enabled = false;
            this.dataAnulowania1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataAnulowania1.Location = new System.Drawing.Point(647, 70);
            this.dataAnulowania1.Name = "dataAnulowania1";
            this.dataAnulowania1.Size = new System.Drawing.Size(84, 20);
            this.dataAnulowania1.TabIndex = 98;
            this.dataAnulowania1.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataAnulowaniaLabel
            // 
            this.dataAnulowaniaLabel.AutoSize = true;
            this.dataAnulowaniaLabel.Enabled = false;
            this.dataAnulowaniaLabel.Location = new System.Drawing.Point(644, 52);
            this.dataAnulowaniaLabel.Name = "dataAnulowaniaLabel";
            this.dataAnulowaniaLabel.Size = new System.Drawing.Size(90, 13);
            this.dataAnulowaniaLabel.TabIndex = 99;
            this.dataAnulowaniaLabel.Text = "Data anulowania:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(900, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 101;
            this.label9.Text = "Laborant:";
            // 
            // laborantComboBox
            // 
            this.laborantComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.laborantComboBox.FormattingEnabled = true;
            this.laborantComboBox.Location = new System.Drawing.Point(903, 26);
            this.laborantComboBox.Name = "laborantComboBox";
            this.laborantComboBox.Size = new System.Drawing.Size(226, 21);
            this.laborantComboBox.TabIndex = 102;
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(1011, 129);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(123, 23);
            this.clear.TabIndex = 103;
            this.clear.Text = "Wyczyść";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(900, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 104;
            this.label10.Text = "Lekarz:";
            // 
            // LekarzComboBox
            // 
            this.LekarzComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LekarzComboBox.FormattingEnabled = true;
            this.LekarzComboBox.Location = new System.Drawing.Point(903, 69);
            this.LekarzComboBox.Name = "LekarzComboBox";
            this.LekarzComboBox.Size = new System.Drawing.Size(226, 21);
            this.LekarzComboBox.TabIndex = 105;
            // 
            // dataZlecenia2
            // 
            this.dataZlecenia2.CustomFormat = "yyyy-MM-dd";
            this.dataZlecenia2.Enabled = false;
            this.dataZlecenia2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZlecenia2.Location = new System.Drawing.Point(504, 27);
            this.dataZlecenia2.Name = "dataZlecenia2";
            this.dataZlecenia2.Size = new System.Drawing.Size(84, 20);
            this.dataZlecenia2.TabIndex = 106;
            this.dataZlecenia2.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataWykonania2
            // 
            this.dataWykonania2.CustomFormat = "yyyy-MM-dd";
            this.dataWykonania2.Enabled = false;
            this.dataWykonania2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataWykonania2.Location = new System.Drawing.Point(504, 70);
            this.dataWykonania2.Name = "dataWykonania2";
            this.dataWykonania2.Size = new System.Drawing.Size(84, 20);
            this.dataWykonania2.TabIndex = 107;
            this.dataWykonania2.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataZatwierdzenia2
            // 
            this.dataZatwierdzenia2.CustomFormat = "yyyy-MM-dd";
            this.dataZatwierdzenia2.Enabled = false;
            this.dataZatwierdzenia2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZatwierdzenia2.Location = new System.Drawing.Point(744, 27);
            this.dataZatwierdzenia2.Name = "dataZatwierdzenia2";
            this.dataZatwierdzenia2.Size = new System.Drawing.Size(84, 20);
            this.dataZatwierdzenia2.TabIndex = 108;
            this.dataZatwierdzenia2.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // dataAnulowania2
            // 
            this.dataAnulowania2.CustomFormat = "yyyy-MM-dd";
            this.dataAnulowania2.Enabled = false;
            this.dataAnulowania2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataAnulowania2.Location = new System.Drawing.Point(744, 70);
            this.dataAnulowania2.Name = "dataAnulowania2";
            this.dataAnulowania2.Size = new System.Drawing.Size(84, 20);
            this.dataAnulowania2.TabIndex = 109;
            this.dataAnulowania2.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(493, 31);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 13);
            this.label11.TabIndex = 110;
            this.label11.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(493, 74);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 111;
            this.label12.Text = "-";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(733, 29);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 13);
            this.label13.TabIndex = 112;
            this.label13.Text = "-";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(733, 73);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 13);
            this.label14.TabIndex = 113;
            this.label14.Text = "-";
            // 
            // dataZleceniaCheckBox
            // 
            this.dataZleceniaCheckBox.AutoSize = true;
            this.dataZleceniaCheckBox.Location = new System.Drawing.Point(594, 30);
            this.dataZleceniaCheckBox.Name = "dataZleceniaCheckBox";
            this.dataZleceniaCheckBox.Size = new System.Drawing.Size(15, 14);
            this.dataZleceniaCheckBox.TabIndex = 119;
            this.dataZleceniaCheckBox.UseVisualStyleBackColor = true;
            this.dataZleceniaCheckBox.CheckedChanged += new System.EventHandler(this.dataZleceniaCheckBox_CheckedChanged);
            // 
            // dataWykonaniaCheckBox
            // 
            this.dataWykonaniaCheckBox.AutoSize = true;
            this.dataWykonaniaCheckBox.Location = new System.Drawing.Point(594, 73);
            this.dataWykonaniaCheckBox.Name = "dataWykonaniaCheckBox";
            this.dataWykonaniaCheckBox.Size = new System.Drawing.Size(15, 14);
            this.dataWykonaniaCheckBox.TabIndex = 120;
            this.dataWykonaniaCheckBox.UseVisualStyleBackColor = true;
            this.dataWykonaniaCheckBox.CheckedChanged += new System.EventHandler(this.dataWykonaniaCheckBox_CheckedChanged);
            // 
            // dataZatwierdzeniaCheckBox
            // 
            this.dataZatwierdzeniaCheckBox.AutoSize = true;
            this.dataZatwierdzeniaCheckBox.Location = new System.Drawing.Point(834, 30);
            this.dataZatwierdzeniaCheckBox.Name = "dataZatwierdzeniaCheckBox";
            this.dataZatwierdzeniaCheckBox.Size = new System.Drawing.Size(15, 14);
            this.dataZatwierdzeniaCheckBox.TabIndex = 121;
            this.dataZatwierdzeniaCheckBox.UseVisualStyleBackColor = true;
            this.dataZatwierdzeniaCheckBox.CheckedChanged += new System.EventHandler(this.dataZatwierdzeniaCheckBox_CheckedChanged);
            // 
            // dataAnulowaniaCheckBox
            // 
            this.dataAnulowaniaCheckBox.AutoSize = true;
            this.dataAnulowaniaCheckBox.Location = new System.Drawing.Point(834, 73);
            this.dataAnulowaniaCheckBox.Name = "dataAnulowaniaCheckBox";
            this.dataAnulowaniaCheckBox.Size = new System.Drawing.Size(15, 14);
            this.dataAnulowaniaCheckBox.TabIndex = 122;
            this.dataAnulowaniaCheckBox.UseVisualStyleBackColor = true;
            this.dataAnulowaniaCheckBox.CheckedChanged += new System.EventHandler(this.dataAnulowaniaCheckBox_CheckedChanged);
            // 
            // performButton
            // 
            this.performButton.Enabled = false;
            this.performButton.Location = new System.Drawing.Point(882, 574);
            this.performButton.Name = "performButton";
            this.performButton.Size = new System.Drawing.Size(123, 23);
            this.performButton.TabIndex = 123;
            this.performButton.Text = "Weryfikuj";
            this.performButton.UseVisualStyleBackColor = true;
            this.performButton.Click += new System.EventHandler(this.performButton_Click);
            // 
            // KierownikLab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 609);
            this.Controls.Add(this.performButton);
            this.Controls.Add(this.dataAnulowaniaCheckBox);
            this.Controls.Add(this.dataZatwierdzeniaCheckBox);
            this.Controls.Add(this.dataWykonaniaCheckBox);
            this.Controls.Add(this.dataZleceniaCheckBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dataAnulowania2);
            this.Controls.Add(this.dataZatwierdzenia2);
            this.Controls.Add(this.dataWykonania2);
            this.Controls.Add(this.dataZlecenia2);
            this.Controls.Add(this.LekarzComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.laborantComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dataAnulowaniaLabel);
            this.Controls.Add(this.dataAnulowania1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.dataZatwierdzeniaLabel);
            this.Controls.Add(this.dataZatwierdzenia1);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.dataWykonania1);
            this.Controls.Add(this.dataZlecenia1);
            this.Controls.Add(this.dataZleceniaLabel);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.examIDTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listaBadan);
            this.Controls.Add(this.showBt);
            this.Controls.Add(this.searchBt);
            this.Controls.Add(this.dataWykonaniaLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "KierownikLab";
            this.ShowIcon = false;
            this.Text = "Kierownik Laboratorium";
            this.Load += new System.EventHandler(this.KierownikLab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listaBadan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SurnameTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label dataZatwierdzeniaLabel;
        private System.Windows.Forms.DateTimePicker dataZatwierdzenia1;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.DateTimePicker dataWykonania1;
        private System.Windows.Forms.DateTimePicker dataZlecenia1;
        private System.Windows.Forms.Label dataZleceniaLabel;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox examIDTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView listaBadan;
        private System.Windows.Forms.Button showBt;
        private System.Windows.Forms.Button searchBt;
        private System.Windows.Forms.Label dataWykonaniaLabel;
        private System.Windows.Forms.DateTimePicker dataAnulowania1;
        private System.Windows.Forms.Label dataAnulowaniaLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox laborantComboBox;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurnameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCol4;
        private System.Windows.Forms.DataGridViewTextBoxColumn LaboratnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn LekarzCol;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox LekarzComboBox;
        private System.Windows.Forms.DateTimePicker dataZlecenia2;
        private System.Windows.Forms.DateTimePicker dataWykonania2;
        private System.Windows.Forms.DateTimePicker dataZatwierdzenia2;
        private System.Windows.Forms.DateTimePicker dataAnulowania2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Timer autorefresh;
        private System.Windows.Forms.CheckBox dataZleceniaCheckBox;
        private System.Windows.Forms.CheckBox dataWykonaniaCheckBox;
        private System.Windows.Forms.CheckBox dataZatwierdzeniaCheckBox;
        private System.Windows.Forms.CheckBox dataAnulowaniaCheckBox;
        private System.Windows.Forms.Button performButton;
    }
}