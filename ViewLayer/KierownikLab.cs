﻿using BusinessLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLayer;
using StanBadaniaItem = Forms.ComboBoxItem<DataLayer.StanBadania?>;
using KontoIdItem = Forms.ComboBoxItem<int?>;

namespace ViewLayer
{
    public partial class KierownikLab : Form
    {

        public KierownikLab(ApplicationEngine e, int idKierownika)
        {
            InitializeComponent();
            engine = e;
            this.idKierownika = idKierownika;
            stateComboBox.Items.Add(new StanBadaniaItem { Text = " ", Value = null });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Zlecone.ToString(), Value = StanBadania.Zlecone });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Wykonywane.ToString(), Value = StanBadania.Wykonywane });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Wykonane.ToString(), Value = StanBadania.Wykonane });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.Zatwierdzone.ToString(), Value = StanBadania.Zatwierdzone });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.AnulowaneLaborant.ToString(), Value = StanBadania.AnulowaneLaborant });
            stateComboBox.Items.Add(new StanBadaniaItem { Text = StanBadania.AnulowaneKierownik.ToString(), Value = StanBadania.AnulowaneKierownik });

            KontoIdItem dowolny = new KontoIdItem { Text = " ", Value = null };
            LekarzComboBox.Items.Add(dowolny);
            laborantComboBox.Items.Add(dowolny);

            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
                .Wykonaj(x => {
                    LekarzComboBox.Items.Add(new KontoIdItem { Text = x.Nazwisko + " " + x.Imie, Value = x.Id });
                });

            new ManagerLaborantow.PoszukiwaczLaborantow(engine.LaborantManager)
                .Wykonaj(x => {
                    laborantComboBox.Items.Add(new KontoIdItem { Text = x.Nazwisko + " " + x.Imie, Value = x.Id });
                });


            ustawDomyslneWartosci();
            LadujDomyslneBadania();
            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;

        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private readonly ApplicationEngine engine;
        private readonly int idKierownika;
        private readonly Regex idRegex = new Regex(@"^\d*$");
        private bool odswiezSzukaneBadania = false;


        private void logoutButton_Click(object sender, EventArgs e)
        {
            engine.KontoManager.Wyloguj();
        }

        private bool IsUserInputValid()
        {
            return (idRegex.Match(examIDTextBox.Text).Success);
        }

        private void searchBt_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                odswiezSzukaneBadania = true;
                LadujSzukaneBadania();
            }
            else
            {
                MessageBox.Show("Nieprawidłowe dane szukanego badania!");
            }
        }

        private void showBt_Click(object sender, EventArgs e)
        {
            int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            using (var form = new BadanieLaboratoryjnePokaz(engine, idBadania))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void performButton_Click(object sender, EventArgs e)
        {
            int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            using (var form = new BadanieLaboratoryjneKierownikLab(engine, idBadania, idKierownika))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void badanieList_SelectionChanged(object sender, EventArgs e)
        {
            if(listaBadan.SelectedRows.Count == 1)
            {
                showBt.Enabled = true;
                StanBadania stan = (StanBadania)listaBadan.SelectedRows[0].Cells[4].Value;
                if ((stan == StanBadania.Zatwierdzone) || (stan == StanBadania.AnulowaneKierownik) || (stan == StanBadania.AnulowaneLaborant))
                    performButton.Enabled = false;
                else
                    performButton.Enabled = true;

            }
            else
            {
                showBt.Enabled = performButton.Enabled = false;
            }
        }

        private void clear_Click(object sender, EventArgs e)
        {
            odswiezSzukaneBadania = false;
            ustawDomyslneWartosci();
            LadujDomyslneBadania();
        }

        private void ustawDomyslneWartosci()
        {
            NameTextBox.Text = SurnameTextBox.Text = examIDTextBox.Text = string.Empty;
            dataZlecenia1.Value = dataWykonania1.Value = dataZatwierdzenia1.Value = dataAnulowania1.Value = DateTime.Now;
            stateComboBox.SelectedIndex = 3;
            LekarzComboBox.SelectedIndex = laborantComboBox.SelectedIndex = 0;
            dataZlecenia2.Value = dataZatwierdzenia2.Value = dataWykonania2.Value = dataAnulowania2.Value = DateTime.Now;
            dataZlecenia1.Value = DateTime.Now.AddYears(-1);
            dataZatwierdzenia1.Value = DateTime.Now.AddDays(-7);
            dataAnulowania1.Value = DateTime.Now.AddMonths(-1);
            dataWykonania1.Value = DateTime.Now;
            dataZlecenia1.Enabled = dataZlecenia2.Enabled = dataZleceniaLabel.Enabled = dataZleceniaCheckBox.Checked =
                dataWykonania1.Enabled = dataWykonania2.Enabled = dataWykonaniaLabel.Enabled = dataWykonaniaCheckBox.Checked =
                dataZatwierdzenia1.Enabled = dataZatwierdzenia2.Enabled = dataZatwierdzeniaLabel.Enabled = dataZatwierdzeniaCheckBox.Checked =
                dataAnulowania1.Enabled = dataAnulowania2.Enabled = dataAnulowaniaLabel.Enabled = dataAnulowaniaCheckBox.Checked = false;

        }

        private void LadujDomyslneBadania()
        {
            listaBadan.Rows.Clear();
            new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                .Stan(StanBadania.Wykonane)
                  .Wykonaj(x => {
                      if (x.Laborant != null)
                      {
                          listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.Wizyta.Pacjent.Imie, x.Wizyta.Pacjent.Nazwisko, x.Stan, x.DataZlecenia, x.DataWykonania,
                          x.DataZatwierdzenia, x.DataAnulowania, x.Laborant.Nazwisko + " " + x.Laborant.Imie, x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie);
                      }
                      else
                      {
                          listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.Wizyta.Pacjent.Imie, x.Wizyta.Pacjent.Nazwisko, x.Stan, x.DataZlecenia, x.DataWykonania,
                          x.DataZatwierdzenia, x.DataAnulowania, "-", x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie);
                      }
                  });
            listaBadan.Sort(listaBadan.Columns[5], ListSortDirection.Descending);
            listaBadan.ClearSelection();
        }

        private void LadujSzukaneBadania()
        {
            listaBadan.Rows.Clear();
            var poszukiwacz = new ManagerBadanLaboratoryjnych.PoszukiwaczBadanLaboratoryjnych(engine.BadanieLaboratoryjneManager)
                .ImiePacjenta(NameTextBox.Text)
                .NazwiskoPacjenta(SurnameTextBox.Text);
            if (dataZleceniaCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataZlecenia(dataZlecenia1.Value)
                            .NajpozniejszaDataZlecenia(dataZlecenia2.Value);
            if (dataWykonaniaCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataWykonania(dataWykonania1.Value)
                            .NajpozniejszaDataWykonania(dataWykonania2.Value);
            if (dataZatwierdzeniaCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataZatwierdzenia(dataZatwierdzenia1.Value)
                            .NajpozniejszaDataZatwierdzenia(dataZatwierdzenia2.Value);
            if(dataAnulowaniaCheckBox.Checked == true)
                poszukiwacz.NajwczesniejszaDataAnulowania(dataAnulowania1.Value)
                            .NajpozniejszaDataAnulowania(dataAnulowania2.Value);
            var stan = (stateComboBox.SelectedItem as StanBadaniaItem).Value;
            if (stan != null)
                poszukiwacz.Stan(stan.Value);
            var idLekarza = (LekarzComboBox.SelectedItem as KontoIdItem).Value;
            if (idLekarza != null)
                poszukiwacz.IdLekarza(idLekarza.Value);
            var idLaboranta = (laborantComboBox.SelectedItem as KontoIdItem).Value;
            if (idLaboranta != null)
                poszukiwacz.IdLaboranta(idLaboranta.Value);
            if (!string.IsNullOrEmpty(examIDTextBox.Text))
                poszukiwacz.IdBadania(int.Parse(examIDTextBox.Text));
            poszukiwacz.Wykonaj(x => {
                if (x.Laborant != null)
                {
                    listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.Wizyta.Pacjent.Imie, x.Wizyta.Pacjent.Nazwisko, x.Stan, x.DataZlecenia, x.DataWykonania,
                    x.DataZatwierdzenia, x.DataAnulowania, x.Laborant.Nazwisko + " " + x.Laborant.Imie, x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie);
                }
                else
                {
                    listaBadan.Rows.Add(x.Id, x.TypBadania.Kod, x.Wizyta.Pacjent.Imie, x.Wizyta.Pacjent.Nazwisko, x.Stan, x.DataZlecenia, x.DataWykonania,
                    x.DataZatwierdzenia, x.DataAnulowania, "-", x.Wizyta.Lekarz.Nazwisko + " " + x.Wizyta.Lekarz.Imie);
                }
            });
            listaBadan.Sort(listaBadan.Columns[5], ListSortDirection.Descending);
            listaBadan.ClearSelection();
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if (listaBadan.SelectedRows.Count == 1)
            {
                int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
                if (odswiezSzukaneBadania == true)
                    LadujSzukaneBadania();
                else
                    LadujDomyslneBadania();
                foreach (DataGridViewRow row in listaBadan.Rows)
                {
                    if ((int)row.Cells[0].Value == idBadania)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                if (odswiezSzukaneBadania == true)
                    LadujSzukaneBadania();
                else
                    LadujDomyslneBadania();
            }
        }

        private void KierownikLab_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void dataZleceniaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dataZlecenia1.Enabled = dataZlecenia2.Enabled = dataZleceniaLabel.Enabled =
                (dataZleceniaCheckBox.Checked == true) ? true : false;
        }

        private void dataWykonaniaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dataWykonania1.Enabled = dataWykonania2.Enabled = dataWykonaniaLabel.Enabled =
                (dataWykonaniaCheckBox.Checked == true) ? true : false;
        }

        private void dataZatwierdzeniaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dataZatwierdzenia1.Enabled = dataZatwierdzenia2.Enabled = dataZatwierdzeniaLabel.Enabled =
                (dataZatwierdzeniaCheckBox.Checked == true) ? true : false;
        }

        private void dataAnulowaniaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dataAnulowania1.Enabled = dataAnulowania2.Enabled = dataAnulowaniaLabel.Enabled =
                (dataAnulowaniaCheckBox.Checked == true) ? true : false;
        }
    }
}
