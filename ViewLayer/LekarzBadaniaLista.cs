﻿using BusinessLayer;
using DataLayer;
using Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public partial class LekarzBadaniaLista : Form
    {
        public LekarzBadaniaLista(ApplicationEngine e, int idWizyty)
        {
            InitializeComponent();
            this.engine = e;
            this.idWizyty = idWizyty;
            LadujBadania();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private void backBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void showBt_Click(object sender, EventArgs e)
        {
            int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
            BadanieTyp typBadania = (BadanieTyp)listaBadan.SelectedRows[0].Cells[5].Value;
            if(typBadania == BadanieTyp.Fizyczne)
            {
                using (var form = new BadanieFizykalne(engine, idBadania, BadanieFizykalne.Tryb.Pokaz))
                {
                    autorefresh.Stop();
                    form.FormClosed += ChildClosing;
                    form.ShowDialog();
                }
            }
            else
            {
                using (var form = new BadanieLaboratoryjnePokaz(engine, idBadania))
                {
                    autorefresh.Stop();
                    form.FormClosed += ChildClosing;
                    form.ShowDialog();
                }     
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            showBt.Enabled = (listaBadan.SelectedRows.Count == 1) ? true : false;
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if(listaBadan.SelectedRows.Count == 1)
            {
                int idBadania = (int)listaBadan.SelectedRows[0].Cells[0].Value;
                BadanieTyp typBadania = (BadanieTyp)listaBadan.SelectedRows[0].Cells[5].Value;
                LadujBadania();
                foreach (DataGridViewRow row in listaBadan.Rows)
                {
                    if (((int)row.Cells[0].Value == idBadania) && ((BadanieTyp)row.Cells[5].Value == typBadania))
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                LadujBadania();
            }
        }

        private void LadujBadania()
        {
            listaBadan.Rows.Clear();
            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                   .IdWizyty(idWizyty)
                   .Wykonaj(x =>
                   {
                       char[] delimiters = new char[] { ' ', '\r', '\n' };
                       foreach (var b in x.BadaniaFizyczne)
                       {
                           string[] words = b.Wynik.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                           string wynik = (words.Length > 1) ? (words[0] + "...") : b.Wynik;
                           listaBadan.Rows.Add(b.Id, b.TypBadania.Kod, b.TypBadania.Nazwa, StanBadania.Wykonane, wynik, BadanieTyp.Fizyczne);
                       }
                       foreach (var b in x.BadaniaLaboratoryjne)
                       {
                           if (b.Wynik != null)
                           {
                               string[] words = b.Wynik.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                               string wynik = (words.Length > 1) ? (words[0] + "...") : b.Wynik;
                               listaBadan.Rows.Add(b.Id, b.TypBadania.Kod, b.TypBadania.Nazwa, b.Stan, wynik, BadanieTyp.Laboratoryjne);
                           }
                           else
                               listaBadan.Rows.Add(b.Id, b.TypBadania.Kod, b.TypBadania.Nazwa, b.Stan, "-", BadanieTyp.Laboratoryjne);
                       }
                       listaBadan.Sort(listaBadan.Columns[1], ListSortDirection.Ascending);
                   });
        }

        private readonly ApplicationEngine engine;
        private readonly int idWizyty;

        private void LekarzBadaniaLista_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }
    }
}
