﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewLayer
{
    public class ApplicationLoader
    {
#if DEBUG
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();
#endif
        public ApplicationLoader(Action<Exception> onException)
        {
            exHandler = onException;
#if DEBUG
            msgBoxResult = MessageBox.Show("otwieranie połączenia. czy użyć istniejącej już bazy ? (nie aby stworzyć nową)", "DEBUG MODE", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            AllocConsole();
#endif
            new Thread(() =>
            {
                lock (this)
                {
                    loadingStart.Set();
                    load();
                }
            }).Start();
            loadingStart.WaitOne();
        }
        public ApplicationEngine Engine
        {
            get
            {
                lock (this)
                {
                    return engine;
                }
            }
        }

        private void load()
        {
            try
            {
                ApplicationEngine e = null;
#if DEBUG
                if (msgBoxResult == DialogResult.Yes)
                {
                    e = new ApplicationEngine(false, exHandler);
                }
                else if (msgBoxResult == DialogResult.No)
                {
                    e = new ApplicationEngine(true, exHandler);
                }
                else
                {
                    Application.Exit();
                }
#else
                e = new ApplicationEngine(false, exHandler);
#endif
                engine = e;
            }
            catch (Exception e)
            {
                if (exHandler != null)
                {
                    exHandler(e);
                }
                Application.Exit();
            }
        }
        private ApplicationEngine engine;
        private ManualResetEvent loadingStart = new ManualResetEvent(false);
        private Action<Exception> exHandler;
        private DialogResult msgBoxResult;
    }
}
