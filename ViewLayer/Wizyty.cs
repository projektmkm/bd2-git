﻿using BusinessLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewLayer;
using KontoIdItem = Forms.ComboBoxItem<int?>;
using StanWizytyItem = Forms.ComboBoxItem<DataLayer.StanWizyty?>;

namespace Forms
{
    public partial class Wizyty : Form
    {
        public Wizyty(ApplicationEngine e, int idRejestratora)
        {
            InitializeComponent();
            engine = e;
            this.idRejestratora = idRejestratora;
            KontoIdItem dowolny = new KontoIdItem { Text = " ", Value = null };
            DoctorComboBox.Items.Add(dowolny);

            new ManagerLekarzy.PoszukiwaczLekarzy(engine.LekarzManager)
              .Wykonaj(x => {
                  DoctorComboBox.Items.Add(new KontoIdItem { Text = x.Nazwisko + " " + x.Imie, Value = x.Id });
              });

            stateComboBox.Items.Add(new StanWizytyItem { Text = " ", Value = null });
            stateComboBox.Items.Add(new StanWizytyItem { Text = StanWizyty.Zarejestrowana.ToString(), Value = StanWizyty.Zarejestrowana });
            stateComboBox.Items.Add(new StanWizytyItem { Text = StanWizyty.WToku.ToString(), Value = StanWizyty.WToku });
            stateComboBox.Items.Add(new StanWizytyItem { Text = StanWizyty.Wykonana.ToString(), Value = StanWizyty.Wykonana });
            stateComboBox.Items.Add(new StanWizytyItem { Text = StanWizyty.Anulowana.ToString(), Value = StanWizyty.Anulowana });

            UstawDomyslneWartosci();
            LadujWizyty();

            autorefresh.Interval = 1000 * Forms.Properties.Settings.Default.AutoRefreshLong;
            autorefresh.Tick += Autorefresh_Tick;
        }

        private void Autorefresh_Tick(object sender, EventArgs e)
        {
            Odswiez();
        }

        private readonly ApplicationEngine engine;
        private bool odswiezSzukaneWizyty = false;
        private readonly int idRejestratora;

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            autorefresh.Stop();
            using (var form = new Pacjenci(engine, idRejestratora, Pacjenci.Tryb.UstawieniaWizyty))
                form.ShowDialog();
            autorefresh.Start();

            UstawDomyslneWartosci();
            odswiezSzukaneWizyty = false;
            LadujWizyty();
            if (listaWizyt.SelectedRows.Count > 0)
            {
                int idWizyty = (int)listaWizyt.SelectedRows[0].Cells[0].Value;
                
                foreach (DataGridViewRow row in listaWizyt.Rows)
                {
                    if ((int)row.Cells[0].Value == idWizyty)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }
        }

        private void searchBt_Click(object sender, EventArgs e)
        {
            odswiezSzukaneWizyty = true;
            LadujSzukaneWizyty();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            int liczbaAnulowanychWizyt = listaWizyt.SelectedRows.Count;
            string komunikat;
            if (liczbaAnulowanychWizyt == 1)
                komunikat = "Czy na pewno chcesz anulować tą wizytę?";
            else
                komunikat = "Czy na pewno chcesz anulować " + liczbaAnulowanychWizyt + " wizyt?";
            if(MessageBox.Show(komunikat, "",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in listaWizyt.SelectedRows)
                {
                    int idWizyty = (int)row.Cells[0].Value;
                    engine.WizytaManager.AnulujWizyte(idWizyty);
                    row.Cells[3].Value = StanWizyty.Anulowana;
                }
                cancelButton.Enabled = false;
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            int idWizyty = (int)listaWizyt.SelectedRows[0].Cells[0].Value;
            using (var form = new WizytaEdycja(engine, idWizyty))
            {
                autorefresh.Stop();
                form.FormClosed += ChildClosing;
                form.ShowDialog();
            }
        }

        private void VisitsList_SelectionChanged(object sender, EventArgs e)
        {
            if (listaWizyt.SelectedRows.Count == 1)
            {
                StanWizyty stan = (StanWizyty)listaWizyt.SelectedRows[0].Cells[3].Value;
                cancelButton.Enabled = EditButton.Enabled = (stan == StanWizyty.Zarejestrowana) ? true : false;
            }
            else if(listaWizyt.SelectedRows.Count > 1)
            {
                cancelButton.Enabled = true;
                EditButton.Enabled = false;
                foreach (DataGridViewRow row in listaWizyt.SelectedRows)
                {
                    StanWizyty stan = (StanWizyty)row.Cells[3].Value;
                    if(stan != StanWizyty.Zarejestrowana)
                    {
                        cancelButton.Enabled = false;
                        break;
                    }
                }
            }else
                cancelButton.Enabled = EditButton.Enabled = false;
        }

        private void LadujWizyty()
        {
            listaWizyt.Rows.Clear();
            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .NajwczesniejszaDataRejestracji(DateTime.Now)
                .NajpozniejszaDataRejestracji(DateTime.Now)
                .Wykonaj(x => {
                    listaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.Stan, x.DataRejestracji,
                    x.Lekarz.Nazwisko + " " + x.Lekarz.Imie);
                });
            listaWizyt.ClearSelection();
            new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .Stan(StanWizyty.Zarejestrowana)
                .Wykonaj(x => {
                    bool juzDodane = false;
                    foreach(DataGridViewRow row in listaWizyt.Rows)
                    {
                        if(x.IdWizyty  == (int)row.Cells[0].Value)
                        {
                            juzDodane = true;
                            break;
                        }
                    }
                    if(juzDodane == false)
                        listaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.Stan, x.DataRejestracji,
                        x.Lekarz.Nazwisko + " " + x.Lekarz.Imie);
                });
            listaWizyt.Sort(listaWizyt.Columns[4], System.ComponentModel.ListSortDirection.Descending);
            listaWizyt.ClearSelection();
        }

        private void LadujSzukaneWizyty()
        {
            listaWizyt.Rows.Clear();
            var poszukiwacz = new ManagerWizyt.PoszukiwaczWizyt(engine.WizytaManager)
                .ImiePacjenta(NameTextBox.Text)
                .NazwiskoPacjenta(SurnameTextBox.Text);
            if (regDateCheckBox.Checked == true) { 
                poszukiwacz.NajwczesniejszaDataRejestracji(dataRejestracji1.Value)
                .NajpozniejszaDataRejestracji(dataRejestracji2.Value);
            }
            var idLekarza = (DoctorComboBox.SelectedItem as KontoIdItem).Value;
            if (idLekarza != null)
                poszukiwacz.IdLekarza(idLekarza.Value);
            var stan = (stateComboBox.SelectedItem as StanWizytyItem).Value;
            if (stan != null)
                poszukiwacz.Stan(stan.Value);
            poszukiwacz.Wykonaj(x => {
                listaWizyt.Rows.Add(x.IdWizyty, x.Pacjent.Imie, x.Pacjent.Nazwisko, x.Stan, x.DataRejestracji,
                    x.Lekarz.Nazwisko + " " + x.Lekarz.Imie);
            });
            listaWizyt.Sort(listaWizyt.Columns[4], System.ComponentModel.ListSortDirection.Descending);
        }

        private void UstawDomyslneWartosci()
        {
            stateComboBox.SelectedIndex = 1;
            DoctorComboBox.SelectedIndex = 0;
            NameTextBox.Text = SurnameTextBox.Text = string.Empty;
            dataRejestracji1.Value = dataRejestracji2.Value = DateTime.Now;
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled = regDateCheckBox.Checked = false;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            UstawDomyslneWartosci();
            odswiezSzukaneWizyty = false;
            LadujWizyty();
        }

        private void ChildClosing(object sender, FormClosedEventArgs e)
        {
            Odswiez();
            autorefresh.Start();
        }

        private void Odswiez()
        {
            if (listaWizyt.SelectedRows.Count > 0)
            {
                int idWizyty = (int)listaWizyt.SelectedRows[0].Cells[0].Value;
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujWizyty();
                listaWizyt.ClearSelection();
                foreach (DataGridViewRow row in listaWizyt.Rows)
                {
                    if ((int)row.Cells[0].Value == idWizyty)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }else
            {
                if (odswiezSzukaneWizyty == true)
                    LadujSzukaneWizyty();
                else
                    LadujWizyty();
            }
        }

        private void Wizyty_Load(object sender, EventArgs e)
        {
            autorefresh.Start();
        }

        private void regDateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            regDateLabel.Enabled = dataRejestracji1.Enabled = dataRejestracji2.Enabled = (regDateCheckBox.Checked == true) ? true : false;
        }
    }
}
