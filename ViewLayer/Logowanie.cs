﻿using DataLayer;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;

namespace ViewLayer
{
    public partial class Logowanie : Form
    {
        private readonly Regex LoginRegex = new Regex(@"^(\S)+$");
        private readonly Regex PasswordRegex = new Regex(@"^(\S)+$");

        public Logowanie(ApplicationLoader e)
        {
            InitializeComponent();
            loader = e;
            TopMost = true;//make window appear on top
            TopMost = false;
            Activate();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void loginBt_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                this.Cursor = Cursors.WaitCursor;
                this.LoginButton.Enabled = false;
                LoginTextBox.Cursor = Cursors.WaitCursor;
                LoginTextBox.Enabled = false;
                PasswordTextBox.Cursor = Cursors.WaitCursor;
                PasswordTextBox.Enabled = false;
                getEngine();
                timer = new System.Windows.Forms.Timer();
                timer.Interval = 5;
                timer.Tick += new EventHandler(onLoadingTimer);
                timer.Start();
                progressBar1.Show();
                progressBar1.Value = progressBar1.Minimum;
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private void getEngine()
        {
            if (engine != null) return;
            new Thread(() => {
                var e = loader.Engine;
                e.KontoManager.AddLogoutListener(onLogout);
                engine = e;
            }).Start();
        }
        private void onLoadingTimer(object obj1, object obj2)
        {
            if (engine != null)
            {
                timer.Stop();
                this.progressBar1.Hide();
                this.Cursor = Cursors.Default;
                this.LoginButton.Enabled = true;
                LoginTextBox.Cursor = Cursors.Default;
                LoginTextBox.Enabled = true;
                PasswordTextBox.Cursor = Cursors.Default;
                PasswordTextBox.Enabled = true;
                if (engine.KontoManager.Zaloguj(LoginTextBox.Text, PasswordTextBox.Text))
                {
                    var role = engine.KontoManager.RolaZalogowanegoKonta();
                    if (role == KontoRola.Administrator)
                    {
                        toShow = new Admin(engine);
                    }
                    else if (role == KontoRola.Lekarz)
                    {
                        toShow = new Lekarz(engine, engine.KontoManager.ZalogowaneKontoId.Value);
                    }
                    else if (role == KontoRola.Laborant)
                    {
                        toShow = new Laborant(engine, engine.KontoManager.ZalogowaneKontoId.Value);
                    }
                    else if (role == KontoRola.KierownikLaboratorium)
                    {
                        toShow = new KierownikLab(engine, engine.KontoManager.ZalogowaneKontoId.Value);
                    }
                    else if (role == KontoRola.Rejestrator)
                    {
                        toShow = new Forms.Rejestrator(engine, engine.KontoManager.ZalogowaneKontoId.Value);
                    }
                    if (toShow != null)
                    {
                        ErrorProvider.Clear();
                        Hide();
                        toShow.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Błąd - brak obsługiwanych ról dla konta");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Logowanie się nie powiodło. Zły login lub hasło", "Błąd logowania", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private System.Windows.Forms.Timer timer;
        private ApplicationLoader loader;
        private ApplicationEngine engine;
        private Form toShow = null;
        private void onLogout()
        {
            if (toShow != null)
            {
                toShow.Close();
                toShow = null;
            }
            LoginTextBox.Text = "";
            PasswordTextBox.Text = "";
            Show();
            LoginTextBox.Focus();
        }

        private bool IsUserInputValid()
        {
            return (LoginRegex.Match(LoginTextBox.Text).Success) && (PasswordRegex.Match(PasswordTextBox.Text).Success);
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (LoginRegex.Match(LoginTextBox.Text).Success == false)
                ErrorProvider.SetError(LoginTextBox, "Nieprawidłowy login");
            if (LoginRegex.Match(PasswordTextBox.Text).Success == false)
                ErrorProvider.SetError(PasswordTextBox, "Nieprawidłowe hasło");
        }

    }
}
