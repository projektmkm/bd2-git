﻿using BusinessLayer;
using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class AdminBadanie : Form
    {
        private readonly ApplicationEngine engine;
        private readonly BadanieTyp typBadania;
        private readonly int? idBadania;
        private string poprzedniKodBadania;
        public AdminBadanie(ApplicationEngine e, BadanieTyp typ, int? idBadania = null)
        {
            InitializeComponent();
            engine = e;
            typBadania = typ;
            this.idBadania = idBadania;
            if (this.idBadania != null)
            {
                this.Text = "Edytuj badanie";
                new ManagerTypowBadan.PoszukiwaczTypowBadan(engine.SlownikBadanManager)
                .IdBadania(this.idBadania.Value)
                .Wykonaj(x => {
                    nameTextBox.Text = x.Nazwa;
                    codeTextBox.Text = x.Kod;
                    descriptionTextBox.Text = x.Opis;
                });
                poprzedniKodBadania = codeTextBox.Text;
            }
            else
            {
                this.Text = "Dodaj badanie";
            }
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool IsUserInputValid()
        {
            return (!string.IsNullOrWhiteSpace(nameTextBox.Text)) && (!string.IsNullOrWhiteSpace(descriptionTextBox.Text))
                && (!string.IsNullOrWhiteSpace(codeTextBox.Text));
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (IsUserInputValid())
            {
                if (idBadania != null)
                {
                    if (poprzedniKodBadania != codeTextBox.Text)
                    {
                        if (!engine.SlownikBadanManager.KodWolny(codeTextBox.Text))
                        {
                            MessageBox.Show("Podany kod jest już zajęty!");
                            return;
                        }
                    }
                    engine.SlownikBadanManager.EdytujTypBadania(idBadania.Value,
                            codeTextBox.Text,
                            nameTextBox.Text,
                            descriptionTextBox.Text,
                            typBadania);
                    Close();
                }
                else
                {
                    if (engine.SlownikBadanManager.KodWolny(codeTextBox.Text))
                    {
                        engine.SlownikBadanManager.DodajTypBadania(codeTextBox.Text,
                            nameTextBox.Text,
                            descriptionTextBox.Text,
                            typBadania);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Podany kod jest już zajęty!");
                    }
                }
            }
            else
            {
                ZaznaczNieprawidloweDane();
            }
        }

        private void ZaznaczNieprawidloweDane()
        {
            ErrorProvider.Clear();
            if (string.IsNullOrWhiteSpace(nameTextBox.Text))
                ErrorProvider.SetError(nameTextBox, "Podaj nazwę");
            if (string.IsNullOrWhiteSpace(codeTextBox.Text))
                ErrorProvider.SetError(codeTextBox, "Podaj kod");
            if (string.IsNullOrWhiteSpace(descriptionTextBox.Text))
                ErrorProvider.SetError(descriptionTextBox, "Uzupełnij opis");
        }
    }
}
