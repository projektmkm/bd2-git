﻿namespace Forms
{
    partial class Rejestrator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VisitsButton = new System.Windows.Forms.Button();
            this.RegisterVisitButton = new System.Windows.Forms.Button();
            this.PatientsButton = new System.Windows.Forms.Button();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // VisitsButton
            // 
            this.VisitsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.VisitsButton.Location = new System.Drawing.Point(66, 73);
            this.VisitsButton.Name = "VisitsButton";
            this.VisitsButton.Size = new System.Drawing.Size(105, 45);
            this.VisitsButton.TabIndex = 36;
            this.VisitsButton.Text = "Wizyty";
            this.VisitsButton.UseVisualStyleBackColor = true;
            this.VisitsButton.Click += new System.EventHandler(this.visitsButton_Click);
            // 
            // RegisterVisitButton
            // 
            this.RegisterVisitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterVisitButton.Location = new System.Drawing.Point(66, 22);
            this.RegisterVisitButton.Name = "RegisterVisitButton";
            this.RegisterVisitButton.Size = new System.Drawing.Size(105, 45);
            this.RegisterVisitButton.TabIndex = 37;
            this.RegisterVisitButton.Text = "Rejestruj wizytę";
            this.RegisterVisitButton.UseVisualStyleBackColor = true;
            this.RegisterVisitButton.Click += new System.EventHandler(this.regVisButton_Click);
            // 
            // PatientsButton
            // 
            this.PatientsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PatientsButton.Location = new System.Drawing.Point(66, 124);
            this.PatientsButton.Name = "PatientsButton";
            this.PatientsButton.Size = new System.Drawing.Size(105, 45);
            this.PatientsButton.TabIndex = 38;
            this.PatientsButton.Text = "Pacjenci";
            this.PatientsButton.UseVisualStyleBackColor = true;
            this.PatientsButton.Click += new System.EventHandler(this.patientsButton_Click);
            // 
            // LogoutButton
            // 
            this.LogoutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LogoutButton.Location = new System.Drawing.Point(66, 175);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(105, 45);
            this.LogoutButton.TabIndex = 39;
            this.LogoutButton.Text = "Wyloguj";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // Rejestrator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 244);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.PatientsButton);
            this.Controls.Add(this.RegisterVisitButton);
            this.Controls.Add(this.VisitsButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Rejestrator";
            this.ShowIcon = false;
            this.Text = "Rejestrator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button VisitsButton;
        private System.Windows.Forms.Button RegisterVisitButton;
        private System.Windows.Forms.Button PatientsButton;
        private System.Windows.Forms.Button LogoutButton;
    }
}