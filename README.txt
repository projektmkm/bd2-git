﻿Konta tworzone domyślnie podczas instalacji oprogramowania (opcjonalne):

Rola:				Login:				Hasło:
---------------------------------------------------------------------------------------------
Administrator			admin				admin
Lekarz				lekarz				lekarz
Lekarz				lekarz2				lekarz2
Rejestrator			rejestrator			rejestrator
Kierownik			kierownik			kierownik
Laborant			laborant			laborant
Laborant			laborant2			laborant2
---------------------------------------------------------------------------------------------
	
	
1.  Administrator:
	Po zalogowaniu otwiera się okienko zawierające następujące opcje:
	* Zarządzaj użytkownikami 
		- dodawanie
		- wyszukiwanie
		- edycja użytkowników
	* Uzupełnij badania fizykalne
		- dodawanie
		- edycja
		- usuwanie (tylko, gdy nie zostało wykonane żadne badanie danego typu)
	* Uzupełnij badania laboratoryjne
		- jak wyżej, tylko badania laboratoryjne
	* Raport wizyty
		- generowanie raportów
	* Wyloguj
	
2. Rejestrator:
	Opcje dostępne w główny okienku rejestratora:
	* Rejestruj wizytę:
		- otwiera okienko wybou pacjentów, w którym należy odszukać i wybrać pacjenta. Po wybraniu pacjenta i kliknięciu
		  przyciku "dalej" rejetrator ma możliwośc wyboru lekarza oraz godziny. Kilknięcie przyciku "rejestruj" powoduje zarejestrawanie wizyty.
	* Wizyty:
		- rejestracja wizyty (proces przebiega tak samo jak w przypadku opcji "Rejestruj wizytę" w głównym okienku)
		- edycja istniejącej wizyty
		- wyszukiwanie
		- anulowanie wizyt
	* Pacjenci
		- dodawanie
		- edycja
		- wyszukiwanie
		
3. Lekarz:
	Po zalogowaniu pojawia się okienko z wizytami lekarza (zarestrowane i rozpoczęte). Lekarz ma możliwośc wyszukiwania,
	anulowania, rozpoczęcia obsługi i podglądu wizyt.
	* Obsługa wizyty:
	   - możliwośc wykonania badania fizykalnego
	   - możliwośc zlecenia badania laboratoryjnego
	   - możliwośc podglądu innych wizyt pacjęta włącznie z badaniami wykonanymi lub zleconymi w ramach tych wizyt.
	   - możliwośc podlądu badań wykonanych lub zleconych w ramach aktualnie obsługiwanej wizyty.
	   - możliwość zakończenia lub anlowania wizyty

4. Laborant:
    Po zalogowaniu pojawia się okienko z wszystkimi zleconymi badaniami laboratoryjnymi, oraz badaniami rozpoczętymi przez danego laboranta
	Laborant ma możliwośc wyszukiwania, wykonania, anulowania oraz podglądu badań.
	
5. Kierownik:
    Po zalogowaniu pojawia się okienko z wszystkimi wykonanymi badaniami laboratoryjnyi. Kierownik ma możliwośc wyszukiwania, podglądu oraz weryfikacji
	badań.
	* Weryfikacja badania:
	   - zatwierdzenie badania
	   - anulowanie badania (należy podać przyczynę anulowania)